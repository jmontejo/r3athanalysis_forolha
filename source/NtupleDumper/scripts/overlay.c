#include "/afs/cern.ch/work/b/bcarlson/public//atlasstyle-00-03-05/AtlasStyle.C"
void overlay(){

  SetAtlasStyle(); 
  TFile *f1 = new TFile("cell_Time_out.root","READ"); 
  TFile *f2 = new TFile("cell_Et100_Et125p_out.root","READ");

  for(int iL=0; iL<24; iL++){
    if(iL>=12 && iL<=20)continue;
    TProfile *et = (TProfile*)f1->Get(Form("et_%d",iL) ); 
    TProfile *et_noBCID = (TProfile*)f2->Get(Form("et_%d",iL) );
      
    TCanvas *c_b = new TCanvas(Form("c_b%d",iL)); 
    et->SetLineColor(kBlue);
    if(iL<21){
      //cout << "set axis range" << endl; 
      et_noBCID->SetMinimum(-100);//-200
      et_noBCID->SetMaximum(300);//500
    }
    else {
      et_noBCID->SetMinimum(-100);//-2000
      et_noBCID->SetMaximum(500);//3000
    }
    et_noBCID->SetTitle(Form("|#eta| < 3.1, Layer %d; Dist. front of train;<E_{T}> (MeV)",iL));
    et_noBCID->SetStats(kFALSE);
    et_noBCID->SetLineColor(kRed); 
    et_noBCID->Draw("histo"); 
    et->Draw("histo same"); 

    TString dir = "/afs/cern.ch/user/b/bcarlson/www/Run3Plots/Aug14/CellOffline_Et100_Et125p/";
    TString out_b = dir+c_b->GetName()+".pdf"; 

    c_b->Print(out_b); 

  }

}
