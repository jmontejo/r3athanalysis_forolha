from ROOT import TFile,TH1F
import sys

listofiles=sys.argv[1:]

jTowerPath="jTower"

print ("merging files",listofiles)

histogrammap=["average_weight_0","average_weight_c0","rms_weight_0","jTowerArea_hist","jTowerArea_corr"]
members=["average","averagec","rms","area","correction"]

class jTowerArea:
    pass

jTowerHists=[]

for mergefile in listofiles:
    print ("reading",mergefile)
    f=TFile(mergefile)
    jTowerHist=jTowerArea()
    for hist,member in zip (histogrammap,members):
        h=f.Get(jTowerPath).Get(hist)
        h.SetDirectory(0)
        jTowerHist.__dict__[member]=h
        print (mergefile,hist,member)
    jTowerHists.append(jTowerHist)

first=True
for jTowerHist in jTowerHists:
    if first:
        first=False
        averagetotal=jTowerHist.average.Clone("averagetotal_0")
        averagetotal.Multiply(jTowerHist.averagec)
        counttotal=jTowerHist.averagec.Clone("counttotal")
        rmstotal=jTowerHist.rms.Clone("rmstotal_0")
        rmstotal.Multiply(rmstotal)
        rmstotal.Multiply(counttotal)
        continue
    jTowerHist.average.Multiply(jTowerHist.averagec)
    averagetotal.Add(jTowerHist.average)
    counttotal.Add(jTowerHist.averagec)
    jTowerHist.rms.Multiply(jTowerHist.rms)
    jTowerHist.rms.Multiply(jTowerHist.averagec)
    rmstotal.Add(jTowerHist.rms)
    print ("->",averagetotal.GetBinContent(1), counttotal.GetBinContent(1), rmstotal.GetBinContent(1))
averagetotal.Divide(counttotal)
rmstotal.Divide(counttotal)
print (averagetotal.GetBinContent(1), counttotal.GetBinContent(1), rmstotal.GetBinContent(1))
jTowerArea=f.Get(jTowerPath).Get("jTowerArea_hist")
jTowerCorr=f.Get(jTowerPath).Get("jTowerArea_corr")
jTowerArea_final=[]
for bin in range(7744):
    value=jTowerArea.GetBinContent(bin+1)
    if jTowerCorr.GetBinContent(bin+1) != 0.0:
        value=value*(averagetotal.GetBinContent(bin+1)/1000.0)
    jTowerArea_final.append(value)

norm=jTowerArea_final[0]
for bin in range(7744):
    if jTowerArea_final[bin] == 0:
        print ("WARNING bin",bin,"has zero energy, interpolating")
        value=(jTowerArea_final[bin-1]+jTowerArea_final[bin+1])/2
        print (value, jTowerArea_final[bin-1], jTowerArea_final[bin+1])
        jTowerArea_final[bin]=value
    jTowerArea_final[bin]=jTowerArea_final[bin]/norm


f=TFile("jTowerCorrection.root","RECREATE")
jTowerArea_final_hist=TH1F("jTowerArea_final_hist","jTowerArea_final_hist",7712,0,7712)
for bin in range(7744):
    jTowerArea_final_hist.SetBinContent(bin+1,jTowerArea_final[bin])
jTowerArea_final_hist.Write()
f.Close()
