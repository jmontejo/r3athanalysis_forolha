import os
import ROOT
import copy
import math
from helperFunc import *

######################################
######## Initialization
######################################

#ROOT.gROOT.SetStyle('ATLAS')

# Get files
from glob import glob
fileList = glob("/afs/cern.ch/work/v/viveiros/user.bcarlson.361020.Emulated.V15_OUTPUT/*.root*")
#fileList = glob("myfile.root")
print fileList


myTree = ROOT.TChain("ntuple")
for myFile in fileList:
    myTree.AddFile(myFile)


# Define list of criteria to be studied, and create plot array
myCrits = ["EM24", "EM24V", "EM24H", "EM24I", "EM24VHI", "EM24WHI", "EM28WHI", "EM24R3", "EM28R3", "2EM15VHI", "2EM15WHI", "2EM12WHI"]
myPlots = histHolder(myCrits, "Electron")


######################################
######## Event Loop
######################################

for myEvt in myTree:

    # Construct a list of truth and trigger taus
    truthEles = []
    for i in range(myEvt.TruthEles_pt.size()):
        if myEvt.TruthEles_barcode.at(i) > 200000:
            continue
        tempVector = ROOT.TLorentzVector()
        tempVector.SetPtEtaPhiM(myEvt.TruthEles_pt.at(i), myEvt.TruthEles_eta.at(i), myEvt.TruthEles_phi.at(i), 0)
        truthEles += [ tempVector ]


    # Construct a list of trigger candidates for Run-3
    run3Eles = []
    for i in range(myEvt.Run3_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setDec(myEvt.Run3_L1Ele_passiso.at(i))
        myROI.setP4(myEvt.Run3_L1Ele_Et.at(i), myEvt.Run3_L1Ele_eta.at(i), myEvt.Run3_L1Ele_phi.at(i))
        run3Eles += [ myROI ]


    # Construct a list of trigger candidates for Run-2
    run2Eles = []
    for i in range(myEvt.Run2_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setP4(myEvt.Run2_L1Ele_Et.at(i), myEvt.Run2_L1Ele_eta.at(i), myEvt.Run2_L1Ele_phi.at(i))
        myROI.setIso(myEvt.Run2_L1Ele_iso.at(i))
        myROI.setHad(myEvt.Run2_L1Ele_had.at(i))
        run2Eles += [ myROI ]

    # Construct Turn-on Curves for true eles

    # For each criteria, store the hottest 2 items
    # Focus on EMVHI2, EMWHI3

    myEMVHI2 = []
    myEMWHI3 = []

    for candEle in run2Eles:
        if abs(candEle.Eta()) > 2.47:
            continue

        passI =  isIso(candEle.Pt(), candEle.Iso)
        passH = isHad(candEle.Pt(), candEle.Had)
        
        myET = applyVoffset(candEle.Pt(), candEle.Eta())

        if passI and passH:
            myEMVHI2 += [myET/1000.]

    for candEle in run3Eles:
        if abs(candEle.Eta()) > 2.47:
            continue

        if candEle.Dec:
            myEMWHI3 += [candEle.Pt()]


    # Create plot of criteria
    # Start by sorting
    myEMVHI2.sort(reverse=True)
    myEMWHI3.sort(reverse=True)

    #print myEMVHI2
    #print myEMWHI3

    if len(myEMVHI2) > 0:
        myPlots.fillTOB("EM24VHI", myEMVHI2[0])
    if len(myEMVHI2) > 1:
        myPlots.fillTOB("2EM15VHI", myEMVHI2[1])

    if len(myEMWHI3) > 0:
        myPlots.fillTOB("EM28WHI", myEMWHI3[0])
    if len(myEMWHI3) > 1:
        myPlots.fillTOB("2EM15WHI", myEMWHI3[1])
    


# Done

# Store output files
myNewFile = ROOT.TFile("ratesEles.root", "RECREATE")
myNewFile.cd()
myPlots.savePlots()
