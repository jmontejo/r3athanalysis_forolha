def enhancedBiasWeight(run_number):
    from AthenaCommon.AlgSequence import AlgSequence
    from AthenaCommon.AppMgr import ToolSvc
    import AthenaCommon.CfgMgr as CfgMgr
    topSequence=AlgSequence()
    from EnhancedBiasWeighter.EnhancedBiasWeighterConf import EnhancedBiasWeighter
    TRIG1AugmentationTool = EnhancedBiasWeighter(name = "TRIG1AugmentationTool")
    TRIG1AugmentationTool.RunNumber = run_number
    TRIG1AugmentationTool.UseBunchCrossingTool = True
    ToolSvc += TRIG1AugmentationTool

    from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__DerivationKernel
    topSequence += CfgMgr.DerivationFramework__DerivationKernel("TRIG1Kernel",
                                                              AugmentationTools = [TRIG1AugmentationTool]
                                                              )
