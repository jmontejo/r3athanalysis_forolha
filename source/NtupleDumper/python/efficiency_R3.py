import os
import ROOT
import copy
import math
from helperFunc import *
import time


start = time.time()

######################################
######## Initialization ##############
######################################


# Get files
from glob import glob
fileList = glob("/eos/home-t/thrynova/Run3L1Calo/ntuples/user.bcarlson.361106.Default.Aug21v1_OUTPUT.326381138/*.root")

myTree = ROOT.TChain("ntuple")
for myFile in fileList:
    myTree.AddFile(myFile)

# myCrits = [ "EM22R390", "EM22R395", "EM22R399"]
myCrits = ["EM22R300","EM22R390", "EM22R395", "EM22R399"]
myPlots = histHolder(myCrits, "Electron")

######################################
######## Event Loop
######################################

for myEvt in myTree:

    # Construct a list of truth and trigger taus
    offEles = []
    for i in range(myEvt.OffEles_pt.size()):
        if myEvt.OffEles_flag.at(i) != 1:
            continue
        tempVector = ROOT.TLorentzVector()
        tempVector.SetPtEtaPhiM(myEvt.OffEles_pt.at(i), myEvt.OffEles_eta.at(i), myEvt.OffEles_phi.at(i), 0)
        offEles += [ tempVector ]


    # Construct a list of trigger candidates for Run-3
    run3Eles = []
    for i in range(myEvt.Run3_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setDec(myEvt.Run3_L1Ele_passiso.at(i))
        myROI.setP4(myEvt.Run3_L1Ele_Et.at(i), myEvt.Run3_L1Ele_eta.at(i), myEvt.Run3_L1Ele_phi.at(i))
        myROI.setRhad(myEvt.Run3_L1Ele_rhad.at(i))
        myROI.setWstot(myEvt.Run3_L1Ele_wstot.at(i))
        myROI.setReta(myEvt.Run3_L1Ele_reta.at(i))
        run3Eles += [ myROI ]


    # Construct Turn-on Curves for true eles
    for offEle in offEles:

        if abs(offEle.Eta()) > 2.4 or offEle.Pt() < 1000:
        #if abs(offEle.Eta()) > 2.4 or offEle.Pt() < 1000 or (abs(offEle.Eta()) > 1.37 and abs(offEle.Eta()) < 1.52):
            continue

        # Find nearest candidate, Run-III
        nearestEle = None
        smallestDR = 50.0

        for candEle in run3Eles:
            myDR = offEle.DeltaR(candEle.TLV)
            if myDR < smallestDR:
                nearestEle = candEle
                smallestDR = myDR

        if smallestDR > 0.2:
            myPlots.fillTO('EM22R300', offEle.Pt(), 0)
            myPlots.fillTO('EM22R390', offEle.Pt(), 0)
            myPlots.fillTO('EM22R395', offEle.Pt(), 0)
            myPlots.fillTO('EM22R399', offEle.Pt(), 0)
            # myPlots.fillTO('EM22R3', offEle.Pt(), 0)
        else:
            # myPlots.fillTO('EM22R3', offEle.Pt(), nearestEle.Pt()>22)
            pass00 = R3cuts_00(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass90 = R3cuts_90(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass95 = R3cuts_95(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)
            pass99 = R3cuts_99(nearestEle.Pt(), nearestEle.Rhad, nearestEle.Wstot, nearestEle.Reta)

            myPlots.fillTO('EM22R300', offEle.Pt(), pass00 and nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R390', offEle.Pt(), pass90 and nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R395', offEle.Pt(), pass95 and nearestEle.Pt() > 22)
            myPlots.fillTO('EM22R399', offEle.Pt(), pass99 and nearestEle.Pt() > 22)

# Done

# Store output files

myNewFile = ROOT.TFile("plots/eff_R3_tight_22cuts.root", "RECREATE")
#myNewFile = ROOT.TFile("plots/eff_R3_tight_22cuts_nocrack.root", "RECREATE")
myNewFile.cd()
myPlots.savePlots()
print ((time.time()- start)/60)
