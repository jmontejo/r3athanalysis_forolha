def truthTaus():
    from AthenaCommon.AlgSequence import AlgSequence
    from AthenaCommon.AppMgr import ToolSvc
    import AthenaCommon.CfgMgr as CfgMgr
    topSequence=AlgSequence()
    # Try using TAT to build truth taus?
    DFCommonTauTruthWrapperTools = []

    from MCTruthClassifier.MCTruthClassifierConf import MCTruthClassifier
    DFCommonTauTruthClassifier = MCTruthClassifier(name = "DFCommonTauTruthClassifier",
                                        ParticleCaloExtensionTool="")

    ToolSvc += DFCommonTauTruthClassifier

    from DerivationFrameworkMCTruth.DerivationFrameworkMCTruthConf import DerivationFramework__TruthCollectionMakerTau
    DFCommonTruthTauTool = DerivationFramework__TruthCollectionMakerTau(name             = "DFCommonTruthTauTool",
                                                                        NewCollectionName       = "TruthTaus",
                                                                        MCTruthClassifier       = DFCommonTauTruthClassifier)
    ToolSvc += DFCommonTruthTauTool
    DFCommonTauTruthWrapperTools.append(DFCommonTruthTauTool)

    from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauTruthMatchingTool
    from DerivationFrameworkTau.DerivationFrameworkTauConf import DerivationFramework__TauTruthMatchingWrapper
    DFCommonTauTruthMatchingTool = TauAnalysisTools__TauTruthMatchingTool(name="DFCommonTauTruthMatchingTool")
    ToolSvc += DFCommonTauTruthMatchingTool
    DFCommonTauTruthMatchingWrapper = DerivationFramework__TauTruthMatchingWrapper( name = "DFCommonTauTruthMatchingWrapper",
                                                                                    TauTruthMatchingTool = DFCommonTauTruthMatchingTool,
                                                                                    TauContainerName     = "TauJets")
    ToolSvc += DFCommonTauTruthMatchingWrapper
    #print DFCommonTauTruthMatchingWrapper
    DFCommonTauTruthWrapperTools.append(DFCommonTauTruthMatchingWrapper)

    from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__CommonAugmentation
    topSequence += CfgMgr.DerivationFramework__CommonAugmentation("TauTruthCommonKernel",
                                                                AugmentationTools = DFCommonTauTruthWrapperTools)
