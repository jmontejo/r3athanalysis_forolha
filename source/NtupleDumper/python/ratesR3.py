import os
import ROOT
import copy
import math
from helperFunc import *
from glob import glob
import time

start = time.time()

fileList = glob("/eos/home-t/thrynova/Run3L1Calo/ntuples/user.bcarlson.361020.Default.Aug21v1_OUTPUT/*.root")

myTree =  ROOT.TChain("ntuple")
for file in fileList:
    myTree.Add(file)

count18_00 = 0
count20_00 = 0
count22_00 = 0
count24_00 = 0
count26_00 = 0
count28_00 = 0

count18_90 = 0
count20_90 = 0
count22_90 = 0
count24_90 = 0
count26_90 = 0
count28_90 = 0

count18_95 = 0
count20_95 = 0
count22_95 = 0
count24_95 = 0
count26_95 = 0
count28_95 = 0

count18_99 = 0
count20_99 = 0
count22_99 = 0
count24_99 = 0
count26_99 = 0
count28_99 = 0

count22VHI = 0

for myEvt in myTree:

    count22VHI += myEvt.trig_L1_EM22VHI
    # Construct a list of trigger candidates for Run-3
    run3Eles = []
    run2Eles = []
    for i in range(myEvt.Run3_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setDec(myEvt.Run3_L1Ele_passiso.at(i))
        myROI.setP4(myEvt.Run3_L1Ele_Et.at(i), myEvt.Run3_L1Ele_eta.at(i), myEvt.Run3_L1Ele_phi.at(i))
        myROI.setRhad(myEvt.Run3_L1Ele_rhad.at(i))
        myROI.setWstot(myEvt.Run3_L1Ele_wstot.at(i))
        myROI.setReta(myEvt.Run3_L1Ele_reta.at(i))
        run3Eles += [ myROI ]


    myEM18_00 = []
    myEM20_00 = []
    myEM22_00 = []
    myEM24_00 = []
    myEM26_00 = []
    myEM28_00 = []


    myEM18_90 = []
    myEM20_90 = []
    myEM22_90 = []
    myEM24_90 = []
    myEM26_90 = []
    myEM28_90 = []

    myEM18_95 = []
    myEM20_95 = []
    myEM22_95 = []
    myEM24_95 = []
    myEM26_95 = []
    myEM28_95 = []

    myEM18_99 = []
    myEM20_99 = []
    myEM22_99 = []
    myEM24_99 = []
    myEM26_99 = []
    myEM28_99 = []


    for candEle in run3Eles:
        if abs(candEle.Eta()) > 2.47:
            continue

        pass95 = R3cuts_95(candEle.Pt(), candEle.Rhad, candEle.Wstot, candEle.Reta)
        pass99 = R3cuts_99(candEle.Pt(), candEle.Rhad, candEle.Wstot, candEle.Reta)
        pass90 = R3cuts_90(candEle.Pt(), candEle.Rhad, candEle.Wstot, candEle.Reta)
        pass00 = R3cuts_00(candEle.Pt(), candEle.Rhad, candEle.Wstot, candEle.Reta)

        if pass00 and candEle.Pt()>18:
            myEM18_00 +=[candEle.Pt()]

        if pass00 and candEle.Pt()>20:
            myEM20_00 +=[candEle.Pt()]

        if pass00 and candEle.Pt()>22:
            myEM22_00 +=[candEle.Pt()]

        if pass00 and candEle.Pt()>24:
            myEM24_00 +=[candEle.Pt()]

        if pass00 and candEle.Pt()>26:
            myEM26_00 +=[candEle.Pt()]

        if pass00 and candEle.Pt()>28:
            myEM28_00 +=[candEle.Pt()]


        if pass90 and candEle.Pt()>18:
            myEM18_90 +=[candEle.Pt()]

        if pass90 and candEle.Pt()>20:
            myEM20_90 +=[candEle.Pt()]

        if pass90 and candEle.Pt()>22:
            myEM22_90 +=[candEle.Pt()]

        if pass90 and candEle.Pt()>24:
            myEM24_90 +=[candEle.Pt()]

        if pass90 and candEle.Pt()>26:
            myEM26_90 +=[candEle.Pt()]

        if pass90 and candEle.Pt()>28:
            myEM28_90 +=[candEle.Pt()]



        if pass95 and candEle.Pt()>18:
            myEM18_95 +=[candEle.Pt()]

        if pass95 and candEle.Pt()>20:
            myEM20_95 +=[candEle.Pt()]

        if pass95 and candEle.Pt()>22:
            myEM22_95 +=[candEle.Pt()]

        if pass95 and candEle.Pt()>24:
            myEM24_95 +=[candEle.Pt()]

        if pass95 and candEle.Pt()>26:
            myEM26_95 +=[candEle.Pt()]

        if pass95 and candEle.Pt()>28:
            myEM28_95 +=[candEle.Pt()]


        if pass99 and candEle.Pt()>18:
            myEM18_99 +=[candEle.Pt()]

        if pass99 and candEle.Pt()>20:
            myEM20_99 +=[candEle.Pt()]

        if pass99 and candEle.Pt()>22:
            myEM22_99 +=[candEle.Pt()]

        if pass99 and candEle.Pt()>24:
            myEM24_99 +=[candEle.Pt()]

        if pass99 and candEle.Pt()>26:
            myEM26_99 +=[candEle.Pt()]

        if pass99 and candEle.Pt()>28:
            myEM28_99 +=[candEle.Pt()]




    if len(myEM18_00)>=1:
        count18_00 +=1

    if len(myEM20_00)>=1:
        count20_00 +=1

    if len(myEM22_00)>=1:
        count22_00 +=1

    if len(myEM24_00)>=1:
        count24_00 +=1

    if len(myEM26_00)>=1:
        count26_00 +=1

    if len(myEM28_00)>=1:
        count28_00 +=1

    if len(myEM18_90)>=1:
        count18_90 +=1

    if len(myEM20_90)>=1:
        count20_90 +=1

    if len(myEM22_90)>=1:
        count22_90 +=1

    if len(myEM24_90)>=1:
        count24_90 +=1

    if len(myEM26_90)>=1:
        count26_90 +=1

    if len(myEM28_90)>=1:
        count28_90 +=1




    if len(myEM18_95)>=1:
        count18_95 +=1

    if len(myEM20_95)>=1:
        count20_95 +=1

    if len(myEM22_95)>=1:
        count22_95 +=1

    if len(myEM24_95)>=1:
        count24_95 +=1

    if len(myEM26_95)>=1:
        count26_95 +=1

    if len(myEM28_95)>=1:
        count28_95 +=1


    if len(myEM18_99)>=1:
        count18_99 +=1

    if len(myEM20_99)>=1:
        count20_99 +=1

    if len(myEM22_99)>=1:
        count22_99 +=1

    if len(myEM24_99)>=1:
        count24_99 +=1

    if len(myEM26_99)>=1:
        count26_99 +=1

    if len(myEM28_99)>=1:
        count28_99 +=1

lst00 = [count18_00, count20_00, count22_00, count24_00, count26_00, count28_00]
lst90 = [count18_90, count20_90, count22_90, count24_90, count26_90, count28_90]
lst95 = [count18_95, count20_95, count22_95, count24_95, count26_95, count28_95]
lst99 = [count18_99, count20_99, count22_99, count24_99, count26_99, count28_99]

#print ((time.time()- start)/60)
#print("90: ", *lst90, sep = ", ")
#print("95: ", *lst95, sep = ", ")
#print("99: ", *lst99, sep = ", ")
print "22VHI: ", count22VHI
print "00: ", lst00
print "90: ", lst90
print "95: ", lst95
print "99: ", lst99
