import os
import ROOT
import copy
import math
from helperFunc import *

######################################
######## Initialization
######################################

#ROOT.gROOT.SetStyle('ATLAS')

# Get files
from glob import glob
fileList = glob("/afs/cern.ch/work/v/viveiros/user.bcarlson.361106.Emulated.V15_OUTPUT/*.root*")
#fileList = glob("myfile.root")
print fileList


myTree = ROOT.TChain("ntuple")
for myFile in fileList:
    myTree.AddFile(myFile)


# Define list of criteria to be studied, and create plot array
myCrits = ["EM24", "EM24V", "EM24H", "EM24I", "EM24VHI", "EM24WHI", "EM28WHI", "EM24R3", "EM28R3"]
myPlots = histHolder(myCrits, "Electron")


######################################
######## Event Loop
######################################

for myEvt in myTree:

    # Construct a list of truth and trigger taus
    truthEles = []
    for i in range(myEvt.TruthEles_pt.size()):
        if myEvt.TruthEles_barcode.at(i) > 200000:
            continue
        tempVector = ROOT.TLorentzVector()
        tempVector.SetPtEtaPhiM(myEvt.TruthEles_pt.at(i), myEvt.TruthEles_eta.at(i), myEvt.TruthEles_phi.at(i), 0)
        truthEles += [ tempVector ]


    # Construct a list of trigger candidates for Run-3
    run3Eles = []
    for i in range(myEvt.Run3_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setDec(myEvt.Run3_L1Ele_passiso.at(i))
        myROI.setP4(myEvt.Run3_L1Ele_Et.at(i), myEvt.Run3_L1Ele_eta.at(i), myEvt.Run3_L1Ele_phi.at(i))
        run3Eles += [ myROI ]


    # Construct a list of trigger candidates for Run-2
    run2Eles = []
    for i in range(myEvt.Run2_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setP4(myEvt.Run2_L1Ele_Et.at(i), myEvt.Run2_L1Ele_eta.at(i), myEvt.Run2_L1Ele_phi.at(i))
        myROI.setIso(myEvt.Run2_L1Ele_iso.at(i))
        myROI.setHad(myEvt.Run2_L1Ele_had.at(i))
        run2Eles += [ myROI ]

    # Construct Turn-on Curves for true eles
    for trueEle in truthEles:

        if abs(trueEle.Eta()) > 2.4 or trueEle.Pt() < 1000:
            continue

        # Find nearest candidate, Run-II
        nearestEle = None
        smallestDR = 50.0
        for candEle in run2Eles:
            myDR = trueEle.DeltaR(candEle.TLV)
            if myDR < smallestDR:
                nearestEle = candEle
                smallestDR = myDR

        if smallestDR > 0.2:
            myPlots.fillTO('EM24', trueEle.Pt(), 0)
            myPlots.fillTO('EM24V', trueEle.Pt(), 0)
            myPlots.fillTO('EM24H', trueEle.Pt(), 0)
            myPlots.fillTO('EM24I', trueEle.Pt(), 0)
            myPlots.fillTO('EM24VHI', trueEle.Pt(), 0)
        else:
            myPlots.fillTO('EM24', trueEle.Pt(), nearestEle.Pt() > 24000 )
            myPlots.fillRes('EM24', trueEle.Pt(), nearestEle.Pt())

            # Emulate Run-II decision
            passI =  isIso(nearestEle.Pt(), nearestEle.Iso)
            passH = isHad(nearestEle.Pt(), nearestEle.Had)
            passV = isV(nearestEle.Pt(), nearestEle.Eta())
            passVHI = passI and passH and passV
            #and isHad(nearestEle.Pt(), nearestEle.Had) and isV(nearestEle.Pt(), nearestEle.Eta())

            myPlots.fillTO('EM24V', trueEle.Pt(), passV and nearestEle.Pt() > 24000)
            myPlots.fillTO('EM24H', trueEle.Pt(), passH and nearestEle.Pt() > 24000)
            myPlots.fillTO('EM24I', trueEle.Pt(), passI and nearestEle.Pt() > 24000)
            myPlots.fillTO('EM24VHI', trueEle.Pt(), passVHI and nearestEle.Pt() > 24000)
            if passVHI:
                myPlots.fillRes('EM24VHI', trueEle.Pt(), nearestEle.Pt())

        # Find nearest candidate, Run-III
        nearestEle = None
        smallestDR = 50.0
        for candEle in run3Eles:
            myDR = trueEle.DeltaR(candEle.TLV)
            if myDR < smallestDR:
                nearestEle = candEle
                smallestDR = myDR

        if smallestDR > 0.2:
            myPlots.fillTO('EM28R3', trueEle.Pt(), 0)
            myPlots.fillTO('EM24R3', trueEle.Pt(), 0)
            myPlots.fillTO('EM24WHI', trueEle.Pt(), 0)
            myPlots.fillTO('EM28WHI', trueEle.Pt(), 0)
        else:
            myPlots.fillTO('EM28R3', trueEle.Pt(), nearestEle.Pt() > 28 )
            myPlots.fillTO('EM24R3', trueEle.Pt(), nearestEle.Pt() > 24 )
            myPlots.fillTO('EM28WHI', trueEle.Pt(), nearestEle.Pt() > 28 and nearestEle.Dec)
            myPlots.fillTO('EM24WHI', trueEle.Pt(), nearestEle.Pt() > 24 and nearestEle.Dec)
            myPlots.fillRes('EM28WHI', trueEle.Pt(), nearestEle.Pt())

# Done

# Store output files
myNewFile = ROOT.TFile("turnOnCurveEles.root", "RECREATE")
myNewFile.cd()
myPlots.savePlots()
