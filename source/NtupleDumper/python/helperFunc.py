import ROOT

def R3cuts_99(ET, rhad, wstot, reta):
    if ET>60:
        return 1
    else:
        if rhad < 0.31 and wstot<0.79 and reta< 0.12:
            return 1
        else:
            return 0


def R3cuts_95(ET, rhad, wstot, reta):
    if ET>60:
        return 1
    else:
        if rhad < 0.2 and wstot<0.65 and reta< 0.09:
            return 1
        else:
            return 0


def R3cuts_90(ET, rhad, wstot, reta):
    if ET>60:
        return 1
    else:
        if rhad < 0.19 and wstot<0.53 and reta< 0.08:
            return 1
        else:
            return 0

def R3cuts_00(ET, rhad, wstot, reta):
    return 1

def isIso(ET, iso):

    if ET > 50000:
        return 1
    else:
        return iso<=2000 or (iso/1000.) <= (ET/1000.)/8.0 -1.8
    
def isHad(ET, had):
    if ET > 50000:
        return 1
    else:
        return had<=1000 or (had/1000.) <= (ET/1000.)/23.0 - 0.2

# pT, eta
def isV(ET, eta, ETcut=24000):
    cutsmod = [2, 1, 0, -1, -2, -3, -1, 1]

    etarange = [0.7, 0.8, 1.1, 1.3, 1.4, 1.5, 1.7, 2.5]

    for bin in range(8):
        if( abs(eta) < etarange[bin]):
            return ET >= ETcut+(cutsmod[bin]*1000.)
        
    return 0

def applyVoffset(ET, eta):

    # Local variation on the pT cut
    cutsmod = [2, 1, 0, -1, -2, -3, -1, 1]

    etarange = [0.7, 0.8, 1.1, 1.3, 1.4, 1.5, 1.7, 2.5]

    for bin in range(8):
        if( abs(eta) < etarange[bin]):
            return ET-(cutsmod[bin]*1000)
    
    return 0
        
class tauROI:
    def __init__(self):
        self.TLV = ROOT.TLorentzVector()
        self.Iso = 0

    def setP4(self, Pt, Eta, Phi):
        self.TLV.SetPtEtaPhiM(Pt, Eta, Phi, 0)

    def Pt(self):
        return self.TLV.Pt()

    def Eta(self):
        return self.TLV.Eta()

    def Phi(self):
        return self.TLV.Phi()

    def setIso(self, iso):
        self.Iso = iso

class eleROI:
    def __init__(self):
        self.TLV = ROOT.TLorentzVector()
        # Run-II isolation/hadcore info
        self.Iso = 0
        self.Had = 0
        # Run-III combined decision
        self.Dec = 0
        self.Rhad = 0
        self.Wstot = 0
        self.Reta = 0

    def setP4(self, Pt, Eta, Phi):
        self.TLV.SetPtEtaPhiM(Pt, Eta, Phi, 0)

    def Pt(self):
        return self.TLV.Pt()

    def Eta(self):
        return self.TLV.Eta()

    def Phi(self):
        return self.TLV.Phi()

    def setIso(self, iso):
        self.Iso = iso

    def setHad(self, had):
        self.Had = had

    def setDec(self, dec):
        self.Dec = dec
    def setRhad(self, Rhad):
        self.Rhad = Rhad
    def setReta(self, Reta):
        self.Reta = Reta
    def setWstot(self, Wstot):
        self.Wstot = Wstot

# Histogram Array Holder Class
class histHolder:
    
    # Resolution Plots Dictionary
    resPlots = {}
    # Turn-On Curves Dictionary
    toPlots = {}
    # nTOB Dictionary
    ntobPlots = {}
    # Integral for rates
    intPlots = {}

    def __init__(self, listOfCriteria, name="#tau vis."):
        for myCrit in listOfCriteria:
            self.toPlots[myCrit] =  ROOT.TProfile("TurnOn"+myCrit, "TurnOn"+myCrit, 100, 0, 200)
            self.toPlots[myCrit].SetXTitle("true "+name+" p_{T}")
            self.toPlots[myCrit].SetYTitle("Efficiency")

            self.resPlots[myCrit] = ROOT.TH1D("Reso"+myCrit, "Reso"+myCrit, 50, -1.0, 1.0)
            self.resPlots[myCrit].SetXTitle("#frac{L1 #tau p_{T} - true #tau p_{T}}{true #tau p_{T}}")
            self.resPlots[myCrit].SetYTitle("Entries")
            
            self.ntobPlots[myCrit] = ROOT.TH1D("nTOB"+myCrit, "nTOB"+myCrit, 200, 0, 200)
            self.ntobPlots[myCrit].SetXTitle("TOB p_{T}")
            self.ntobPlots[myCrit].SetYTitle("Entries")

            self.intPlots[myCrit]  = ROOT.TH1D("int"+myCrit, "int"+myCrit, 200, 0, 200)
            self.intPlots[myCrit].SetXTitle(name+" p_{T} [GeV]")
            self.intPlots[myCrit].SetYTitle("Rate")

    def fillTO(self, name, pT, status):
        self.toPlots[name].Fill(pT/1000., status)

    def fillRes(self, name, etTrue, etCand):
        if etTrue > 0:
            self.resPlots[name].Fill((etCand-etTrue)/etTrue)

    def fillTOB(self, name, etCand, weight=1):
        self.ntobPlots[name].Fill(etCand, weight)

    def scaleEB(self, weight):
        for key in self.ntobPlots:
            self.ntobPlots[key].Scale(1./weight)

    def savePlots(self):
        for key in self.resPlots:
            self.resPlots[key].Write()
        for key in self.toPlots:
            self.toPlots[key].Write()
        for key in self.ntobPlots:
            self.ntobPlots[key].Write()
        for key in self.intPlots:
            for i in range(self.intPlots[key].GetNbinsX()):
                self.intPlots[key].SetBinContent(i+1, self.ntobPlots[key].Integral(i+1, 200+1))
            self.intPlots[key].Write()
