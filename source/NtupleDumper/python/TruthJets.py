#Truth jets
def truthJets(isESD):
    from AthenaCommon.AlgSequence import AlgSequence
    topSequence = AlgSequence()
    #An easy way: you just add this line... the problem is, it doesn't work in 21.3
    #from DerivationFrameworkMCTruth.MCTruthCommon import addStandardTruthContents
    #addStandardTruthContents()
    
    if(isESD==False):
        print "Using the InDetPhysValMonitoring code to build truth jets. Someone has also hacked it here, and it works for one jet collection..."
        import InDetPhysValMonitoring.InDetPhysValMonitoringConf
        from InDetPhysValMonitoring.addTruthJets import addTruthJetsIfNotExising
        addTruthJetsIfNotExising("AntiKt4TruthJets")
     
        return
    
    from DerivationFrameworkCore.DerivationFrameworkMaster import *
    from DerivationFrameworkMCTruth.MCTruthCommon import * 
    
    from JetRec.JetAlgorithm import addJetRecoToAlgSequence
    addJetRecoToAlgSequence(topSequence,eventShapeTools=None)

    from JetRec.JetRecFlags import jetFlags
    jetFlags.useTruth = True
    jetFlags.useTracks = False
    from JetRec.JetRecStandard import jtm
    from JetRec.JetRecConf import JetAlgorithm
    
    jetFlags.truthFlavorTags = ["BHadronsInitial", "BHadronsFinal", "BQuarksFinal",
                                "CHadronsInitial", "CHadronsFinal", "CQuarksFinal",
                                "TausFinal",
                                "Partons",
                            ]

    akt4 = jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth",
                            modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler], 
                            ptmin= 5000)
    akt4alg = JetAlgorithm("jetalgAntiKt4TruthJets", Tools = [akt4] )
    topSequence +=  akt4alg
    
    akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth",
                             modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler],
                             ptmin= 5000)
    akt10alg = JetAlgorithm("jetalgAntiKt10TruthJets", Tools = [akt10] )
    topSequence +=  akt10alg

    #akt10trim = jtm.addJetTrimmer("TrimmedAntiKt10TruthJets", rclus=0.2, ptfrac=0.05, input='AntiKt10TruthJets', modifiersin=[jtm.nsubjettiness, jtm.removeconstit])
    #akt10trimalg = JetAlgorithm("jetalgTrimmedAntiKt10TruthJets", Tools = [akt10trim] )
    #topSequence +=  akt10trim

