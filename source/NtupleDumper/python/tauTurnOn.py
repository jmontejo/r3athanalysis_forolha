import os
import ROOT
import copy
import math
from helperFunc import *

ROOT.gROOT.SetStyle('ATLAS')

######################################
######## Initialization
######################################

# Get files
from glob import glob
#fileList = glob("/afs/cern.ch/work/v/viveiros/user.bcarlson.361108.Default.V12_OUTPUT/*.root*")
fileList = glob("/afs/cern.ch/work/v/viveiros/user.bcarlson.361108.Emulated.V15_OUTPUT/*.root*")
#fileList = glob("myfile.root")
print fileList


myTree = ROOT.TChain("ntuple")
for myFile in fileList:
    myTree.AddFile(myFile)


# Define list of criteria to be studied, and create plot array
myCrits = ["TAU12", "TAU12R3", "TAU20", "TAU20R3", "TAU12E", "TAU12E3", "TAU20E", "TAU20E3"]
myPlots = histHolder(myCrits)


######################################
######## Event Loop
######################################

for myEvt in myTree:

    
    # Construct a list of truth and trigger taus
    truthTaus = []
    for i in range(myEvt.TruthTaus_ptvis.size()):
        tempVector = ROOT.TLorentzVector()
        tempVector.SetPtEtaPhiM(myEvt.TruthTaus_ptvis.at(i), myEvt.TruthTaus_etavis.at(i), myEvt.TruthTaus_phivis.at(i), 0)
        truthTaus += [ tempVector ]


    # Construct a list of trigger candidates for Run-3
    run3Taus = []
    for i in range(myEvt.Run3_L1Tau_Et.size()):
        myROI = tauROI()
        myROI.setP4(myEvt.Run3_L1Tau_Et.at(i), myEvt.Run3_L1Tau_eta.at(i), myEvt.Run3_L1Tau_phi.at(i))
        run3Taus += [ myROI ]


    # Construct a list of trigger candidates for Run-2
    run2Taus = []
    for i in range(myEvt.Run2_L1Tau_Et.size()):
        myROI = tauROI()
        myROI.setP4(myEvt.Run2_L1Tau_Et.at(i), myEvt.Run2_L1Tau_eta.at(i), myEvt.Run2_L1Tau_phi.at(i))
        myROI.setIso(myEvt.Run2_L1Tau_iso.at(i))
        run2Taus += [ myROI ]

    # Construct Turn-on Curves for true taus
    for trueTau in truthTaus:

        if abs(trueTau.Eta()) > 2.4 or trueTau.Pt() < 1000:
            continue

        # Find nearest candidate, Run-II
        nearestTau = None
        smallestDR = 50.0
        for candTau in run2Taus:
            myDR = trueTau.DeltaR(candTau.TLV)
            if myDR < smallestDR:
                nearestTau = candTau
                smallestDR = myDR

        if smallestDR > 0.2:
            myPlots.fillTO('TAU12', trueTau.Pt(), 0) 
            myPlots.fillTO('TAU20', trueTau.Pt(), 0)
            if abs(trueTau.Eta()) > 1.5:
                myPlots.fillTO('TAU12E', trueTau.Pt(), 0) 
                myPlots.fillTO('TAU20E', trueTau.Pt(), 0)

        else:
            myPlots.fillTO('TAU12', trueTau.Pt(), nearestTau.Pt() > 12000 )
            myPlots.fillTO('TAU20', trueTau.Pt(), nearestTau.Pt() > 20000 )
            if abs(trueTau.Eta()) > 1.5:
                myPlots.fillTO('TAU12E', trueTau.Pt(), nearestTau.Pt() > 12000 )
                myPlots.fillTO('TAU20E', trueTau.Pt(), nearestTau.Pt() > 20000 )

            myPlots.fillRes('TAU12', trueTau.Pt(), nearestTau.Pt())

        # Find nearest candidate, Run-III
        nearestTau = None
        smallestDR = 50.0
        for candTau in run3Taus:
            myDR = trueTau.DeltaR(candTau.TLV)
            if myDR < smallestDR:
                nearestTau = candTau
                smallestDR = myDR

        if smallestDR > 0.2:
            myPlots.fillTO('TAU12R3', trueTau.Pt(), 0)
            myPlots.fillTO('TAU20R3', trueTau.Pt(), 0)
            if abs(trueTau.Eta()) > 1.5:
                myPlots.fillTO('TAU12E3', trueTau.Pt(), 0) 
                myPlots.fillTO('TAU20E3', trueTau.Pt(), 0)
        else:
            myPlots.fillTO('TAU12R3', trueTau.Pt(), nearestTau.Pt() > 12000 )
            myPlots.fillTO('TAU20R3', trueTau.Pt(), nearestTau.Pt() > 20000 )
            myPlots.fillRes('TAU12R3', trueTau.Pt(), nearestTau.Pt())
            if abs(trueTau.Eta()) > 1.5:
                myPlots.fillTO('TAU12E3', trueTau.Pt(), nearestTau.Pt() > 12000) 
                myPlots.fillTO('TAU20E3', trueTau.Pt(), nearestTau.Pt() > 20000)

# Done

# Store output files
myNewFile = ROOT.TFile("turnOnCurveTaus.root", "RECREATE")
myNewFile.cd()
myPlots.savePlots()
