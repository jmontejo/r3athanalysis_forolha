import os
import ROOT
import copy
import math
from helperFunc import *
from glob import glob
import time

start = time.time()

from glob import glob

fileList = glob("/eos/home-t/thrynova/Run3L1Calo/ntuples/user.bcarlson.361020.Default.Aug21v1_OUTPUT/*.root")

myTree =  ROOT.TChain("ntuple")
for file in fileList:
    myTree.Add(file)


trig_counts = 0
em22 = 0
r3_em22_count = 0

countsEM22_95 = 0
countsEM22_99 = 0

for myEvt in myTree:


    # Construct a list of trigger candidates for Run-3
    run2Eles = []
    for i in range(myEvt.Run2_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setP4(myEvt.Run2_L1Ele_Et.at(i), myEvt.Run2_L1Ele_eta.at(i), myEvt.Run2_L1Ele_phi.at(i))
        myROI.setIso(myEvt.Run2_L1Ele_iso.at(i))
        myROI.setHad(myEvt.Run2_L1Ele_had.at(i))
        run2Eles += [ myROI ]



    run3Eles = []
    for i in range(myEvt.Run3_L1Ele_Et.size()):
        myROI = eleROI()
        myROI.setDec(myEvt.Run3_L1Ele_passiso.at(i))
        myROI.setP4(myEvt.Run3_L1Ele_Et.at(i), myEvt.Run3_L1Ele_eta.at(i), myEvt.Run3_L1Ele_phi.at(i))
        myROI.setRhad(myEvt.Run3_L1Ele_rhad.at(i))
        myROI.setWstot(myEvt.Run3_L1Ele_wstot.at(i))
        myROI.setReta(myEvt.Run3_L1Ele_reta.at(i))
        run3Eles += [ myROI ]


    myEM22 = []
    trig = []
    r3_em22 = []

    em22_95 = []
    em22_99 = []



    for candEle in run2Eles:

        passI =  isIso(candEle.Pt(), candEle.Iso)
        passH = isHad(candEle.Pt(), candEle.Had)
        passV = isV(candEle.Pt(), candEle.Eta(), 22000)
        passVHI = passI and passH and passV


        if passVHI:
            myEM22 += [candEle.Pt()]

    if myEvt.trig_L1_EM22VHI>0:
        trig_counts+=1

    if len(myEM22)>=1:
        em22+=1

    for candEle in run3Eles:

        pass95 = R3cuts_95(candEle.Pt(), candEle.Rhad, candEle.Wstot, candEle.Reta)
        pass99 = R3cuts_99(candEle.Pt(), candEle.Rhad, candEle.Wstot, candEle.Reta)

        if candEle.Pt()>22:
            r3_em22 +=[candEle.Pt()]

        if pass95 and candEle.Pt()>22:
            em22_95+=[candEle.Pt()]

        if pass99 and candEle.Pt()>22:
            em22_99+=[candEle.Pt()]




    if len(r3_em22)>=1:
        r3_em22_count +=1

    if len(em22_95)>=1:
        countsEM22_95+=1

    if len(em22_99)>=1:
        countsEM22_99+=1




print ((time.time()- start)/60)
print("Tring counts: ", trig_counts)
print("EM22VHI: ", em22)
print("Run 3 EM22: ", r3_em22_count)
print("Run 3 EM22 95%:", countsEM22_95)
print("Run 3 EM22 99%:", countsEM22_99)
