#ifndef NTUPLEDUMPER_NTUPLEDUMPERALG_H
#define NTUPLEDUMPER_NTUPLEDUMPERALG_H 1

#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigger/EnergySumRoIAuxInfo.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "TrigT1Interfaces/L1METvalue.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/EmTauRoIContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoI.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventShape/EventShape.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTrigL1Calo/JGTowerContainer.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"


#include "TTree.h"

class NtupleDumperAlg: public ::AthAlgorithm { 
 public: 
  NtupleDumperAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~NtupleDumperAlg(); 

  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed
  virtual StatusCode  Run2_L1();
  virtual StatusCode  HLT_MET_algs();
  virtual StatusCode  MET_algs();
  virtual StatusCode  Jet_algs();
  virtual StatusCode  Tau_algs();
  virtual StatusCode  Ele_algs();
    virtual StatusCode RetrieveTopoclusters(TString name);
  virtual StatusCode  RetrieveMET(TString name); 
  virtual StatusCode  RetrieveHLTMET(TString name);
  virtual StatusCode  RetrieveJets(TString name);
  virtual StatusCode  RetrieveL1Jets(TString name);
  virtual StatusCode  FillTrigDecisions(); 
  virtual StatusCode  FillTruthMET();
  virtual StatusCode  FillTruthTaus();
  virtual StatusCode  FillTruthEles();
  virtual StatusCode  FillTruthPhs();
  virtual StatusCode  FillEventShapes(); 
  virtual StatusCode  CalibrateOffJets();
  virtual StatusCode  ConstituentScaleHLTJets();
  virtual StatusCode  FillHltPhs();
  static  bool        GreaterPt(TLorentzVector &a, TLorentzVector &b); 
  virtual StatusCode  FilterPileupJets(); 
  virtual StatusCode  FillOffEles();
  virtual StatusCode  FillOffPhs();
  virtual StatusCode  DumpTowers();

 private: 
  ToolHandle<IJetCalibrationTool> m_jetCalibration;
  ToolHandle<IJetUpdateJvt> m_jvt;
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecTool{"Trig::TrigDecisionTool/TrigDecisionTool"}; //!
  asg::AnaToolHandle<Trig::IMatchingTool>    m_trigJetMatchTool{"Trig::MatchingTool/MatchingTool", this}; //!
  asg::AnaToolHandle<CP::IsolationSelectionTool> iso_tool{"CP::IsolationSelectionTool/IsolationSelectionTool"}; //!

  TTree *m_tree;
  float mu; 
  int eventNumber; 
  double eventLiveTime; 
  double eventWeight; 
  float distFrontBunchTrain;

  float eventDensity; 
  float eventDensitySigma;
  float eventArea; 

  float m_rhoA; float m_rhoB; float m_rhoC; 
  float m_threshA; float m_threshB; float m_threshC; 

  bool goodLB; 
  bool isPileup;

  float truth_met_et; 
  bool m_useRun2L1; 
  bool m_dumpTrigDec;
  bool m_dumpTauAlgs; 
  bool m_dumpEleAlgs;
  bool m_dumpTruthJets; 
  bool m_dumpTruthTaus;
  bool m_dumpTruthEles;
  bool m_dumpTruthMet;
  bool m_dumpTruthPhs;
  bool m_dumpOffEles;
  bool m_dumpOffJets; 
  bool m_applyJVT; 
  bool m_dumpOffPhs;
  bool m_dumpOnJets; 
  bool m_dumpTowers; 
  bool m_dumpRun2Towers; 
  bool m_dumpHltMet;
  bool m_dumpHltPhs;
  bool m_applyFilter;
  bool m_dumpClusters;
  bool m_isMC; 
  float Run2_L1MET;
  float Run2_L1MET_phi; 
                                   
  // Run 2 Jets
  std::vector<float> Run2_L1Jet_Et;
  std::vector<float> Run2_L1Jet_eta;
  std::vector<float> Run2_L1Jet_phi;

  // Run 2 Electrons
  std::vector<float> Run2_L1Ele_Et;
  std::vector<float> Run2_L1Ele_eta;
  std::vector<float> Run2_L1Ele_phi;
  std::vector<float> Run2_L1Ele_iso;
  std::vector<float> Run2_L1Ele_had;

  // Run 2 Taus
  std::vector<float> Run2_L1Tau_Et;
  std::vector<float> Run2_L1Tau_eta;
  std::vector<float> Run2_L1Tau_phi;
  std::vector<float> Run2_L1Tau_iso;

  //Run 2 muons
  std::vector<float> Run2_L1Muon_ptThres; 
  std::vector<float> Run2_L1Muon_eta;
  std::vector<float> Run2_L1Muon_phi; 

  std::vector<float>  Run3_jTau_Et;
  std::vector<float>  Run3_jTau_eta; 
  std::vector<float>  Run3_jTau_phi;
  std::vector<float>  Run3_jTau_iso;
  //jFEX forward electrons 
  std::vector<float>  Run3_jEle_Et;
  std::vector<float>  Run3_jEle_eta;
  std::vector<float>  Run3_jEle_phi;
  std::vector<float>  Run3_jEle_iso;
  std::vector<float>  Run3_jEle_Had;
  
  // Run 3 Electrons
  std::vector<float> Run3_L1Ele_Et;
  std::vector<float> Run3_L1Ele_eta;
  std::vector<float> Run3_L1Ele_phi;
  std::vector<float> Run3_L1Ele_rhad;
  std::vector<float> Run3_L1Ele_reta;
  std::vector<float> Run3_L1Ele_retaL12;
  std::vector<float> Run3_L1Ele_wstot;
  std::vector<int> Run3_L1Ele_passiso;
  
  // Run 3 Taus
  std::vector<float> Run3_L1Tau_Et;
  std::vector<float> Run3_L1Tau_eta;
  std::vector<float> Run3_L1Tau_phi;
  std::vector<float> Run3_L1Tau_iso;
  std::vector<float> Run3_L1Tau_BC_Et;
  std::vector<float> Run3_L1Tau_BC_iso;
  std::vector<bool> Run3_L1Tau_BC_iso_12pass;
  std::vector<bool> Run3_L1Tau_BC_iso_20pass;
  std::vector<float> Run3_L1Tau_Ore_Et;
  std::vector<float> Run3_L1Tau_Ore_iso;
  std::vector<bool> Run3_L1Tau_Ore_iso_12pass;
  std::vector<bool> Run3_L1Tau_Ore_iso_20pass;

  // HLT Photons                                                                                                                                                      
  std::vector<float> HltPhs_pt;
  std::vector<float> HltPhs_eta;
  std::vector<float> HltPhs_phi;
  std::vector<float> HltPhs_ptcone;
  std::vector<float> HltPhs_etcone;
  std::vector<int> HltPhs_flag;
  std::vector<int> HltPhs_tightflag;
  std::vector<int> HltPhs_looseisoflag;
  std::vector<int> HltPhs_tightisoflag;

  // Truth Taus
  std::vector<float> TruthTaus_ptvis;
  std::vector<float> TruthTaus_etavis;
  std::vector<float> TruthTaus_phivis;
  std::vector<float> TruthTaus_mvis;
  std::vector<int>   TruthTaus_nCharged;
  std::vector<int>   TruthTaus_nNeutral;

  // Truth Electrons
  std::vector<float> TruthEles_pt;
  std::vector<float> TruthEles_eta;
  std::vector<float> TruthEles_phi;
  std::vector<int> TruthEles_barcode;
  std::vector<int> TruthEles_motherID;

  // Truth Photons                                                                                                                                                    
  std::vector<float> TruthPhs_pt;
  std::vector<float> TruthPhs_eta;
  std::vector<float> TruthPhs_phi;
  std::vector<int> TruthPhs_barcode;
  std::vector<int> TruthPhs_motherID;

  // Offline Electrons
  std::vector<float> OffEles_pt;
  std::vector<float> OffEles_eta;
  std::vector<float> OffEles_phi;
  std::vector<float> OffEles_ptcone;
  std::vector<float> OffEles_etcone;
  std::vector<int> OffEles_flag;
  std::vector<int> OffEles_tightflag;
  std::vector<int> OffEles_isoflag;

  // Offline Photons                                                                                                                                                  
  std::vector<float> OffPhs_pt;
  std::vector<float> OffPhs_eta;
  std::vector<float> OffPhs_phi;
  std::vector<float> OffPhs_ptcone;
  std::vector<float> OffPhs_etcone;
  std::vector<int> OffPhs_flag;
  std::vector<int> OffPhs_tightflag;
  std::vector<int> OffPhs_isoflag;

  std::vector<float> m_gTowerEt;
  std::vector<float> m_gTowerEta;
  std::vector<float> m_gTowerPhi; 
  std::vector<int>   m_gTowerSampling; 
  
  std::vector<float> m_jTowerEt;
  std::vector<float> m_jTowerEta;
  std::vector<float> m_jTowerPhi;
  std::vector<int> m_jTowerSampling;

  std::vector<float> m_Run2TowerEt;
  std::vector<float> m_Run2TowerEta;
  std::vector<float> m_Run2TowerPhi;
  std::vector<int> m_Run2TowerSampling;

  //MET algorithm variables
  std::map<TString, float> m_met_et;
  std::map<TString, float> m_met_phi;

  std::map<TString, float> m_hlt_met_et;
  std::map<TString, float> m_hlt_met_phi; 

  std::map<TString, float> m_jwoj_mht;
  std::map<TString, float> m_jwoj_mst;

  std::vector<TString> m_hlt_met_list; 
  std::vector<TString> m_met_list; 
  std::vector<TString> m_jet_list;

  std::vector<std::string> Mlist;
  std::vector<std::string> HltMlist; 
  std::vector<std::string> Jlist; 
  std::vector<std::string> triggerlist;

  int * triggerDecisions;  

  std::map<TString, std::vector<float> > m_jet_pt; 
  std::map<TString, std::vector<float> > m_jet_phi; 
  std::map<TString, std::vector<float> > m_jet_eta;
  std::map<TString, std::vector<float> > m_jet_m;


}; 

#endif //> !NTUPLEDUMPER_NTUPLEDUMPERALG_H
