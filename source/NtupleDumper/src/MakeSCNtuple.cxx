// NtupleDumper includes
#include "MakeSCNtuple.h"

//#include "xAODEventInfo/EventInfo.h"




MakeSCNtuple::MakeSCNtuple( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
    
    //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
    declareProperty("SuperCellContainer", m_SuperCellContainer="SCell");
    
}


MakeSCNtuple::~MakeSCNtuple() {}


StatusCode MakeSCNtuple::initialize() {
    ATH_MSG_INFO ("Initializing " << name() << "...");
    ATH_MSG_INFO ("Supercell container " << m_SuperCellContainer);
    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK( histSvc.retrieve() );
    
    m_tree = new TTree("SCntuple","SCntuple");
    
    m_tree->Branch("scells_Et",&scells_Et);
    m_tree->Branch("scells_eta",&scells_eta);
    m_tree->Branch("scells_phi",&scells_phi);
    m_tree->Branch("scells_time",&scells_time);
    m_tree->Branch("scells_quality",&scells_quality);
    m_tree->Branch("scells_provenance",&scells_provenance);
    m_tree->Branch("scells_sampling",&scells_sampling);
    m_tree->Branch("scells_PF",&scells_PF);
    m_tree->Branch("scells_Time",&scells_Time);
    
    CHECK( histSvc->regTree("/OUTPUT/SCTree",m_tree) );
    
    return StatusCode::SUCCESS;
}

StatusCode MakeSCNtuple::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    //
    //Things that happen once at the end of the event loop go here
    //
    
    
    return StatusCode::SUCCESS;
}

StatusCode MakeSCNtuple::FillTree(){
    const CaloCellContainer* scells = 0;
    CHECK( evtStore()->retrieve( scells, m_SuperCellContainer.data() ) );
    scells_Et.clear();
    scells_eta.clear();
    scells_phi.clear();
    scells_time.clear();
    scells_quality.clear();
    scells_provenance.clear();
    scells_sampling.clear();
    scells_PF.clear();
    scells_Time.clear();
    
    for(auto scell : *scells) {
        
        float et = scell->et();
        float t = scell->time();
        
        int sampling = scell->caloDDE()->getSampling();

        bool passTime = scell->provenance() & 0x200;
        bool passPF = scell->provenance() & 0x40;
        
        scells_Et.push_back(et);
        scells_eta.push_back(scell->eta() );
        scells_phi.push_back(scell->phi() );
        scells_time.push_back(t);
        scells_quality.push_back(scell->quality() );
        scells_sampling.push_back(sampling);
        scells_PF.push_back(passPF);
        scells_Time.push_back(passTime);
    }
    m_tree->Fill();
    return StatusCode::SUCCESS;
}

StatusCode MakeSCNtuple::execute() {  
    ATH_MSG_DEBUG ("Executing " << name() << "...");
    setFilterPassed(false); //optional: start with algorithm not passed
    
    CHECK(FillTree());
    
    setFilterPassed(true); //if got here, assume that means algorithm passed
    return StatusCode::SUCCESS;
}

