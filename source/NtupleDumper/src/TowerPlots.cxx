// NtupleDumper includes
#include "TowerPlots.h"
#include "xAODEventInfo/EventInfo.h"




TowerPlots::TowerPlots( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("SuperCellContainer", m_SuperCellContainer="SCell"); 
}


TowerPlots::~TowerPlots() {}


StatusCode TowerPlots::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  ATH_MSG_INFO ("Supercell container " << m_SuperCellContainer); 
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  CHECK( histSvc.retrieve() );

  std::string name = "";
  //initialize sc plots 
  m_scTimeRawET10 = new TH1F("m_scTimeRawET10",";Time (ns);",100,-50,50);
  m_scTimeRaw10ET = new TH1F("m_scTimeRaw10ET",";Time (ns);",100,-50,50);

  m_scTimeET10 = new TH1F("m_scTimeET10",";Time (ns);",100,-50,50);
  m_scTime10ET = new TH1F("m_scTime10ET",";Time (ns);",100,-50,50);
  m_scTimeET = new TH1F("m_scTimeET",";E_{T} (MeV);",100,-5000,15000);

  m_scTimePFET10 = new TH1F("m_scTimePFET10",";Time (ns);",100,-50,50);
  m_scTimePF10ET = new TH1F("m_scTimePF10ET",";Time (ns);",100,-50,50);
  m_scPFET = new TH1F("m_scPFET",";E_{T} (MeV);",100,-5000,15000);

  m_scPFvsTimingET10 = new TH1F("m_scPFvsTimingET10","",10,0,10);
  m_scPFvsTiming10ET = new TH1F("m_scPFvsTiming10ET","",10,0,10); 

  m_SCET_bcid = new TProfile("m_SCET_bcid","",50,0,50); 
  m_SCET_mu   = new TProfile("m_SCET_mu","",100,0,100);

  name = Form("/OUTPUT/SC/%s",m_scTimeRawET10->GetName() );
  CHECK( histSvc->regHist(name,m_scTimeRawET10) );

  name = Form("/OUTPUT/SC/%s",m_scTimeRaw10ET->GetName() );
  CHECK( histSvc->regHist(name,m_scTimeRaw10ET) );

  name = Form("/OUTPUT/SC/%s",m_scTimeET10->GetName() );
  CHECK( histSvc->regHist(name,m_scTimeET10) );

  name = Form("/OUTPUT/SC/%s",m_scTime10ET->GetName() );
  CHECK( histSvc->regHist(name,m_scTime10ET) );

  name = Form("/OUTPUT/SC/%s",m_scTimeET->GetName() );
  CHECK( histSvc->regHist(name,m_scTimeET) );

  name = Form("/OUTPUT/SC/%s",m_scTimePFET10->GetName() );
  CHECK( histSvc->regHist(name,m_scTimePFET10) );

  name = Form("/OUTPUT/SC/%s",m_scTimePF10ET->GetName() );
  CHECK( histSvc->regHist(name,m_scTimePF10ET) );

  name = Form("/OUTPUT/SC/%s",m_scPFET->GetName() );
  CHECK( histSvc->regHist(name,m_scPFET)); 

  name = Form("/OUTPUT/SC/%s",m_scPFvsTimingET10->GetName() );
  CHECK( histSvc->regHist(name,m_scPFvsTimingET10) ); 
  
  name = Form("/OUTPUT/SC/%s",m_scPFvsTiming10ET->GetName() );
  CHECK( histSvc->regHist(name,m_scPFvsTiming10ET) );

  name = Form("/OUTPUT/SC/%s",m_SCET_bcid->GetName() );
  CHECK( histSvc->regHist(name,m_SCET_bcid) );

  name = Form("/OUTPUT/SC/%s",m_SCET_mu->GetName() );
  CHECK( histSvc->regHist(name,m_SCET_mu) );

  //initialize gTower plots
  m_gTowerET = new TH1F("m_gTowerET",";ET [GeV];",300,-10,20); 
  m_gTowerET_EM = new TH1F("m_gTowerET_EM",";ET [GeV];",300,-10,20); 
  m_gTowerET_had = new TH1F("m_gTowerET_had",";ET [GeV];",300,-10,20); 
  m_gTowerET_fwd = new TH1F("m_gTowerET_fwd",";ET [GeV];",300,-10,20); 
  m_gTowerEta = new TH1F("m_gTowerEta",";#eta;",128,-6.4,6.4); 
  m_gTowerPhi = new TH1F("m_gTowerPhi",";#phi;",128,-6.4,6.4); 
  m_gTowerET_BCID = new TH2F("m_gTowerET_BCID",";BCID distance from front of train;ET [GeV]",100,0,50,300,-10,20); 
  m_gTowerEtaPhi_ETmap = new TH2F("m_gTowerEtaPhi_ETmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4);
  m_gTowerEtaPhi_ET_EMmap = new TH2F("m_gTowerEtaPhi_ET_EMmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4);
  m_gTowerEtaPhi_ET_hadmap = new TH2F("m_gTowerEtaPhi_ET_hadmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4);
  m_gTowerEtaPhi_ET_fwdmap = new TH2F("m_gTowerEtaPhi_ET_fwdmap",";#eta;#phi",64,-6.4,6.4,64,-6.4,6.4);
  m_gTowerEtaPhi_BCIDmap = new TH2F("m_gTowerEtaPhi_BCIDmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4); 
 
  //initialize jTower plots
  m_jTowerET = new TH1F("m_jTowerET",";ET [GeV];",300,-10,20); 
  m_jTowerET_EM = new TH1F("m_jTowerET_EM",";ET [GeV];",300,-10,20); 
  m_jTowerET_had = new TH1F("m_jTowerET_had",";ET [GeV];",300,-10,20); 
  m_jTowerET_fwd = new TH1F("m_jTowerET_fwd",";ET [GeV];",300,-10,20); 
  m_jTowerEta = new TH1F("m_jTowerEta",";#eta;",128,-6.4,6.4); 
  m_jTowerPhi = new TH1F("m_jTowerPhi",";#phi;",128,-6.4,6.4); 
  m_jTowerET_BCID = new TH2F("m_jTowerET_BCID",";BCID distance from front of train;ET [GeV]",100,0,50,300,-10,20);  
  m_jTowerEtaPhi_ETmap = new TH2F("m_jTowerEtaPhi_ETmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4); 
  m_jTowerEtaPhi_ET_EMmap = new TH2F("m_jTowerEtaPhi_ET_EMmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4); 
  m_jTowerEtaPhi_ET_hadmap = new TH2F("m_jTowerEtaPhi_ET_hadmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4); 
  m_jTowerEtaPhi_ET_fwdmap = new TH2F("m_jTowerEtaPhi_ET_fwdmap",";#eta;#phi",64,-6.4,6.4,64,-6.4,6.4); 
  m_jTowerEtaPhi_BCIDmap = new TH2F("m_jTowerEtaPhi_BCIDmap",";#eta;#phi",128,-6.4,6.4,128,-6.4,6.4); 

  m_jTowerET_bcid = new TProfile("m_jTowerET_bcid","",50,0,50);
  m_jTowerET_mu = new TProfile("m_jTowerET_mu","",50,0,50);

  m_gTowerET_bcid = new TProfile("m_gTowerET_bcid","",50,0,50);
  m_gTowerET_mu = new TProfile("m_gTowerET_mu","",50,0,50);

  //register output gTower
  name = Form("/OUTPUT/gTowers/%s",m_gTowerET->GetName() ); 
  CHECK( histSvc->regHist(name,m_gTowerET) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerET_EM->GetName() ); 
  CHECK( histSvc->regHist(name,m_gTowerET_EM) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerET_had->GetName() ); 
  CHECK( histSvc->regHist(name,m_gTowerET_had) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerET_fwd->GetName() ); 
  CHECK( histSvc->regHist(name,m_gTowerET_fwd) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerEta->GetName() ); 
  CHECK( histSvc->regHist(name,m_gTowerEta) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerPhi->GetName() ); 
  CHECK( histSvc->regHist(name,m_gTowerPhi) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerET_BCID->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerET_BCID) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerEtaPhi_ETmap->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerEtaPhi_ETmap) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerEtaPhi_ET_EMmap->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerEtaPhi_ET_EMmap) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerEtaPhi_ET_hadmap->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerEtaPhi_ET_hadmap) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerEtaPhi_ET_fwdmap->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerEtaPhi_ET_fwdmap) );
  name = Form("/OUTPUT/gTowers/%s",m_gTowerEtaPhi_BCIDmap->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerEtaPhi_BCIDmap) );

  name = Form("/OUTPUT/gTowers/%s",m_gTowerET_bcid->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerET_bcid ) ); 

  name = Form("/OUTPUT/gTowers/%s",m_gTowerET_mu->GetName() );
  CHECK( histSvc->regHist(name,m_gTowerET_mu ) );

  //register output jTower
  name = Form("/OUTPUT/jTowers/%s",m_jTowerET->GetName() ); 
  CHECK( histSvc->regHist(name,m_jTowerET) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerET_EM->GetName() ); 
  CHECK( histSvc->regHist(name,m_jTowerET_EM) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerET_had->GetName() ); 
  CHECK( histSvc->regHist(name,m_jTowerET_had) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerET_fwd->GetName() ); 
  CHECK( histSvc->regHist(name,m_jTowerET_fwd) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerEta->GetName() ); 
  CHECK( histSvc->regHist(name,m_jTowerEta) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerPhi->GetName() ); 
  CHECK( histSvc->regHist(name,m_jTowerPhi) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerET_BCID->GetName() );  
  CHECK( histSvc->regHist(name,m_jTowerET_BCID) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerEtaPhi_ETmap->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerEtaPhi_ETmap) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerEtaPhi_ET_EMmap->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerEtaPhi_ET_EMmap) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerEtaPhi_ET_hadmap->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerEtaPhi_ET_hadmap) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerEtaPhi_ET_fwdmap->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerEtaPhi_ET_fwdmap) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerEtaPhi_BCIDmap->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerEtaPhi_BCIDmap) );

  name = Form("/OUTPUT/jTowers/%s",m_jTowerET_bcid->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerET_bcid ) );
  name = Form("/OUTPUT/jTowers/%s",m_jTowerET_mu->GetName() );
  CHECK( histSvc->regHist(name,m_jTowerET_mu ) );


  return StatusCode::SUCCESS;
}

StatusCode TowerPlots::DigiTruth(){

  const CaloCellContainer* DigiTruth = 0;
  CHECK( evtStore()->retrieve( DigiTruth, "AllCalo_DigiHSTruth")); 
  
  /*
  for(auto digi : *DigiTruth) {
    if(digi->e()<250)ATH_MSG_INFO("eta: " << digi->eta() << " phi: " << digi->phi() << " et " << digi->et() << " time: " << digi->time() ); 
  }
  */
  return StatusCode::SUCCESS; 

}

StatusCode TowerPlots::SuperCellPlots(){

  const CaloCellContainer* scells = 0;
  CHECK( evtStore()->retrieve( scells, m_SuperCellContainer.data() ) );
  
  int Nsc = 0; 

  

  for(auto scell : *scells) {
    Nsc ++; 
    float et = scell->et(); 
    float t = scell->time();

    int sampling = scell->caloDDE()->getSampling(); 

    if(et>10e3)m_scTimeRawET10->Fill(t);
    else m_scTimeRaw10ET->Fill(t);

    bool passTime = scell->provenance() & 0x200; 
    bool passPF = scell->provenance() & 0x40;
    
    //ATH_MSG_INFO("et, t " << et << "," << t << " sampling " << sampling << " Q " << scell->quality()  << " PF " << passPF << " Pass Timing " << passTime);

    if(et>10e3) {
      if(passTime && passPF ) m_scPFvsTimingET10->Fill("Time & PF",1); 
      if(passTime && !passPF) m_scPFvsTimingET10->Fill("Time & !PF",1); 
      if(!passTime && passPF ) m_scPFvsTimingET10->Fill("!Time & PF",1);
      if(!passTime && !passPF) m_scPFvsTimingET10->Fill("!Time & !PF",1);
    }
    else{
      if(passTime && passPF ) m_scPFvsTiming10ET->Fill("Time & PF",1);
      if(passTime && !passPF) m_scPFvsTiming10ET->Fill("Time & !PF",1);
      if(!passTime && passPF ) m_scPFvsTiming10ET->Fill("!Time & PF",1);
      if(!passTime && !passPF) m_scPFvsTiming10ET->Fill("!Time & !PF",1);
    }

    if(passTime){
      if(et>10e3)m_scTimeET10->Fill(t);
      else m_scTime10ET->Fill(t);

      m_scTimeET->Fill(et); 

      m_SCET_bcid->Fill(distFrontBunchTrain,et); 
      m_SCET_mu->Fill(mu,et); 
      
    }
    if(passPF){
      if(et>10e3)m_scTimePFET10->Fill(t);
      else m_scTimePF10ET->Fill(t);
      
      m_scPFET->Fill(et); 
    }
  }
  //ATH_MSG_INFO("Nsc: " << Nsc );
  return StatusCode::SUCCESS;

}

StatusCode TowerPlots::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode TowerPlots::PlotJTowers(){

  const xAOD::JGTowerContainer* jTowers =0;
  CHECK( evtStore()->retrieve( jTowers,"JTower"));

  unsigned int jsize = jTowers->size(); 

  for(unsigned int t = 0; t < jsize; t++){
    const xAOD::JGTower* tower = jTowers->at(t);
    //ATH_MSG_INFO("jTower: " << tower->eta() << " , " << tower->phi() << " , " << tower->et() ); 
    m_jTowerET->Fill(tower->et()*0.001 ); 
    // 0 = barrel EM. 1 = barrel had. 2 = forward.
    if(tower->sampling()==0)  m_jTowerET_EM->Fill(tower->et()*0.001 );
    if(tower->sampling()==1)  m_jTowerET_had->Fill(tower->et()*0.001 );
    if(tower->sampling()==2)  m_jTowerET_fwd->Fill(tower->et()*0.001 );

    m_jTowerEta->Fill(tower->eta() ); 
    m_jTowerPhi->Fill(tower->phi() ); 
    if(TMath::Abs(tower->eta())<2.4)m_jTowerET_BCID->Fill(distFrontBunchTrain,tower->et()*0.001); 
    m_jTowerEtaPhi_ETmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    if(tower->sampling()==0)  m_jTowerEtaPhi_ET_EMmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    if(tower->sampling()==1)  m_jTowerEtaPhi_ET_hadmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    if(tower->sampling()==2)  m_jTowerEtaPhi_ET_fwdmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    m_jTowerEtaPhi_BCIDmap->Fill(tower->eta(),tower->phi(),distFrontBunchTrain);
    
    float eta = fabs(tower->eta() ); 
    if(eta>2.4)continue; 
    m_jTowerET_bcid->Fill(distFrontBunchTrain, tower->et()); 
    m_jTowerET_mu->Fill(mu, tower->et());
  }


  return StatusCode::SUCCESS;


}

StatusCode TowerPlots::PlotGTowers(){

  const xAOD::JGTowerContainer* gTowers =0;
  CHECK( evtStore()->retrieve( gTowers,"GTower"));

  unsigned int gsize = gTowers->size();

  for(unsigned int t = 0; t < gsize; t++){
    const xAOD::JGTower* tower = gTowers->at(t);
    float eta = fabs(tower->eta()); 
    //if(tower->eta()<0.2 && tower->eta()>0 && tower->phi()>0 &&tower->phi()<0.2)
    //  ATH_MSG_INFO("gTower: " << tower->eta() << " , " << tower->phi() << " , " << tower->et() );
    if(eta<2.4) m_gTowerET->Fill(tower->et()*0.001 );

    // 0 = barrel EM. 1 = barrel had. 2 = forward.
    if(tower->sampling()==0)  m_gTowerET_EM->Fill(tower->et()*0.001 );
    if(tower->sampling()==1)  m_gTowerET_had->Fill(tower->et()*0.001 );
    if(tower->sampling()==2)  m_gTowerET_fwd->Fill(tower->et()*0.001 );

    m_gTowerEta->Fill(tower->eta() ); 
    m_gTowerPhi->Fill(tower->phi() );  
    if(eta<2.4)m_gTowerET_BCID->Fill(distFrontBunchTrain,tower->et()*0.001); 
    m_gTowerEtaPhi_ETmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    if(tower->sampling()==0)  m_gTowerEtaPhi_ET_EMmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    if(tower->sampling()==1)  m_gTowerEtaPhi_ET_hadmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);
    if(tower->sampling()==2)  m_gTowerEtaPhi_ET_fwdmap->Fill(tower->eta(),tower->phi(),tower->et()*0.001);

    m_gTowerEtaPhi_BCIDmap->Fill(tower->eta(),tower->phi(),distFrontBunchTrain);
    if(eta<2.4){
      m_gTowerET_bcid->Fill(distFrontBunchTrain, tower->et());
      m_gTowerET_mu->Fill(mu, tower->et());
    }

  }

  return StatusCode::SUCCESS;

}


StatusCode TowerPlots::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  mu = ei->averageInteractionsPerCrossing();
  bool isMC=false;
  isMC = (ei->eventType(xAOD::EventInfo::IS_SIMULATION) );
  ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
  distFrontBunchTrain = m_bcTool->distanceFromFront(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);

  if(isMC)CHECK(DigiTruth() ); 
  CHECK(SuperCellPlots() );
  CHECK(PlotJTowers() ); 
  CHECK(PlotGTowers() ); 


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode TowerPlots::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


