#ifndef NTUPLEDUMPER_COMPUTEJTOWERPARAMETERS_H
#define NTUPLEDUMPER_COMPUTEJTOWERPARAMETERS_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

//Example ROOT Includes
#include "TMath.h"
#include "TH1F.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigL1Calo/JGTowerContainer.h"
#include "GaudiKernel/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"

class ComputejTowerParameters: public ::AthAnalysisAlgorithm { 
 public: 
  ComputejTowerParameters( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ComputejTowerParameters(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   //TTree* m_myTree = 0;
  std::string m_depthCorrectionFile;
  bool m_doFromFile=false;

  TH1F* m_jTowerArea_hist=0;
  TH1F* m_jTowerArea_corr=0;
  TH1F* m_jTowerArea_final=0;

  std::vector <int> *m_HAD1towers = new std::vector <int>;
  std::vector <int> *m_HAD2towers = new std::vector <int>;
  std::vector <int> *m_HAD3towers = new std::vector <int>;
  std::vector <int> *m_HADtowers  = new std::vector <int>;
  std::vector <int> *m_EMtowers   = new std::vector <int>;
  std::vector <int> *m_HEC        = new std::vector <int>;
  std::vector <int> *m_FCAL1      = new std::vector <int>;
  std::vector <int> *m_FCAL2      = new std::vector <int>;
  std::vector <int> *m_FCAL3      = new std::vector <int>;
  std::vector <int> *m_fullFCAL   = new std::vector <int>;
  std::vector < float > m_jTowerArea;
  std::vector < float > m_jTowerArea_cor;

  TH1F* m_average_0=0;
  TH1F* m_average_weight_0=0;
  TH1F* m_rms_0=0;
  TH1F* m_rms_weight_0=0;
  TH1I* m_average_c0=0;
  TH1I* m_average_weight_c0=0;

  bool m_firstEvent=true;

}; 

#endif //> !NTUPLEDUMPER_COMPUTEJTOWERPARAMETERS_H
