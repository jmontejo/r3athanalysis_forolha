#ifndef NTUPLEDUMPER_SCPLOTS_H
#define NTUPLEDUMPER_SCPLOTS_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "CaloEvent/CaloCellContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
//Example ROOT Includes
#include "TH1F.h"
#include "TProfile.h"
#include "TString.h"

class SCPlots: public ::AthAnalysisAlgorithm { 
public:
    SCPlots( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~SCPlots();
    std::string m_SuperCellContainer;
    virtual StatusCode createProfile(TString Name, TString title, int Nx, double X0, double X1);
    virtual StatusCode PlotAverageET();
    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  execute();        //per event
    virtual StatusCode  finalize();       //once, after all events processed1
private:
    //ServiceHandle<ITHistSvc> histSvc;
    std::map<TString, TProfile*> prName;
    float distFrontBunchTrain;
    float mu;
}; 

#endif //> !NTUPLEDUMPER_SCPLOTS_H
