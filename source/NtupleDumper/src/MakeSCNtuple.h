#ifndef NTUPLEDUMPER_MAKESCNTUPLE_H
#define NTUPLEDUMPER_MAKESCNTUPLE_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "CaloEvent/CaloCellContainer.h"
//Example ROOT Includes
#include "TTree.h"
//#include "TH1D.h"



class MakeSCNtuple: public ::AthAnalysisAlgorithm { 
public:
    MakeSCNtuple( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~MakeSCNtuple();

    std::string m_SuperCellContainer;
    //IS EXECUTED:
    virtual StatusCode  initialize();     //once, before any input is loaded
    //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
    virtual StatusCode  execute();        //per event
    virtual StatusCode  FillTree(); //Fill supercell container into TTree
    virtual StatusCode  finalize();       //once, after all events processed

private:
    
    TTree*   m_tree=0;
    
    std::vector<float> scells_Et;
    std::vector<float> scells_eta;
    std::vector<float> scells_phi;
    std::vector<float> scells_time;
    std::vector<int>   scells_quality;
    std::vector<float> scells_provenance;
    std::vector<int>   scells_sampling;
    std::vector<bool>  scells_PF;
    std::vector<bool>  scells_Time;
    
    
}; 

#endif //> !NTUPLEDUMPER_MAKESCNTUPLE_H
