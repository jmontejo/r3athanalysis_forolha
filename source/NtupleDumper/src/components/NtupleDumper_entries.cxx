
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../NtupleDumperAlg.h"

DECLARE_ALGORITHM_FACTORY( NtupleDumperAlg )


#include "../TowerPlots.h"
DECLARE_ALGORITHM_FACTORY( TowerPlots )


#include "../ComputejTowerParameters.h"
DECLARE_ALGORITHM_FACTORY( ComputejTowerParameters )


#include "../ComputegTowerParameters.h"
DECLARE_ALGORITHM_FACTORY( ComputegTowerParameters )


#include "../SCPlots.h"
DECLARE_ALGORITHM_FACTORY( SCPlots )


#include "../MakeSCNtuple.h"
DECLARE_ALGORITHM_FACTORY( MakeSCNtuple )

DECLARE_FACTORY_ENTRIES( NtupleDumper ) 
{
  DECLARE_ALGORITHM( MakeSCNtuple );
  DECLARE_ALGORITHM( SCPlots );
  DECLARE_ALGORITHM( ComputegTowerParameters );
  DECLARE_ALGORITHM( ComputejTowerParameters );
  DECLARE_ALGORITHM( TowerPlots );
  DECLARE_ALGORITHM( NtupleDumperAlg );
}
