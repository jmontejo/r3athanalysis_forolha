// NtupleDumper includes
#include "ComputegTowerParameters.h"
//#include "xAODEventInfo/EventInfo.h"




ComputegTowerParameters::ComputegTowerParameters( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  declareProperty("maxEt",m_max_et=10e3);
  declareProperty("minEt",m_min_et=-99e6); 
  declareProperty("absEt",m_absEt=-1); 
  declareProperty("cutMidTrain", m_midTrain=true);

}


ComputegTowerParameters::~ComputegTowerParameters() {}

int ComputegTowerParameters::iEta(float eta){
  float eta_ = fabs(eta); 
  int ieta = h_eta->FindBin(eta_); 
  //if(eta>0)ieta+=21; 
    
  return ieta; 
}


StatusCode ComputegTowerParameters::createProfile(TString name, TString title, int Nx, float X0, float X1){
  TProfile *pr = new TProfile(name,title,Nx,X0,X1,"s");
  prName[name]=pr; 
  return StatusCode::SUCCESS; 
}

StatusCode ComputegTowerParameters::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  float eta [] = {0.0,0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.5,2.7,2.9,3.1,3.2,3.5,4.0,4.45,4.9};
  int N =sizeof(eta)/sizeof(float)-1; 
  h_eta = new TH1F("h_eta","",N,eta); 

  pr_eta = new TProfile("pr_eta","",N,0.5,N+0.5,"s"); 
  pr_eta_EM = new TProfile("pr_eta_EM","",N,0.5,N+0.5,"s");
  pr_eta_HAD = new TProfile("pr_eta_HAD","",N,0.5,N+0.5,"s");
  for(int i=1; i<=N; i++){
    pr_eta->GetXaxis()->SetBinLabel(i,Form("ieta%d",i)); 
    pr_eta_EM->GetXaxis()->SetBinLabel(i,Form("ieta%d",i));
    pr_eta_HAD->GetXaxis()->SetBinLabel(i,Form("ieta%d",i));
  }
  for(int i=1; i<=N; i++){
    CHECK(createProfile(Form("Rho_ieta%d",i),";#rho;#rho_#eta",100,-10e3,30e3));
  }

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  CHECK( histSvc.retrieve() );
  CHECK( histSvc->regHist("/OUTPUT/gTowerNoise/pr_eta",pr_eta)); 
  CHECK( histSvc->regHist("/OUTPUT/gTowerNoise/pr_eta_EM",pr_eta_EM));
  CHECK( histSvc->regHist("/OUTPUT/gTowerNoise/pr_eta_HAD",pr_eta_HAD));  
  for (std::map<TString,TProfile*>::iterator it=prName.begin(); it!=prName.end(); it++) {
    std::string name = Form("/OUTPUT/gTowerNoise/%s",it->first.Data()); 
    CHECK( histSvc->regHist(name,it->second) ); 
  }

  return StatusCode::SUCCESS;
}

StatusCode ComputegTowerParameters::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

float ComputegTowerParameters::Rho(std::vector<float> et){
  float rho = 0; 
  for(int i=0; i<et.size(); i++){
    rho+=et.at(i); 
  }
  return rho/et.size(); 
}

StatusCode ComputegTowerParameters::RetrieveRho(TString name){
  ATH_MSG_DEBUG("RetrieveMET " << name);

  const xAOD::EnergySumRoI* met(0);
  CHECK( evtStore()->retrieve(met, name.Data() ) );
  SG::AuxElement::Accessor<float> rhoA("RhoA");
  SG::AuxElement::Accessor<float> rhoB("RhoB");
  SG::AuxElement::Accessor<float> rhoC("RhoC");

  if(rhoA.isAvailable(*met)){
    m_rhoA = rhoA(*met);
    m_rhoB = rhoB(*met);
    m_rhoC = rhoC(*met);
  }
  return StatusCode::SUCCESS; 

}

StatusCode ComputegTowerParameters::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
  float distFrontBunchTrain = m_bcTool->distanceFromFront(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);
  float distEndBunchTrain = m_bcTool->distanceFromTail(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);

  if(m_midTrain == true){

    std::cout<<"Applying selection on middle of train"<<std::endl;
    if(distFrontBunchTrain < 21 || distEndBunchTrain < 9) return StatusCode::SUCCESS;
  }

  CHECK(Noise()); 
  CHECK(Rho_Eta());
  setFilterPassed(true); //if got here, assume that means algorithm passed                                                                                             
  return StatusCode::SUCCESS;
}

StatusCode ComputegTowerParameters::Noise(){  
  const xAOD::JGTowerContainer* gTowers =0;
  CHECK( evtStore()->retrieve( gTowers,"GTower"));

  for(const auto &gT : *gTowers){
    float eta = gT->eta();
    float phi = gT->phi(); 
    float et = gT->et(); 
    if(et>m_max_et)continue; 
    if(et<m_min_et)continue;
    if(m_absEt>0){
      if(fabs(et)<m_absEt)continue; 
    }
    int ieta = iEta(eta); 
    if(gT->sampling()==0)pr_eta_EM->Fill(ieta,et); 
    else pr_eta_HAD->Fill(ieta,et);     
  }
  const xAOD::JGTowerContainer* gCaloTowers =0;
  CHECK( evtStore()->retrieve( gCaloTowers,"gCaloTowers")); 

  std::vector<float> et_a; 
  std::vector<float> et_b;
  std::vector<float> et_c;

  for(const auto &gT : *gCaloTowers){
    float eta = gT->eta();
    float phi = gT->phi();
    float et = gT->et();
    if(et>m_max_et)continue;
    if(et<m_min_et)continue;
    if(m_absEt>0){
      if(fabs(et)<m_absEt)continue;
    }
    int ieta = iEta(eta);
    pr_eta->Fill(ieta,et);

    if(fabs(eta)>3.1)    et_c.push_back(et); 
    else if(eta>0 && eta<2.5) et_b.push_back(et); 
    else et_a.push_back(et); 
  }

  m_rhoA=Rho(et_a);
  m_rhoB=Rho(et_b);
  m_rhoC=Rho(et_c); 
  return StatusCode::SUCCESS;
}

StatusCode ComputegTowerParameters::Rho_Eta(){
  const xAOD::JGTowerContainer* gCaloTowers =0;
  CHECK( evtStore()->retrieve( gCaloTowers,"gCaloTowers"));
  for(const auto &gT : *gCaloTowers){
    float eta = gT->eta();
    float phi = gT->phi();
    float et = gT->et();
    if(et>m_max_et)continue;
    if(et<m_min_et)continue;
    if(m_absEt>0){
      if(fabs(et)<m_absEt)continue;
    }
    int ieta = iEta(eta);
    TString name = Form("Rho_ieta%d",ieta); 
    float rho = 0; 
    if(fabs(eta)>=2.5)  rho=m_rhoC; 
    else if(eta>0 && eta<2.5) rho=m_rhoB;
    else rho=m_rhoA;

    prName[name]->Fill(rho,et);
  }
  return StatusCode::SUCCESS;
}

StatusCode ComputegTowerParameters::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


