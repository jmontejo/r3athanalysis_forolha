// NtupleDumper includes
#include "ComputejTowerParameters.h"

//#include "xAODEventInfo/EventInfo.h"




ComputejTowerParameters::ComputejTowerParameters( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
  declareProperty( "DepthCorrectionFile", m_depthCorrectionFile = "", "Depth Correction File" ); //example property declaration
}

ComputejTowerParameters::~ComputejTowerParameters() {}

void calculateaveragerms(TH1F* hist, TH1F* hist2, TH1I* histc, int bin ,float x_n1 ) {
    int n=histc->GetBinContent(bin+1);
    float average_n=hist->GetBinContent(bin+1);
    float average_n1=(x_n1+n*average_n)/(n+1);
    hist->SetBinContent(bin+1,average_n1);
    float sigma_n=hist2->GetBinContent(bin+1);
    float sigma_n1=TMath::Sqrt((n*sigma_n*sigma_n+(x_n1-average_n1)*(x_n1-average_n) ) / (n+1));
    hist2->SetBinContent(bin+1,sigma_n1);
    histc->SetBinContent(bin+1,n+1);
}

StatusCode ComputejTowerParameters::initialize() {
    ATH_MSG_INFO ("Initializing " << name() << "...");
    //
    //This is called once, before the start of the event loop
    //Retrieves of tools you have configured in the joboptions go here
    //

    if (m_depthCorrectionFile != "") {
        m_doFromFile=true;
        ATH_MSG_INFO ("DepthCorrection " << name() << " from file" << m_depthCorrectionFile);
    }

    m_average_0         = new TH1F ( "average_0"         , "average_0"         , 7744 , 0. , 7744. ) ;
    m_average_weight_0  = new TH1F ( "average_weight_0"  , "average_weight_0"  , 7744 , 0. , 7744. ) ;
    m_rms_0             = new TH1F ( "rms_0"             , "rms_0"             , 7744 , 0. , 7744. ) ;
    m_rms_weight_0      = new TH1F ( "rms_weight_0"      , "rms_weight_0"      , 7744 , 0. , 7744. ) ;
    m_average_c0        = new TH1I ( "average_c0"        , "average_c0"        , 7744 , 0. , 7744. ) ;
    m_average_weight_c0 = new TH1I ( "average_weight_c0" , "average_weight_c0" , 7744 , 0. , 7744. ) ;
    
    m_jTowerArea_hist  = new TH1F ( "jTowerArea_hist"  , "" , 7744 , 0 , 7744 ) ;
    m_jTowerArea_final = new TH1F ( "jTowerArea_final" , "" , 7744 , 0 , 7744 ) ;
    m_jTowerArea_corr  = new TH1F ( "jTowerArea_corr"  , "" , 7744 , 0 , 7744 ) ;
    
    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK( histSvc.retrieve() );
    std::string nam = "";
    nam = Form("/OUTPUT/jTower/%s",m_jTowerArea_hist->GetName() ); 
    CHECK( histSvc->regHist(nam,m_jTowerArea_hist) );
    nam = Form("/OUTPUT/jTower/%s",m_jTowerArea_corr->GetName() ); 
    CHECK( histSvc->regHist(nam,m_jTowerArea_corr) );
    if (m_doFromFile) {
        nam = Form("/OUTPUT/jTower/%s",m_jTowerArea_final->GetName() ); 
        CHECK( histSvc->regHist(nam,m_jTowerArea_final) );
        ATH_MSG_INFO ("DepthCorrection " << name() << " into final historam");
    }

    nam = Form("/OUTPUT/jTower/%s",m_average_0->GetName() ); 
    CHECK( histSvc->regHist(nam,m_average_0) );
    nam = Form("/OUTPUT/jTower/%s",m_average_c0->GetName() ); 
    CHECK( histSvc->regHist(nam,m_average_c0) );
    nam = Form("/OUTPUT/jTower/%s",m_average_weight_c0->GetName() ); 
    CHECK( histSvc->regHist(nam,m_average_weight_c0) );
    nam = Form("/OUTPUT/jTower/%s",m_average_weight_0->GetName() ); 
    CHECK( histSvc->regHist(nam,m_average_weight_0) );
    nam = Form("/OUTPUT/jTower/%s",m_rms_weight_0->GetName() ); 
    CHECK( histSvc->regHist(nam,m_rms_weight_0) );
    nam = Form("/OUTPUT/jTower/%s",m_rms_0->GetName() ); 
    CHECK( histSvc->regHist(nam,m_rms_0) );

    return StatusCode::SUCCESS;
}

StatusCode ComputejTowerParameters::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    //
    //Things that happen once at the end of the event loop go here
    //
    TH1D * dhist=0;
    if (m_doFromFile) {
        TFile * fdepth=new TFile(TString(m_depthCorrectionFile));
        dhist=(TH1D*)fdepth->Get("Mean2_AreaNorm_CF_NoSpikes");
        dhist->SetDirectory(0);
        fdepth->Delete();
    }
    int correction_count=0;

    int depthoption=8;
    m_jTowerArea_cor.resize(7744,1.0);
    for (unsigned int i=0; i<m_jTowerArea.size(); i ++) {
        float value=1.0;
        if (m_doFromFile) {
            value=dhist->GetBinContent(i+1);
        }
        if ((depthoption & 0x1 ) !=0 ) {
            if (find(m_HAD1towers->begin(),m_HAD1towers->end(),i)!=m_HAD1towers->end()) {
                correction_count++;
                m_jTowerArea_cor[i]= 0.0;
                value=1.0;
            }
        }
        if ((depthoption & 0x2 ) !=0 ) {
            if (find(m_HAD2towers->begin(),m_HAD2towers->end(),i)!=m_HAD2towers->end()) {
                correction_count++;
                m_jTowerArea_cor[i]= 0.0;
                value=1.0;
            }
        }
        if ((depthoption & 0x4 ) !=0 ) {
            if (find(m_HAD3towers->begin(),m_HAD3towers->end(),i)!=m_HAD3towers->end()) {
                correction_count++;
                m_jTowerArea_cor[i]= 0.0;
                value=1.0;
            }
        }
        if ((depthoption & 0x8 ) !=0 ) {
            if (find(m_EMtowers->begin(),m_EMtowers->end(),i)!=m_EMtowers->end()) {
                correction_count++;
                m_jTowerArea_cor[i]= 0.0;
                value=1.0;
            }
        }
        if ((depthoption & 0x10 ) !=0 ) {
            if (find(m_fullFCAL->begin(),m_fullFCAL->end(),i)!=m_fullFCAL->end()) {
                correction_count++;
                m_jTowerArea_cor[i]= 0.0;
                value=1.0;
            }
        }
        m_jTowerArea[i]= m_jTowerArea[i]/value;
    }

    float renormfactor=1.0;

    renormfactor=1.0/m_jTowerArea[0];

    for (unsigned int i=0; i<m_jTowerArea.size(); i ++) {
        m_jTowerArea[i]= m_jTowerArea[i]*renormfactor;
    }

    int index=0;
    for ( const auto &area: m_jTowerArea ) {
        index+=1;
        m_jTowerArea_final->SetBinContent(index,area);
    }
    index=0;
    for ( const auto &area: m_jTowerArea_cor ) {
        index+=1;
        m_jTowerArea_corr->SetBinContent(index,area);
    }



    return StatusCode::SUCCESS;
}

StatusCode ComputejTowerParameters::execute() {  
    ATH_MSG_DEBUG ("Executing " << name() << "...");
    setFilterPassed(false); //optional: start with algorithm not passed

    //
    //Your main analysis code goes here
    //If you will use this algorithm to perform event skimming, you
    //should ensure the setFilterPassed method is called
    //If never called, the algorithm is assumed to have 'passed' by default
    //

    const xAOD::EventInfo* ei = 0;
    CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
    ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
    float distFrontBunchTrain = m_bcTool->distanceFromFront(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);

    if (distFrontBunchTrain < 21) {
        return StatusCode::SUCCESS;
    }

    const xAOD::JGTowerContainer* towers =0;
    CHECK( evtStore()->retrieve( towers,"JTower"));

    if (m_firstEvent) {
        m_firstEvent=false;
    for (unsigned int i=0; i <7744; i++){
        if ( i>=0    && i< 1696 ) m_EMtowers->push_back(i);
        if ( i>=3392 && i< 5088 ) m_EMtowers->push_back(i);
        if ( i>=6784 && i< 6816 ) m_EMtowers->push_back(i);
        if ( i>=6848 && i< 6880 ) m_EMtowers->push_back(i);
        if ( i>=6912 && i< 6976 ) m_EMtowers->push_back(i);
        if ( i>=1696 && i< 3392 ) m_HADtowers->push_back(i);
        if ( i>=5088 && i< 6784 ) m_HADtowers->push_back(i);
        if ( i>=6816 && i< 6848 ) m_HADtowers->push_back(i);
        if ( i>=6880 && i< 6912 ) m_HADtowers->push_back(i);
        if ( i>=6976 && i< 7168 ) m_FCAL1->push_back(i);
        if ( i>=7168 && i< 7296 ) m_FCAL2->push_back(i);
        if ( i>=7296 && i< 7360 ) m_FCAL3->push_back(i);
        if ( i>=7360 && i< 7552 ) m_FCAL1->push_back(i);
        if ( i>=7552 && i< 7680 ) m_FCAL2->push_back(i);
        if ( i>=7680 && i< 7744 ) m_FCAL3->push_back(i);
    }

        m_HAD1towers->clear();
        m_HAD2towers->clear();
        m_HAD3towers->clear();
        for (const auto &i : *m_HADtowers) {
            const xAOD::JGTower* tower = towers->at(i);
            float aeta = fabs(tower->eta());
            if (aeta< 1.5) m_HAD1towers->push_back(i);
            else if (aeta< 1.6) m_HAD2towers->push_back(i);
            else m_HAD3towers->push_back(i);
        }

        m_fullFCAL->reserve(m_fullFCAL->size() + distance(m_FCAL1->begin(), m_FCAL1->end()));
        m_fullFCAL->insert(m_fullFCAL->end(), m_FCAL1->begin(), m_FCAL1->end());
        m_fullFCAL->reserve(m_fullFCAL->size() + distance(m_FCAL2->begin(), m_FCAL2->end()));
        m_fullFCAL->insert(m_fullFCAL->end(), m_FCAL2->begin(), m_FCAL2->end());
        m_fullFCAL->reserve(m_fullFCAL->size() + distance(m_FCAL3->begin(), m_FCAL3->end()));
        m_fullFCAL->insert(m_fullFCAL->end(), m_FCAL3->begin(), m_FCAL3->end());

        m_jTowerArea.resize(7744,1.0);
        for (const auto &i: *m_EMtowers) {
            const xAOD::JGTower* tower = towers->at(i);
            float aeta = fabs(tower->eta());
            if (aeta < 2.5 ) m_jTowerArea[i]=0.1*0.1;    
            else if ( fabs(aeta - 3.15)< 0.01 ) m_jTowerArea[i]=0.2*0.1;
            else m_jTowerArea[i]=0.2*0.2;
        }
        for (const auto &i: *m_HADtowers) {
            const xAOD::JGTower* tower = towers->at(i);
            float aeta = fabs(tower->eta());
            if ( aeta  < 2.5 ) m_jTowerArea[i]=0.1*0.1;    
            else if ( fabs(aeta - 3.15)< 0.01 ) m_jTowerArea[i]=0.1*0.2;
            else m_jTowerArea[i]=0.2*0.2;
        }
        int counter = -1;
        for (const auto &i: *m_FCAL1) {
            counter++;
//     0    3.15409199    0.19050542    0.03324200
//     0    3.15747455    0.59049925    0.03914100
//     0    3.16066743    0.99165041    0.03968200
//     0    3.16365642    1.38220771    0.04280500
//     0    3.16178576    1.77069399    0.04499600
//     0    3.16675654    2.15735748    0.04353500
//     0    3.15735743    2.55091933    0.03886700
//     0    3.14770533    2.93258334    0.02929200
//
//     0    3.24873755    0.19661163    0.04190000
            switch (counter) {
                case 0   :
                case 8   :
                case 192 :
                case 200 : m_jTowerArea[i]=0.03324200; break;
                case 1   :
                case 9   :
                case 193 :
                case 201 : m_jTowerArea[i]=0.03914100; break;
                case 2   : 
                case 10  : 
                case 194 : 
                case 202 : m_jTowerArea[i]=0.03968200; break;
                case 3   : 
                case 11  : 
                case 195 : 
                case 203 : m_jTowerArea[i]=0.04280500; break;
                case 4   : 
                case 12  : 
                case 196 : 
                case 204 : m_jTowerArea[i]=0.04499600; break;
                case 5   : 
                case 13  : 
                case 197 :
                case 205 : m_jTowerArea[i]=0.04353500; break;
                case 6   : 
                case 14  :
                case 198 :
                case 206 : m_jTowerArea[i]=0.03886700; break;
                case 7   :
                case 15  :
                case 199 :
                case 207 : m_jTowerArea[i]=0.02929200; break;
                case 16   : 
                case 24   : 
                case 208  : 
                case 216  : m_jTowerArea[i]=0.04190000; break;
                case 17   : 
                case 25   : 
                case 209  : 
                case 217  : m_jTowerArea[i]=0.04343000; break;
                case 18   : 
                case 26   : 
                case 210  : 
                case 218  : m_jTowerArea[i]=0.04397700; break;
                case 19   : 
                case 27   : 
                case 211  : 
                case 219  : m_jTowerArea[i]=0.04509800; break;
                case 20   : 
                case 28   : 
                case 212  : 
                case 220  : m_jTowerArea[i]=0.04506500; break;
                case 21   : 
                case 29   : 
                case 213  : 
                case 221  : m_jTowerArea[i]=0.04528000; break;
                case 22   : 
                case 30   : 
                case 214  : 
                case 222  : m_jTowerArea[i]=0.04344400; break;
                case 23   : 
                case 31   : 
                case 215  : 
                case 223  : m_jTowerArea[i]=0.04117100; break;
                case 32   : 
                case 40   : 
                case 224  : 
                case 232  : m_jTowerArea[i]=0.02527600; break;
                case 33   : 
                case 41   : 
                case 225  : 
                case 233  : m_jTowerArea[i]=0.02542000; break;
                case 34   : 
                case 42   : 
                case 226  : 
                case 234  : m_jTowerArea[i]=0.02623800; break;
                case 35   : 
                case 43   : 
                case 227  : 
                case 235  : m_jTowerArea[i]=0.02739700; break;
                case 36   : 
                case 44   : 
                case 228  : 
                case 236  : m_jTowerArea[i]=0.02736000; break;
                case 37   : 
                case 45   : 
                case 229  : 
                case 237  : m_jTowerArea[i]=0.02700600; break;
                case 38   : 
                case 46   : 
                case 230  : 
                case 238  : m_jTowerArea[i]=0.02541300; break;
                case 39   : 
                case 47   : 
                case 231  : 
                case 239  : m_jTowerArea[i]=0.02487500; break;
                case 48   : 
                case 56   : 
                case 240  : 
                case 248  : m_jTowerArea[i]=0.02953200; break;
                case 49   : 
                case 57   : 
                case 241  : 
                case 249  : m_jTowerArea[i]=0.02928400; break;
                case 50   : 
                case 58   : 
                case 242  : 
                case 250  : m_jTowerArea[i]=0.02988000; break;
                case 51   : 
                case 59   : 
                case 243  : 
                case 251  : m_jTowerArea[i]=0.03200200; break;
                case 52   : 
                case 60   : 
                case 244  : 
                case 252  : m_jTowerArea[i]=0.03198600; break;
                case 53   : 
                case 61   : 
                case 245  : 
                case 253  : m_jTowerArea[i]=0.03131100; break;
                case 54   : 
                case 62   : 
                case 246  : 
                case 254  : m_jTowerArea[i]=0.02929700; break;
                case 55   : 
                case 63   : 
                case 247  : 
                case 255  : m_jTowerArea[i]=0.02832500; break;
                case 64   : 
                case 72   : 
                case 256  : 
                case 264  : m_jTowerArea[i]=0.03486700; break;
                case 65   : 
                case 73   : 
                case 257  : 
                case 265  : m_jTowerArea[i]=0.03331100; break;
                case 66   : 
                case 74   : 
                case 258  : 
                case 266  : m_jTowerArea[i]=0.03619000; break;
                case 67   : 
                case 75   : 
                case 259  : 
                case 267  : m_jTowerArea[i]=0.03792000; break;
                case 68   : 
                case 76   : 
                case 260  : 
                case 268  : m_jTowerArea[i]=0.03786800; break;
                case 69   : 
                case 77   : 
                case 261  : 
                case 269  : m_jTowerArea[i]=0.03809100; break;
                case 70   : 
                case 78   : 
                case 262  : 
                case 270  : m_jTowerArea[i]=0.03329600; break;
                case 71   : 
                case 79   : 
                case 263  : 
                case 271  : m_jTowerArea[i]=0.03353200; break;
                case 80   : 
                case 88   : 
                case 272  : 
                case 280  : m_jTowerArea[i]=0.04290100; break;
                case 81   : 
                case 89   : 
                case 273  : 
                case 281  : m_jTowerArea[i]=0.03997300; break;
                case 82   : 
                case 90   : 
                case 274  : 
                case 282  : m_jTowerArea[i]=0.04311000; break;
                case 83   : 
                case 91   : 
                case 275  : 
                case 283  : m_jTowerArea[i]=0.04746300; break;
                case 84   : 
                case 92   : 
                case 276  : 
                case 284  : m_jTowerArea[i]=0.04740300; break;
                case 85   : 
                case 93   : 
                case 277  : 
                case 285  : m_jTowerArea[i]=0.04484700; break;
                case 86   : 
                case 94   : 
                case 278  : 
                case 286  : m_jTowerArea[i]=0.04000700; break;
                case 87   : 
                case 95   : 
                case 279  : 
                case 287  : m_jTowerArea[i]=0.04099600; break;
                case 96   : 
                case 104  : 
                case 288  : 
                case 296  : m_jTowerArea[i]=0.05398100; break;
                case 97   : 
                case 105  : 
                case 289  : 
                case 297  : m_jTowerArea[i]=0.05240300; break;
                case 98   : 
                case 106  : 
                case 290  : 
                case 298  : m_jTowerArea[i]=0.05301200; break;
                case 99   : 
                case 107  : 
                case 291  : 
                case 299  : m_jTowerArea[i]=0.06371200; break;
                case 100  : 
                case 108  : 
                case 292  : 
                case 300  : m_jTowerArea[i]=0.06368800; break;
                case 101  : 
                case 109  : 
                case 293  : 
                case 301  : m_jTowerArea[i]=0.05699400; break;
                case 102  : 
                case 110  : 
                case 294  : 
                case 302  : m_jTowerArea[i]=0.05246500; break;
                case 103  : 
                case 111  : 
                case 295  : 
                case 303  : m_jTowerArea[i]=0.05134600; break;
                case 112  : 
                case 120  : 
                case 304  : 
                case 312  : m_jTowerArea[i]=0.07249000; break;
                case 113  : 
                case 121  : 
                case 305  : 
                case 313  : m_jTowerArea[i]=0.07450000; break;
                case 114  : 
                case 122  : 
                case 306  : 
                case 314  : m_jTowerArea[i]=0.07271000; break;
                case 115  : 
                case 123  : 
                case 307  : 
                case 315  : m_jTowerArea[i]=0.09297900; break;
                case 116  : 
                case 124  : 
                case 308  : 
                case 316  : m_jTowerArea[i]=0.09292000; break;
                case 117  : 
                case 125  : 
                case 309  : 
                case 317  : m_jTowerArea[i]=0.07976100; break;
                case 118  : 
                case 126  : 
                case 310  : 
                case 318  : m_jTowerArea[i]=0.07459900; break;
                case 119  : 
                case 127  : 
                case 311  : 
                case 319  : m_jTowerArea[i]=0.06877200; break;
                case 128  : 
                case 136  : 
                case 320  : 
                case 328  : m_jTowerArea[i]=0.11909800; break;
                case 129  : 
                case 137  : 
                case 321  : 
                case 329  : m_jTowerArea[i]=0.12975300; break;
                case 130  : 
                case 138  : 
                case 322  : 
                case 330  : m_jTowerArea[i]=0.10959400; break;
                case 131  : 
                case 139  : 
                case 323  : 
                case 331  : m_jTowerArea[i]=0.09556000; break;
                case 132  : 
                case 140  : 
                case 324  : 
                case 332  : m_jTowerArea[i]=0.09557900; break;
                case 133  : 
                case 141  : 
                case 325  : 
                case 333  : m_jTowerArea[i]=0.12759000; break;
                case 134  : 
                case 142  : 
                case 326  : 
                case 334  : m_jTowerArea[i]=0.12998000; break;
                case 135  : 
                case 143  : 
                case 327  : 
                case 335  : m_jTowerArea[i]=0.10134100; break;
                case 144  : 
                case 152  : 
                case 336  : 
                case 344  : m_jTowerArea[i]=0.08289900; break;
                case 145  : 
                case 153  : 
                case 337  : 
                case 345  : m_jTowerArea[i]=0.05157900; break;
                case 146  : 
                case 154  : 
                case 338  : 
                case 346  : m_jTowerArea[i]=0.08051000; break;
                case 147  : 
                case 155  : 
                case 339  : 
                case 347  : m_jTowerArea[i]=0.05267700; break;
                case 148  : 
                case 156  : 
                case 340  : 
                case 348  : m_jTowerArea[i]=0.05273300; break;
                case 149  : 
                case 157  : 
                case 341  : 
                case 349  : m_jTowerArea[i]=0.05601100; break;
                case 150  : 
                case 158  : 
                case 342  : 
                case 350  : m_jTowerArea[i]=0.04984400; break;
                case 151  : 
                case 159  : 
                case 343  : 
                case 351  : m_jTowerArea[i]=0.11185000; break;
                case 160  : 
                case 168  : 
                case 352  : 
                case 360  : m_jTowerArea[i]=0.07018000; break;
                case 161  : 
                case 169  : 
                case 353  : 
                case 361  : m_jTowerArea[i]=0.07437000; break;
                case 162  : 
                case 170  : 
                case 354  : 
                case 362  : m_jTowerArea[i]=0.07262900; break;
                case 163  : 
                case 171  : 
                case 355  : 
                case 363  : m_jTowerArea[i]=0.06894200; break;
                case 164  : 
                case 172  : 
                case 356  : 
                case 364  : m_jTowerArea[i]=0.06901900; break;
                case 165  : 
                case 173  : 
                case 357  : 
                case 365  : m_jTowerArea[i]=0.07994600; break;
                case 166  : 
                case 174  : 
                case 358  : 
                case 366  : m_jTowerArea[i]=0.06770100; break;
                case 167  : 
                case 175  : 
                case 359  : 
                case 367  : m_jTowerArea[i]=0.07418500; break;
                case 176  : 
                case 184  : 
                case 368  : 
                case 376  : m_jTowerArea[i]=0.07743100; break;
                case 177  : 
                case 185  : 
                case 369  : 
                case 377  : m_jTowerArea[i]=0.08463100; break;
                case 178  : 
                case 186  : 
                case 370  : 
                case 378  : m_jTowerArea[i]=0.07676800; break;
                case 179  : 
                case 187  : 
                case 371  : 
                case 379  : m_jTowerArea[i]=0.07354700; break;
                case 180  : 
                case 188  : 
                case 372  : 
                case 380  : m_jTowerArea[i]=0.07357600; break;
                case 181  : 
                case 189  : 
                case 373  : 
                case 381  : m_jTowerArea[i]=0.08243700; break;
                case 182  : 
                case 190  : 
                case 374  : 
                case 382  : m_jTowerArea[i]=0.07513000; break;
                case 183  : 
                case 191  : 
                case 375  : 
                case 383  : m_jTowerArea[i]=0.08083900; break;
            }
        }

        counter = -1;
        for (const auto &i: *m_FCAL2) {
            counter++;
            switch (counter) {
                case 0   :
                case 8   :
                case 128 :
                case 136 : m_jTowerArea[i]= 0.01287800; break;
                case 1   :
                case 9   :
                case 129 :
                case 137 : m_jTowerArea[i]= 0.00750400; break;
                case 2   :
                case 10  :
                case 130 :
                case 138 : m_jTowerArea[i]= 0.00734600; break;
                case 3   :
                case 11  :
                case 131 :
                case 139 : m_jTowerArea[i]= 0.03110300; break;
                case 4   :
                case 12  :
                case 132 :
                case 140 : m_jTowerArea[i]= 0.03111200; break;
                case 5   :
                case 13  :
                case 133 :
                case 141 : m_jTowerArea[i]= 0.00727100; break;
                case 6   :
                case 14  :
                case 134 :
                case 142 : m_jTowerArea[i]= 0.00743000; break;
                case 7   :
                case 15  :
                case 135 :
                case 143 : m_jTowerArea[i]= 0.01300400; break;
                case 16  :
                case 24  :
                case 144 :
                case 152 : m_jTowerArea[i]= 0.03259300; break;
                case 17  :
                case 25  :
                case 145 :
                case 153 : m_jTowerArea[i]= 0.03297300; break;
                case 18  :
                case 26  :
                case 146 :
                case 154 : m_jTowerArea[i]= 0.01960800; break;
                case 19  :
                case 27  :
                case 147 :
                case 155 : m_jTowerArea[i]= 0.03597100; break;
                case 20  :
                case 28  :
                case 148 :
                case 156 : m_jTowerArea[i]= 0.03591700; break;
                case 21  :
                case 29  :
                case 149 :
                case 157 : m_jTowerArea[i]= 0.01969900; break;
                case 22  :
                case 30  :
                case 150 :
                case 158 : m_jTowerArea[i]= 0.03305500; break;
                case 23  :
                case 31  :
                case 151 :
                case 159 : m_jTowerArea[i]= 0.03281100; break;
                case 32  :
                case 40  :
                case 160 :
                case 168 : m_jTowerArea[i]= 0.03905200; break;
                case 33  :
                case 41  :
                case 161 :
                case 169 : m_jTowerArea[i]= 0.03875800; break;
                case 34  :
                case 42  :
                case 162 :
                case 170 : m_jTowerArea[i]= 0.03695000; break;
                case 35  :
                case 43  :
                case 163 :
                case 171 : m_jTowerArea[i]= 0.04362200; break;
                case 36  :
                case 44  :
                case 164 :
                case 172 : m_jTowerArea[i]= 0.04356200; break;
                case 37  :
                case 45  :
                case 165 :
                case 173 : m_jTowerArea[i]= 0.03697300; break;
                case 38  :
                case 46  :
                case 166 :
                case 174 : m_jTowerArea[i]= 0.03878300; break;
                case 39  :
                case 47  :
                case 167 :
                case 175 : m_jTowerArea[i]= 0.03930700; break;
                case 48  :
                case 56  :
                case 176 :
                case 184 : m_jTowerArea[i]= 0.04965400; break;
                case 49  :
                case 57  :
                case 177 :
                case 185 : m_jTowerArea[i]= 0.04675700; break;
                case 50  :
                case 58  :
                case 178 :
                case 186 : m_jTowerArea[i]= 0.04611400; break;
                case 51  :
                case 59  :
                case 179 :
                case 187 : m_jTowerArea[i]= 0.05574200; break;
                case 52  :
                case 60  :
                case 180 :
                case 188 : m_jTowerArea[i]= 0.05570700; break;
                case 53  :
                case 61  :
                case 181 :
                case 189 : m_jTowerArea[i]= 0.04612500; break;
                case 54  :
                case 62  :
                case 182 :
                case 190 : m_jTowerArea[i]= 0.04676300; break;
                case 55  :
                case 63  :
                case 183 :
                case 191 : m_jTowerArea[i]= 0.05001100; break;
                case 64  :
                case 72  :
                case 192 :
                case 200 : m_jTowerArea[i]= 0.06417400; break;
                case 65  :
                case 73  :
                case 193 :
                case 201 : m_jTowerArea[i]= 0.06247000; break;
                case 66  :
                case 74  :
                case 194 :
                case 202 : m_jTowerArea[i]= 0.06263700; break;
                case 67  :
                case 75  :
                case 195 :
                case 203 : m_jTowerArea[i]= 0.07577900; break;
                case 68  :
                case 76  :
                case 196 :
                case 204 : m_jTowerArea[i]= 0.07573300; break;
                case 69  :
                case 77  :
                case 197 :
                case 205 : m_jTowerArea[i]= 0.06266800; break;
                case 70  :
                case 78  :
                case 198 :
                case 206 : m_jTowerArea[i]= 0.06252000; break;
                case 71  :
                case 79  :
                case 199 :
                case 207 : m_jTowerArea[i]= 0.06517900; break;
                case 80  :
                case 88  :
                case 208 :
                case 216 : m_jTowerArea[i]= 0.10180100; break;
                case 81  :
                case 89  :
                case 209 :
                case 217 : m_jTowerArea[i]= 0.09058800; break;
                case 82  :
                case 90  :
                case 210 :
                case 218 : m_jTowerArea[i]= 0.09311400; break;
                case 83  :
                case 91  :
                case 211 :
                case 219 : m_jTowerArea[i]= 0.11858100; break;
                case 84  :
                case 92  :
                case 212 :
                case 220 : m_jTowerArea[i]= 0.11861800; break;
                case 85  :
                case 93  :
                case 213 :
                case 221 : m_jTowerArea[i]= 0.09318100; break;
                case 86  :
                case 94  :
                case 214 :
                case 222 : m_jTowerArea[i]= 0.09067300; break;
                case 87  :
                case 95  :
                case 215 :
                case 223 : m_jTowerArea[i]= 0.10354300; break;
                case 96  :
                case 104 :
                case 224 :
                case 232 : m_jTowerArea[i]= 0.23966800; break;
                case 97  :
                case 105 :
                case 225 :
                case 233 : m_jTowerArea[i]= 0.18631100; break;
                case 98  :
                case 106 :
                case 226 :
                case 234 : m_jTowerArea[i]= 0.19910800; break;
                case 99  :
                case 107 :
                case 227 :
                case 235 : m_jTowerArea[i]= 0.18372400; break;
                case 100 :
                case 108 :
                case 228 :
                case 236 : m_jTowerArea[i]= 0.18351500; break;
                case 101 :
                case 109 :
                case 229 :
                case 237 : m_jTowerArea[i]= 0.19955700; break;
                case 102 :
                case 110 :
                case 230 :
                case 238 : m_jTowerArea[i]= 0.18664200; break;
                case 103 :
                case 111 :
                case 231 :
                case 239 : m_jTowerArea[i]= 0.24767500; break;
                case 112 :
                case 120 :
                case 240 :
                case 248 : m_jTowerArea[i]= 0.07747400; break;
                case 113 :
                case 121 :
                case 241 :
                case 249 : m_jTowerArea[i]= 0.19241300; break;
                case 114 :
                case 122 :
                case 242 :
                case 250 : m_jTowerArea[i]= 0.20799600; break;
                case 115 :
                case 123 :
                case 243 :
                case 251 : m_jTowerArea[i]= 0.09990800; break;
                case 116 :
                case 124 :
                case 244 :
                case 252 : m_jTowerArea[i]= 0.10005400; break;
                case 117 :
                case 125 :
                case 245 :
                case 253 : m_jTowerArea[i]= 0.21823100; break;
                case 118 :
                case 126 :
                case 246 :
                case 254 : m_jTowerArea[i]= 0.18142400; break;
                case 119 :
                case 127 :
                case 247 :
                case 255 : m_jTowerArea[i]= 0.08090700; break;
            }
        }

        counter = -1;
        for (const auto &i: *m_FCAL3) {
            counter++;
            switch (counter) {
                case 0  :
                case 8  :
                case 64  :
                case 72  : m_jTowerArea[i]=0.04735900 ; break;
                case 1  :
                case 9  :
                case 65  :
                case 73  : m_jTowerArea[i]=0.04906300 ; break;
                case 2  :
                case 10 :
                case 66  :
                case 74  : m_jTowerArea[i]=0.06104200 ; break;
                case 3  :
                case 11 :
                case 67  :
                case 75  : m_jTowerArea[i]=0.03657900 ; break;
                case 4  :
                case 12 :
                case 68  :
                case 76  : m_jTowerArea[i]=0.03664100 ; break;
                case 5  :
                case 13 :
                case 69  :
                case 77  : m_jTowerArea[i]=0.06120200 ; break;
                case 6  :
                case 14 :
                case 70  :
                case 78  : m_jTowerArea[i]=0.06241900 ; break;
                case 7  :
                case 15 :
                case 71  :
                case 79  : m_jTowerArea[i]=0.04837700 ; break;
                case 16 :
                case 24 :
                case 80  :
                case 88  : m_jTowerArea[i]=0.08324100 ; break;
                case 17 :
                case 25 :
                case 81  :
                case 89  : m_jTowerArea[i]=0.08274300 ; break;
                case 18 :
                case 26 :
                case 82  :
                case 90  : m_jTowerArea[i]=0.08794400 ; break;
                case 19 :
                case 27 :
                case 83  :
                case 91  : m_jTowerArea[i]=0.07406900 ; break;
                case 20 :
                case 28 :
                case 84  :
                case 92  : m_jTowerArea[i]=0.07397500 ; break;
                case 21 :
                case 29 :
                case 85  :
                case 93  : m_jTowerArea[i]=0.08799200 ; break;
                case 22 :
                case 30 :
                case 86  :
                case 94  : m_jTowerArea[i]=0.09550300 ; break;
                case 23 :
                case 31 :
                case 87  :
                case 95  : m_jTowerArea[i]=0.08384700 ; break;
                case 32 :
                case 40 :
                case 96  :
                case 104 : m_jTowerArea[i]=0.13624400 ; break;
                case 33 :
                case 41 :
                case 97  :
                case 105 : m_jTowerArea[i]=0.15696500 ; break;
                case 34 :
                case 42 :
                case 98  :
                case 106 : m_jTowerArea[i]=0.15919000 ; break;
                case 35 :
                case 43 :
                case 99  :
                case 107 : m_jTowerArea[i]=0.12587400 ; break;
                case 36 :
                case 44 :
                case 100 :
                case 108 : m_jTowerArea[i]=0.12579600 ; break;
                case 37 :
                case 45 :
                case 101 :
                case 109 : m_jTowerArea[i]=0.15934400 ; break;
                case 38 :
                case 46 :
                case 102 :
                case 110 : m_jTowerArea[i]=0.20798500 ; break;
                case 39 :
                case 47 :
                case 103 :
                case 111 : m_jTowerArea[i]=0.13860500 ; break;
                case 48 :
                case 56 :
                case 112 :
                case 120 : m_jTowerArea[i]=0.32099100 ; break;
                case 49 :
                case 57 :
                case 113 :
                case 121 : m_jTowerArea[i]=0.29283900 ; break;
                case 50 :
                case 58 :
                case 114 :
                case 122 : m_jTowerArea[i]=0.31615100 ; break;
                case 51 :
                case 59 :
                case 115 :
                case 123 : m_jTowerArea[i]=0.37949100 ; break;
                case 52 :
                case 60 :
                case 116 :
                case 124 : m_jTowerArea[i]=0.37963900 ; break;
                case 53 :
                case 61 :
                case 117 :
                case 125 : m_jTowerArea[i]=0.26171100 ; break;
                case 54 :
                case 62 :
                case 118 :
                case 126 : m_jTowerArea[i]=0.26604300 ; break;
                case 55 :
                case 63 :
                case 119 :
                case 127 : m_jTowerArea[i]=0.33325800 ; break;
            }
        }

        int index=0;
        for ( const auto &area: m_jTowerArea ) {
            index+=1;
            m_jTowerArea_hist->SetBinContent(index,area);
        }
    }

    for(unsigned int i=0; i<towers->size(); i++){
        // zero tower energy calculation
        const xAOD::JGTower* tower = towers->at(i);
        float Et =tower->et();
        float Et_area = Et / m_jTowerArea[i];
        if (Et>=0) {
            calculateaveragerms ( m_average_0        , m_rms_0        , m_average_c0        , i , Et      ) ;
            calculateaveragerms ( m_average_weight_0 , m_rms_weight_0 , m_average_weight_c0 , i , Et_area ) ;
        }
    }

    setFilterPassed(true); //if got here, assume that means algorithm passed
    return StatusCode::SUCCESS;
}

StatusCode ComputejTowerParameters::beginInputFile() { 
    //
    //This method is called at the start of each input file, even if
    //the input file contains no events. Accumulate metadata information here
    //

    //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
    // const xAOD::CutBookkeeperContainer* bks = 0;
    // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

    //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
    //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
    //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

    return StatusCode::SUCCESS;
}


