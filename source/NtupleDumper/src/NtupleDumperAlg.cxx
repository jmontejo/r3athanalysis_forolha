// NtupleDumper includes

#include "NtupleDumperAlg.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "TrigT1Interfaces/L1METvalue.h"
#include "xAODCore/ShallowCopy.h"
//#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigCalo/TrigEMClusterContainer.h"
#include "xAODTrigEgamma/TrigElectronContainer.h"

NtupleDumperAlg::NtupleDumperAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ){

  declareProperty( "useRun2L1"    ,  m_useRun2L1=true);
  declareProperty( "dumpTrigDec",  m_dumpTrigDec=true); 
  declareProperty( "dumpTauAlgs",  m_dumpTauAlgs=true); 
  declareProperty( "dumpTruthTaus", m_dumpTruthTaus=true);
  declareProperty( "dumpTruthPhs", m_dumpTruthPhs=true);
  declareProperty( "dumpEleAlgs",  m_dumpEleAlgs=true);
  declareProperty( "dumpTruthEles", m_dumpTruthEles=true);
  declareProperty( "dumpOffJets",  m_dumpOffJets=true);
  declareProperty( "dumpOnJets",  m_dumpOnJets=true);
  declareProperty( "applyJVT",    m_applyJVT=true); 
  declareProperty( "dumpOffEles", m_dumpOffEles=true);
  declareProperty( "dumpOffPhs", m_dumpOffPhs=true);
  declareProperty( "dumpTruthJets",m_dumpTruthJets=true);
  declareProperty( "dumpTruthMet", m_dumpTruthMet=true);
  declareProperty( "dumpClusters", m_dumpClusters=false);
  declareProperty( "dumpTowers",   m_dumpTowers=false); 
  declareProperty( "dumpRun2Towers", m_dumpRun2Towers=false); 
  declareProperty( "dumpHltMET",    m_dumpHltMet=true);
  declareProperty( "dumpHLTPhs", m_dumpHltPhs=true);
  declareProperty( "applyFilter",   m_applyFilter=true); 
  declareProperty( "HLTmet_list",    HltMlist); 
  declareProperty( "met_list",     Mlist); 
  declareProperty( "jet_list",     Jlist); 
  declareProperty( "trigger_list",     triggerlist);
  declareProperty( "isMC",          m_isMC); 
}

NtupleDumperAlg::~NtupleDumperAlg() {}

StatusCode NtupleDumperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  
 if(!m_isMC){
     m_dumpTruthTaus=false;
     m_dumpTruthJets=false;
     m_dumpTruthMet=false;
     m_dumpTruthPhs=false;
     m_dumpTruthEles=false;
     m_applyFilter=false;
 }
  if(m_dumpTrigDec) CHECK( m_trigDecTool.initialize() );

  m_jetCalibration.setTypeAndName("JetCalibrationTool/myJESTool");
  CHECK( m_jetCalibration.retrieve() );

  m_jvt.setTypeAndName("JetVertexTaggerTool/JVT");
  CHECK( m_jvt.retrieve() );

  CHECK( iso_tool.setProperty("ElectronWP","Gradient") );
  CHECK( iso_tool.setProperty("PhotonWP","FixedCutLoose") );
  CHECK( iso_tool.initialize() ); 

  //convert input strings to TStrings because things are weird 
  for(unsigned int i=0; i<Mlist.size(); i++)m_met_list.push_back(Mlist[i]); 
  for(unsigned int j=0; j<Jlist.size(); j++)m_jet_list.push_back(Jlist[j]); 
  for(unsigned int k=0; k<HltMlist.size(); k++)m_hlt_met_list.push_back(HltMlist[k]); 

  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  CHECK( histSvc.retrieve() );

  m_tree = new TTree("ntuple","ntuple");
  
  for(unsigned int i=0; i<m_met_list.size();i++){
    ATH_MSG_INFO(m_met_list[i]); 
    m_met_et[m_met_list[i] ]=0;
    m_met_phi[m_met_list[i] ]=0;
    m_jwoj_mht[m_met_list[i] ] = 0;
    m_jwoj_mst[m_met_list[i] ] = 0;
    m_tree->Branch(m_met_list[i] + "_et",&m_met_et[m_met_list[i] ]);
    m_tree->Branch(m_met_list[i] + "_phi",&m_met_phi[m_met_list[i] ]);
    if(m_met_list[i].Contains("EventVariables")){
      m_tree->Branch(m_met_list[i]+"_RhoA",&m_rhoA);
      m_tree->Branch(m_met_list[i]+"_RhoB",&m_rhoB);
      m_tree->Branch(m_met_list[i]+"_RhoC",&m_rhoC);

      m_tree->Branch(m_met_list[i]+"_ThreshA",&m_threshA);
      m_tree->Branch(m_met_list[i]+"_ThreshB",&m_threshB);
      m_tree->Branch(m_met_list[i]+"_ThreshC",&m_threshC);
    }
    if(m_met_list[i].Contains("JWOJ")){
      m_tree->Branch(m_met_list[i]+"_mht",&m_jwoj_mht[m_met_list[i]]);
      m_tree->Branch(m_met_list[i]+"_mst",&m_jwoj_mst[m_met_list[i]]);
    }
    
  }
  
  for(unsigned int j=0; j<m_jet_list.size(); j++){
    ATH_MSG_INFO(m_jet_list[j]); 
    m_jet_pt[m_jet_list[j]] = {0.}; 
    m_jet_eta[m_jet_list[j]] = {0.};
    m_jet_phi[m_jet_list[j]] = {0.};
    m_jet_m[m_jet_list[j]] = {0.};
      
    m_tree->Branch(m_jet_list[j]+"_pt", &m_jet_pt[m_jet_list[j]]); 
    m_tree->Branch(m_jet_list[j]+"_eta", &m_jet_eta[m_jet_list[j]]);
    m_tree->Branch(m_jet_list[j]+"_phi", &m_jet_phi[m_jet_list[j]]);
    m_tree->Branch(m_jet_list[j]+"_m",   &m_jet_m[m_jet_list[j]]);

  }
  
  for(unsigned int k=0; k<m_hlt_met_list.size();k++){
    m_hlt_met_et[m_hlt_met_list[k]]=0; 
    m_hlt_met_phi[m_hlt_met_list[k]]=0;
    if(m_dumpHltMet){
      m_tree->Branch(m_hlt_met_list[k]+"_et",&m_hlt_met_et[m_hlt_met_list[k]]); 
      m_tree->Branch(m_hlt_met_list[k]+"_phi",&m_hlt_met_phi[m_hlt_met_list[k]]);
    }
  }
  m_tree->Branch("isPileup",&isPileup); 
  m_tree->Branch("mu", &mu);
  m_tree->Branch("eventNumber",&eventNumber);
  m_tree->Branch("distFrontBunchTrain",&distFrontBunchTrain); 
  m_tree->Branch("eventDensity",&eventDensity);
  m_tree->Branch("eventDensitySigma",&eventDensitySigma);
  m_tree->Branch("eventArea",&eventArea);
  m_tree->Branch("eventWeight",&eventWeight);
  m_tree->Branch("eventLiveTime",&eventLiveTime); 
  m_tree->Branch("goodLB",&goodLB); 
  m_tree->Branch("truth_met_et", &truth_met_et); 

  triggerDecisions = NULL;
  if(m_dumpTrigDec){
    triggerDecisions = new int [triggerlist.size()];
    for (unsigned int i = 0 ; i < triggerlist.size(); i++){
      triggerDecisions[i] = 0;
      m_tree->Branch(TString("trig_") + triggerlist[i],&(triggerDecisions[i]));
    }
  }

  if(m_dumpTruthTaus){
    m_tree->Branch("TruthTaus_ptvis", &TruthTaus_ptvis);
    m_tree->Branch("TruthTaus_etavis", &TruthTaus_etavis);
    m_tree->Branch("TruthTaus_phivis", &TruthTaus_phivis);
    m_tree->Branch("TruthTaus_mvis", &TruthTaus_mvis);
    m_tree->Branch("TruthTaus_nCharged", &TruthTaus_nCharged);
    m_tree->Branch("TruthTaus_nNeutral", &TruthTaus_nNeutral);
  }

  if(m_dumpTruthEles){
    m_tree->Branch("TruthEles_pt", &TruthEles_pt);
    m_tree->Branch("TruthEles_eta", &TruthEles_eta);
    m_tree->Branch("TruthEles_phi", &TruthEles_phi);
    m_tree->Branch("TruthEles_motherID", &TruthEles_motherID);
    m_tree->Branch("TruthEles_barcode", &TruthEles_barcode);
  }

  if(m_dumpOffEles){
    m_tree->Branch("OffEles_pt", &OffEles_pt);
    m_tree->Branch("OffEles_eta", &OffEles_eta);
    m_tree->Branch("OffEles_phi", &OffEles_phi);
    m_tree->Branch("OffEles_ptcone", &OffEles_ptcone);
    m_tree->Branch("OffEles_etcone", &OffEles_etcone);
    m_tree->Branch("OffEles_flag", &OffEles_flag);
    m_tree->Branch("OffEles_tightflag", &OffEles_tightflag);
    m_tree->Branch("OffEles_isoflag", &OffEles_isoflag);
  }
  if(m_dumpTruthPhs){
    m_tree->Branch("TruthPhs_pt", &TruthPhs_pt);
    m_tree->Branch("TruthPhs_eta", &TruthPhs_eta);
    m_tree->Branch("TruthPhs_phi", &TruthPhs_phi);
    m_tree->Branch("TruthPhs_motherID", &TruthPhs_motherID);
    m_tree->Branch("TruthPhs_barcode", &TruthPhs_barcode);
  }

  if(m_dumpOffPhs){
    m_tree->Branch("OffPhs_pt", &OffPhs_pt);
    m_tree->Branch("OffPhs_eta", &OffPhs_eta);
    m_tree->Branch("OffPhs_phi", &OffPhs_phi);
    m_tree->Branch("OffPhs_ptcone", &OffPhs_ptcone);
    m_tree->Branch("OffPhs_etcone", &OffPhs_etcone);
    m_tree->Branch("OffPhs_flag", &OffPhs_flag);
    m_tree->Branch("OffPhs_tightflag", &OffPhs_tightflag);
    m_tree->Branch("OffPhs_isoflag", &OffPhs_isoflag);
  }


  if(m_dumpHltPhs){
    m_tree->Branch("HltPhs_pt", &HltPhs_pt);
    m_tree->Branch("HltPhs_eta", &HltPhs_eta);
    m_tree->Branch("HltPhs_phi", &HltPhs_phi);
    m_tree->Branch("HltPhs_ptcone", &HltPhs_ptcone);
    m_tree->Branch("HltPhs_etcone", &HltPhs_etcone);
    m_tree->Branch("HltPhs_flag", &HltPhs_flag);
    m_tree->Branch("HltPhs_tightflag", &HltPhs_tightflag);
    m_tree->Branch("HltPhs_looseisoflag", &HltPhs_looseisoflag);
    m_tree->Branch("HltPhs_tightisoflag", &HltPhs_tightisoflag);
  }



  if(m_useRun2L1){
    // Run 2 Jets
    m_tree->Branch("Run2_L1Jet_Et", &Run2_L1Jet_Et);
    m_tree->Branch("Run2_L1Jet_eta", &Run2_L1Jet_eta);
    m_tree->Branch("Run2_L1Jet_phi", &Run2_L1Jet_phi);

    // Run 2 MET
    m_tree->Branch("Run2_L1MET", &Run2_L1MET);
    m_tree->Branch("Run2_L1MET_phi", &Run2_L1MET_phi);

    // Run 2 Taus
    m_tree->Branch("Run2_L1Tau_Et", &Run2_L1Tau_Et);
    m_tree->Branch("Run2_L1Tau_eta", &Run2_L1Tau_eta);
    m_tree->Branch("Run2_L1Tau_phi", &Run2_L1Tau_phi);
    m_tree->Branch("Run2_L1Tau_iso", &Run2_L1Tau_iso);

    // Run 2 Eles
    m_tree->Branch("Run2_L1Ele_Et", &Run2_L1Ele_Et);
    m_tree->Branch("Run2_L1Ele_eta", &Run2_L1Ele_eta);
    m_tree->Branch("Run2_L1Ele_phi", &Run2_L1Ele_phi);
    m_tree->Branch("Run2_L1Ele_iso", &Run2_L1Ele_iso);
    m_tree->Branch("Run2_L1Ele_had", &Run2_L1Ele_had);

    m_tree->Branch("Run2_L1Muon_ptThres",&Run2_L1Muon_ptThres);
    m_tree->Branch("Run2_L1Muon_eta",&Run2_L1Muon_eta);
    m_tree->Branch("Run2_L1Muon_phi",&Run2_L1Muon_phi); 
  }
 
  // Run 3 Taus
  m_tree->Branch("Run3_L1Tau_Et", &Run3_L1Tau_Et);
  m_tree->Branch("Run3_L1Tau_eta", &Run3_L1Tau_eta);
  m_tree->Branch("Run3_L1Tau_phi", &Run3_L1Tau_phi);
  m_tree->Branch("Run3_L1Tau_iso", &Run3_L1Tau_iso);
  m_tree->Branch("Run3_L1Tau_BC_Et", &Run3_L1Tau_BC_Et);
  m_tree->Branch("Run3_L1Tau_BC_iso", &Run3_L1Tau_BC_iso);
  m_tree->Branch("Run3_L1Tau_BC_iso_12pass", &Run3_L1Tau_BC_iso_12pass);
  m_tree->Branch("Run3_L1Tau_BC_iso_20pass", &Run3_L1Tau_BC_iso_20pass);
  m_tree->Branch("Run3_L1Tau_Ore_Et", &Run3_L1Tau_Ore_Et);
  m_tree->Branch("Run3_L1Tau_Ore_iso", &Run3_L1Tau_Ore_iso);
  m_tree->Branch("Run3_L1Tau_Ore_iso_12pass", &Run3_L1Tau_Ore_iso_12pass);
  m_tree->Branch("Run3_L1Tau_Ore_iso_20pass", &Run3_L1Tau_Ore_iso_20pass);

  //std::vector<float>  Run3_jTau_eta;
  m_tree->Branch("Run3_jTau_Et", &Run3_jTau_Et);
  m_tree->Branch("Run3_jTau_eta", &Run3_jTau_eta);
  m_tree->Branch("Run3_jTau_phi", &Run3_jTau_phi);
  m_tree->Branch("Run3_jTau_iso", &Run3_jTau_iso);

  m_tree->Branch("Run3_jEle_Et", &Run3_jEle_Et);
  m_tree->Branch("Run3_jEle_eta", &Run3_jEle_eta);
  m_tree->Branch("Run3_jEle_phi", &Run3_jEle_phi);
  m_tree->Branch("Run3_jEle_iso", &Run3_jEle_iso);
  m_tree->Branch("Run3_jEle_Had", &Run3_jEle_Had);

  // Run 3 Eles
  m_tree->Branch("Run3_L1Ele_Et", &Run3_L1Ele_Et);
  m_tree->Branch("Run3_L1Ele_eta", &Run3_L1Ele_eta);
  m_tree->Branch("Run3_L1Ele_phi", &Run3_L1Ele_phi);
  m_tree->Branch("Run3_L1Ele_reta", &Run3_L1Ele_reta);
  m_tree->Branch("Run3_L1Ele_retaL12", &Run3_L1Ele_retaL12);
  m_tree->Branch("Run3_L1Ele_rhad", &Run3_L1Ele_rhad);
  m_tree->Branch("Run3_L1Ele_wstot", &Run3_L1Ele_wstot);
  m_tree->Branch("Run3_L1Ele_passiso", &Run3_L1Ele_passiso);
  if(m_dumpTowers){
    m_tree->Branch("gTowerEt", &m_gTowerEt);
    m_tree->Branch("gTowerEta", &m_gTowerEta);
    m_tree->Branch("gTowerPhi", &m_gTowerPhi);
    m_tree->Branch("gTowerSampling",&m_gTowerSampling); 

    m_tree->Branch("jTowerEt", &m_jTowerEt);
    m_tree->Branch("jTowerEta", &m_jTowerEta);
    m_tree->Branch("jTowerPhi", &m_jTowerPhi);
    m_tree->Branch("jTowerSampling",&m_jTowerSampling); 
  }
  if(m_dumpRun2Towers){
    m_tree->Branch("Run2TowerEt",&m_Run2TowerEt);
    m_tree->Branch("Run2TowerEta",&m_Run2TowerEta);
    m_tree->Branch("Run2TowerPhi",&m_Run2TowerPhi);
    m_tree->Branch("Run2TowerSampling",&m_Run2TowerSampling); 

  }
  CHECK( histSvc->regTree("/OUTPUT/ntuple",m_tree) );

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  delete [] triggerDecisions;
  triggerDecisions = NULL;


  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::ConstituentScaleHLTJets(){

    const xAOD::JetContainer* jets = 0;
    CHECK( evtStore()->retrieve( jets, "HLT_xAOD__JetContainer_a4tcemsubjesISFS") );

    xAOD::JetContainer* jetsConstituent = new xAOD::JetContainer();
    xAOD::JetAuxContainer* jetsConstituentAux = new xAOD::JetAuxContainer();
    jetsConstituent->setStore(jetsConstituentAux);
    
    CHECK(evtStore()->record(jetsConstituent, "HLT_xAOD__JetContainer_a4tcem"));
    CHECK(evtStore()->record(jetsConstituentAux, "HLT_xAOD__JetContainer_a4tcemAux."));

    xAOD::JetContainer* jetsPileup = new xAOD::JetContainer();
    xAOD::JetAuxContainer* jetsPileupAux = new xAOD::JetAuxContainer();
    jetsPileup->setStore(jetsPileupAux);
    
    CHECK(evtStore()->record(jetsPileup, "HLT_xAOD__JetContainer_a4tcemsub"));
    CHECK(evtStore()->record(jetsPileupAux, "HLT_xAOD__JetContainer_a4tcemsubAux."));
          
    for ( const auto* jet : *jets ) {

        const xAOD::JetFourMom_t &jNoCalib = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
        const xAOD::JetFourMom_t &jPuScale = jet->getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum");
        //   ATH_MSG_INFO(name << " pt " << jNoCalib.pt() << " " << jPuScale.pt()  );
        
        xAOD::Jet *constituent = new xAOD::Jet();
        jetsConstituent->push_back(constituent);
        constituent->setJetP4(jNoCalib);
        
        xAOD::Jet *pu = new xAOD::Jet();
        jetsPileup->push_back(pu);
        pu->setJetP4(jPuScale);
        
    }
          
  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::RetrieveJets(TString name){
    m_jet_pt[name].clear();
    m_jet_eta[name].clear();
    m_jet_phi[name].clear();
    m_jet_m[name].clear();
    
    std::vector<TLorentzVector> jetsVect;
    
    const xAOD::JetContainer* jets = 0;
    CHECK( evtStore()->retrieve( jets, name.Data() ) );
    
    for ( const auto* jet : *jets ) {
        TLorentzVector tmp;
        if(jet->pt()<5000) continue;
        if(m_applyJVT && name.Contains("AntiKt4EMTopo")){
            float jvt = m_jvt->updateJvt(*jet);
            if(m_applyJVT && (fabs(jet->eta())<2.4 && jvt<0.59 && jet->pt()<120e3) ) continue;
            //ATH_MSG_INFO(name << " pt: " << jet->pt() << "  jvt " << jvt );
        }
        //ATH_MSG_INFO(name << " pt: " << jet->pt() );

        tmp.SetPtEtaPhiM(jet->pt(),jet->eta(),jet->phi(),jet->m());
        jetsVect.push_back(tmp);
    }
    std::sort(jetsVect.begin(),jetsVect.end(),GreaterPt);
    for(unsigned int i=0; i<jetsVect.size();i++){
        m_jet_pt[name].push_back(jetsVect.at(i).Perp());
        m_jet_eta[name].push_back(jetsVect.at(i).Eta());
        m_jet_phi[name].push_back(jetsVect.at(i).Phi());
        m_jet_m[name].push_back(jetsVect.at(i).M());
    }
    return StatusCode::SUCCESS;

}

StatusCode NtupleDumperAlg::RetrieveL1Jets(TString name){

  //clear vectors
  m_jet_pt[name].clear();
  m_jet_eta[name].clear();
  m_jet_phi[name].clear();
  m_jet_m[name].clear();

  const xAOD::JetRoIContainer * l1_jets = 0;
  if (evtStore()->retrieve(l1_jets,name.Data() ).isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 jet RoIs");
    return StatusCode::FAILURE;
  }
  for(auto l1j : *l1_jets){
    //ATH_MSG_INFO("l1 Jet et4x4 " << l1j->et4x4() << " 8x8 " << l1j->et8x8());                                                                                                             // push back et,eta,phi
    if(l1j->et8x8()<5e3) continue; 
    m_jet_pt[name].push_back(l1j->et8x8() ); 
    m_jet_eta[name].push_back(l1j->eta() ); 
    m_jet_phi[name].push_back(l1j->phi() ); 

  }

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::RetrieveHLTMET(TString name){

  const xAOD::TrigMissingETContainer* met(0); 
  CHECK( evtStore()->retrieve(met, name.Data() ));

  float ex = met->front()->ex() * 0.001; 
  float ey = met->front()->ey() * 0.001;

  float Met = sqrt(ex*ex + ey*ey);
  float Met_phi = (ex>0) ? atan(ey/ex) : -999; 

  m_hlt_met_et[name]=Met; 
  m_hlt_met_phi[name]=Met_phi; 
  
  return StatusCode::SUCCESS; 
}

StatusCode NtupleDumperAlg::RetrieveTopoclusters(TString name){
   
    m_jet_pt[name].clear();
    m_jet_eta[name].clear();
    m_jet_phi[name].clear();
    m_jet_m[name].clear();
  
  const xAOD::CaloClusterContainer * topoClusters = 0;
  CHECK(evtStore()->retrieve(topoClusters,name.Data()));

  for ( const auto* cluster : *topoClusters ) {
    float et = cluster->et();
    float m = cluster->m();
    float eta = cluster->eta();
    float phi = cluster->phi();
      
    m_jet_pt[name].push_back(et);
    m_jet_eta[name].push_back(eta);
    m_jet_phi[name].push_back(phi);
    m_jet_m[name].push_back(m);
 
  }
  
  return StatusCode::SUCCESS;
}


StatusCode NtupleDumperAlg::RetrieveMET(TString name){
  ATH_MSG_DEBUG("RetrieveMET " << name); 

  const xAOD::EnergySumRoI* met(0);  
  CHECK( evtStore()->retrieve(met, name.Data() ) );
  SG::AuxElement::Accessor<float> rhoA("RhoA");
  SG::AuxElement::Accessor<float> rhoB("RhoB");
  SG::AuxElement::Accessor<float> rhoC("RhoC");

  SG::AuxElement::Accessor<float> threshA("ThreshA");
  SG::AuxElement::Accessor<float> threshB("ThreshB");
  SG::AuxElement::Accessor<float> threshC("ThreshC");

  SG::AuxElement::Accessor<float> mht("MHT");
  SG::AuxElement::Accessor<float> mst("MST");

  float met_x = met->exMiss();
  float met_y = met->eyMiss(); 

  float met_et = sqrt(met_x*met_x + met_y*met_y); 
  float met_phi = std::atan2(met_y, met_x);
  
  m_met_et[name]=met_et;
  m_met_phi[name]=met_phi; 
  ATH_MSG_DEBUG("metx,y: " << met_x << "," << met_y ); 
  ATH_MSG_DEBUG("met_et,phi " << met_et << ","<<met_phi ); 

  if(name.Contains("EventVariables") ){
    if(rhoA.isAvailable(*met)){
	m_rhoA = rhoA(*met); 
	m_rhoB = rhoB(*met);
	m_rhoC = rhoC(*met); 
	m_threshA = threshA(*met);
	m_threshB = threshB(*met);
	m_threshC = threshC(*met); 
      }
  }
    
  if(name.Contains("JWOJ")){
    if(mht.isAvailable(*met) ){
      m_jwoj_mht[name] = mht(*met); 
    }
    if(mst.isAvailable(*met) ){
      m_jwoj_mst[name] = mst(*met);
    }
  }

  return StatusCode::SUCCESS; 
}

StatusCode NtupleDumperAlg::Jet_algs(){
    for(unsigned int i=0; i<m_jet_list.size(); i++){
        if((m_dumpOnJets && m_jet_list[i].Contains("HLT")) || m_jet_list[i].Contains("AntiKt")){
            if(!m_dumpTruthJets && m_jet_list[i].Contains("Truth"))continue;
            CHECK(RetrieveJets(m_jet_list[i]));
        }
        if(m_dumpClusters){
            if(m_jet_list[i].Contains("Cluster"))
                CHECK(RetrieveTopoclusters(m_jet_list[i]));
        }
        if(m_jet_list[i].Contains("jRound") || m_jet_list[i].Contains("gL1") || m_jet_list[i].Contains("gBlock")  ) CHECK(RetrieveL1Jets(m_jet_list[i]));
    }
  
  return StatusCode::SUCCESS; 

}

StatusCode NtupleDumperAlg::HLT_MET_algs(){
  for(unsigned int i=0; i<m_hlt_met_list.size();i++) CHECK(RetrieveHLTMET(m_hlt_met_list[i]));
  return StatusCode::SUCCESS; 
}

//use only rho subtraction and fixed noise cut
StatusCode NtupleDumperAlg::MET_algs(){
  
  for(unsigned int i=0; i<m_met_list.size();i++) CHECK(RetrieveMET(m_met_list[i])); 

  return StatusCode::SUCCESS;
}


StatusCode NtupleDumperAlg::DumpTowers(){
  //We must clear the towers first!!!

  m_gTowerEt.clear();
  m_gTowerEta.clear();
  m_gTowerPhi.clear();
  m_gTowerSampling.clear();

  m_jTowerEt.clear();
  m_jTowerEta.clear();
  m_jTowerPhi.clear(); 
  m_jTowerSampling.clear();

  m_Run2TowerEt.clear();
  m_Run2TowerEta.clear();
  m_Run2TowerPhi.clear();
  m_Run2TowerSampling.clear();

  //Dump jTowers
  if(m_dumpTowers){
    const xAOD::JGTowerContainer* jTowers =0;
    CHECK( evtStore()->retrieve( jTowers,"JTower"));
    
    unsigned int jsize = jTowers->size();

    for(unsigned int t = 0; t < jsize; t++){
      const xAOD::JGTower* tower = jTowers->at(t);
      m_jTowerEt.push_back(tower->et() ); 
      m_jTowerEta.push_back(tower->eta() ); 
      m_jTowerPhi.push_back(tower->phi() ); 
      m_jTowerSampling.push_back(tower->sampling() ); 
    }

    const xAOD::JGTowerContainer* gTowers =0;
    CHECK( evtStore()->retrieve( gTowers,"GTower"));
    
    unsigned int gsize = gTowers->size();
    
    for(unsigned int t = 0; t < gsize; t++){
      const xAOD::JGTower* tower = gTowers->at(t);
      m_gTowerEt.push_back(tower->et() );
      m_gTowerEta.push_back(tower->eta() );
      m_gTowerPhi.push_back(tower->phi() );
      m_gTowerSampling.push_back(tower->sampling() );
    }
  }
  if(m_dumpRun2Towers){
    const xAOD::TriggerTowerContainer *TTs; 

    CHECK( evtStore()->retrieve(TTs,"xAODTriggerTowers") ); 
    
    for(unsigned tt_hs=0 ; tt_hs<TTs->size(); tt_hs++){
    const xAOD::TriggerTower * tt = TTs->at(tt_hs);
    m_Run2TowerEt.push_back(tt->cpET()*500 ); 
    m_Run2TowerEta.push_back(tt->eta() );
    m_Run2TowerPhi.push_back(tt->phi() ); 
    m_Run2TowerSampling.push_back(tt->sampling() ); 
  }



  }
  return StatusCode::SUCCESS; 

}

StatusCode NtupleDumperAlg::Tau_algs(){

  Run3_L1Tau_Et.clear();
  Run3_L1Tau_eta.clear();
  Run3_L1Tau_phi.clear();
  Run3_L1Tau_iso.clear();
  Run3_L1Tau_BC_Et.clear();
  Run3_L1Tau_BC_iso.clear();
  Run3_L1Tau_BC_iso_12pass.clear();
  Run3_L1Tau_BC_iso_20pass.clear();
  Run3_L1Tau_Ore_Et.clear();
  Run3_L1Tau_Ore_iso.clear();
  Run3_L1Tau_Ore_iso_12pass.clear();
  Run3_L1Tau_Ore_iso_20pass.clear();

  Run3_jTau_Et.clear();
  Run3_jTau_eta.clear(); 
  Run3_jTau_phi.clear();
  Run3_jTau_iso.clear(); 
  

  const xAOD::EmTauRoIContainer * l1_taus = 0;
  if (evtStore()->retrieve(l1_taus, "SClusterTau").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 tau RoIs");
    return StatusCode::FAILURE;
  }

  for(auto l1t : *l1_taus){
    // Dyn accessor
    SG::AuxElement::Accessor<float> my_et("R3ClusterET");
    SG::AuxElement::Accessor<float> my_iso("R3ClusterIso");
    SG::AuxElement::Accessor<float> my_bc_et("R3_BC_ClusterET");
    SG::AuxElement::Accessor<float> my_bc_iso("R3_BC_ClusterIso");
    SG::AuxElement::Accessor<bool> my_bc_iso_12pass("R3_BC_ClusterIso_12pass");
    SG::AuxElement::Accessor<bool> my_bc_iso_20pass("R3_BC_ClusterIso_20pass");
    SG::AuxElement::Accessor<float> my_ore_et("R3_Ore_ClusterET");
    SG::AuxElement::Accessor<float> my_ore_iso("R3_Ore_ClusterIso");
    SG::AuxElement::Accessor<bool> my_ore_iso_12pass("R3_Ore_ClusterIso_12pass");
    SG::AuxElement::Accessor<bool> my_ore_iso_20pass("R3_Ore_ClusterIso_20pass");

    if(my_et(*l1t)<5)continue; 

    Run3_L1Tau_Et.push_back(my_et(*l1t) );
    Run3_L1Tau_eta.push_back(l1t->eta() );
    Run3_L1Tau_phi.push_back(l1t->phi() );
    Run3_L1Tau_iso.push_back(my_iso(*l1t) );
    Run3_L1Tau_BC_Et.push_back(my_bc_et(*l1t) );
    Run3_L1Tau_BC_iso.push_back(my_bc_iso(*l1t) );
    Run3_L1Tau_BC_iso_12pass.push_back(my_bc_iso_12pass(*l1t));
    Run3_L1Tau_BC_iso_20pass.push_back(my_bc_iso_20pass(*l1t));
    Run3_L1Tau_Ore_Et.push_back(my_ore_et(*l1t) );
    Run3_L1Tau_Ore_iso.push_back(my_ore_iso(*l1t) );
    Run3_L1Tau_Ore_iso_12pass.push_back(my_ore_iso_12pass(*l1t));
    Run3_L1Tau_Ore_iso_20pass.push_back(my_ore_iso_20pass(*l1t));
  }

  const xAOD::EmTauRoIContainer * l1_jtaus = 0;
  if (evtStore()->retrieve(l1_jtaus, "jTaus").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 tau RoIs");
    return StatusCode::FAILURE;
  }

  for(auto l1t : *l1_jtaus){
    Run3_jTau_Et.push_back(l1t->tauClus()); 
    Run3_jTau_eta.push_back(l1t->eta());
    Run3_jTau_phi.push_back(l1t->phi());
    Run3_jTau_iso.push_back(l1t->emIsol());
  }

  return StatusCode::SUCCESS;

}

StatusCode NtupleDumperAlg::Ele_algs(){

  Run3_L1Ele_Et.clear();
  Run3_L1Ele_eta.clear();
  Run3_L1Ele_phi.clear();

  Run3_L1Ele_rhad.clear();
  Run3_L1Ele_reta.clear();
  Run3_L1Ele_retaL12.clear();
  Run3_L1Ele_wstot.clear();
  Run3_L1Ele_passiso.clear();
  
  Run3_jEle_Et.clear();
  Run3_jEle_eta.clear();
  Run3_jEle_phi.clear();
  Run3_jEle_Had.clear(); 
  Run3_jEle_iso.clear(); 

  const xAOD::TrigEMClusterContainer * l1_eles = 0;
  if (evtStore()->retrieve(l1_eles, "SClusterCl").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 ele RoIs");
    return StatusCode::FAILURE;
  }
  
  SG::AuxElement::Accessor<float> RetaL12("Run3REtaL12"); 
  
  for(auto l1e : *l1_eles){
    if(l1e->et() < 5)
      continue;
    
    Run3_L1Ele_Et.push_back(l1e->et());
    Run3_L1Ele_eta.push_back(l1e->eta());
    Run3_L1Ele_phi.push_back(l1e->phi());
    
    Run3_L1Ele_rhad.push_back(l1e->auxdecor<float>("Run3RHad"));
    Run3_L1Ele_reta.push_back(l1e->auxdecor<float>("Run3REta"));
    if(RetaL12.isAvailable(*l1e) ) Run3_L1Ele_retaL12.push_back(l1e->auxdecor<float>("Run3REtaL12"));
    else Run3_L1Ele_retaL12.push_back(-1.); 
    Run3_L1Ele_wstot.push_back(l1e->wstot());
    
    // Final iso logic
    bool pass = l1e->auxdecor<int>("PassRun3wstot") && l1e->auxdecor<int>("PassRun3RHad") && l1e->auxdecor<int>("PassRun3REta");
    Run3_L1Ele_passiso.push_back(pass);
  }

  const xAOD::EmTauRoIContainer * l1_jele = 0;
  if (evtStore()->retrieve(l1_jele, "jEles").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 jEM RoIs");
    return StatusCode::FAILURE;
  }

  for(auto l1e : *l1_jele){
    Run3_jEle_Et.push_back(l1e->emClus());
    Run3_jEle_eta.push_back(l1e->eta());
    Run3_jEle_phi.push_back(l1e->phi());
    Run3_jEle_Had.push_back(l1e->tauClus());
    Run3_jEle_iso.push_back(l1e->emIsol());

  }

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::FillTruthMET(){

  const xAOD::MissingETContainer* metTruth = 0;
  CHECK( evtStore()->retrieve( metTruth, "MET_Truth" ) );

  float met_x = (*metTruth)["NonInt"]->mpx();
  float met_y = (*metTruth)["NonInt"]->mpy();

  truth_met_et = sqrt(met_x*met_x+met_y*met_y);

  return StatusCode::SUCCESS; 

}

StatusCode NtupleDumperAlg::FillTruthEles(){
  
  TruthEles_pt.clear();
  TruthEles_eta.clear();
  TruthEles_phi.clear();
  TruthEles_motherID.clear();
  TruthEles_barcode.clear();
  
  const xAOD::TruthParticleContainer* TPs = 0;
  CHECK( evtStore()->retrieve(TPs,"TruthParticles") );
  
  for (auto TP: *TPs){
    bool electronOrPositron = abs(TP->pdgId())==11;
    bool Zboson = abs(TP->pdgId())==23; 
    if(Zboson)ATH_MSG_DEBUG("Z pT " << TP->pt() ); 
    bool correctSatus = TP->status()==1;
    if (electronOrPositron && correctSatus){
      //ATH_MSG_DEBUG("Number of Electron parents " << TP->nParents() << " " << TP->parent()->pdgId()); 
      if(TP->pt()<5e3) continue; 
      TruthEles_pt.push_back(TP->pt());
      TruthEles_eta.push_back(TP->eta());
      TruthEles_phi.push_back(TP->phi());
      TruthEles_motherID.push_back(TP->parent()->pdgId());	
      TruthEles_barcode.push_back(TP->barcode());
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::FillTruthPhs(){

  TruthPhs_pt.clear();
  TruthPhs_eta.clear();
  TruthPhs_phi.clear();
  TruthPhs_motherID.clear();
  TruthPhs_barcode.clear();

  const xAOD::TruthParticleContainer* TPs = 0;
  CHECK( evtStore()->retrieve(TPs,"TruthParticles") );

  for (auto TP: *TPs){
    bool isPhoton = abs(TP->pdgId())==22;
    bool Zboson = abs(TP->pdgId())==23;
    if(Zboson) ATH_MSG_DEBUG("Z pT " << TP->pt() );
    bool correctStatus = TP->status()==1;
    if (isPhoton && correctStatus){
      //ATH_MSG_DEBUG("Number of Photon parent " << TP->nParents() << " " << TP->parent()->pdgId());                                                                   
      // Cut out very low photon candidates. Also, places cut near reco threshold                                                                                      
      if(TP->pt()<5e3) continue;
      TruthPhs_pt.push_back(TP->pt());
      TruthPhs_eta.push_back(TP->eta());
      TruthPhs_phi.push_back(TP->phi());
      TruthPhs_motherID.push_back(TP->parent()->pdgId());
      TruthPhs_barcode.push_back(TP->barcode());
    }
  }

  return StatusCode::SUCCESS;
}



StatusCode NtupleDumperAlg::FilterPileupJets(){
  std::vector<TLorentzVector> truthJets;
  std::vector<TLorentzVector> puJets;

  const xAOD::JetContainer* jets = 0;
  CHECK( evtStore()->retrieve( jets, "AntiKt4TruthJets") );
  for ( const auto* jet : *jets ) {
    TLorentzVector tmp; tmp.SetPtEtaPhiE(jet->pt(),jet->eta(),jet->phi(),jet->e());
    truthJets.push_back(tmp); 
  }
  
  const xAOD::JetContainer* pileup = 0;
  CHECK( evtStore()->retrieve( pileup, "InTimeAntiKt4TruthJets")); 
  for ( const auto* jet : *pileup ) {
    TLorentzVector tmp;tmp.SetPtEtaPhiE(jet->pt(),jet->eta(),jet->phi(),jet->e());
    puJets.push_back(tmp);
  }

  std::sort(truthJets.begin(),truthJets.end(),GreaterPt);
  std::sort(puJets.begin()   ,puJets.end()   ,GreaterPt);

  float truthJetPt = 0; 
  float puJetPt = 0;

  if(truthJets.size()>0)truthJetPt=truthJets.at(0).Perp(); 
  if(puJets.size()>0)   puJetPt=puJets.at(0).Perp(); 
  
  if(puJetPt>truthJetPt)isPileup=true; 
  else isPileup = false; 

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::FillEventShapes(){

  const xAOD::EventShape* eventShape = 0;
  CHECK( evtStore()->retrieve( eventShape, "Kt4EMTopoOriginEventShape" ) );
  eventDensity = eventShape->getDensity( xAOD::EventShape::Density );
  eventArea = eventShape->getDensity( xAOD::EventShape::DensityArea );
  eventDensitySigma = eventShape->getDensity( xAOD::EventShape::DensitySigma );

  return StatusCode::SUCCESS;
}


StatusCode NtupleDumperAlg::CalibrateOffJets(){

  const xAOD::JetContainer * inJets = 0;
  CHECK(evtStore()->retrieve(inJets, "AntiKt4EMTopoJets"));
  //Shallow copy
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > calibJetsSC = xAOD::shallowCopyContainer( *inJets );
  std::string m_outContainerName = "AntiKt4EMTopoJets";
  std::string outSCContainerName=m_outContainerName+"Calibrated";
  std::string outSCAuxContainerName=m_outContainerName+"CalibratedAux.";
  CHECK( evtStore()->record( calibJetsSC.first,  outSCContainerName));
  CHECK( evtStore()->record( calibJetsSC.second, outSCAuxContainerName));

  for ( auto jet : *(calibJetsSC.first) ) {
    m_jetCalibration->applyCorrection(*jet);
    //ATH_MSG_INFO("Jets after calibration " << jet->pt() );
  }

  return StatusCode::SUCCESS; 

}


bool NtupleDumperAlg::GreaterPt(TLorentzVector &a, TLorentzVector &b){

  return a.Perp()>b.Perp();

}

StatusCode NtupleDumperAlg::FillOffEles(){
  
  OffEles_pt.clear();
  OffEles_eta.clear();
  OffEles_phi.clear();
  OffEles_ptcone.clear();
  OffEles_etcone.clear();
  OffEles_flag.clear();
  OffEles_tightflag.clear();
  OffEles_isoflag.clear();
  
  const xAOD::ElectronContainer* electrons = 0;
  CHECK( evtStore()->retrieve( electrons, "Electrons" ));

  static SG::AuxElement::ConstAccessor<char> ElLHMedium("LHMedium");
  static SG::AuxElement::ConstAccessor<char> ElLHTight("LHTight");

  for (auto TP: *electrons){
    OffEles_pt.push_back(TP->pt());
    OffEles_eta.push_back(TP->eta());
    OffEles_phi.push_back(TP->phi());

    OffEles_ptcone.push_back(TP->auxdata<float>("ptvarcone20"));
    OffEles_etcone.push_back(TP->auxdata<float>("topoetcone20"));

    bool isMed = ElLHMedium.isAvailable(*TP) && TP->passSelection("LHMedium");
    OffEles_flag.push_back(isMed);

    bool isTight = ElLHTight.isAvailable(*TP) && TP->passSelection("LHTight");
    OffEles_tightflag.push_back(isTight);

    bool isoflag = iso_tool->accept(*TP);    
    OffEles_isoflag.push_back(isoflag);
  }

  return StatusCode::SUCCESS;
}
StatusCode NtupleDumperAlg::FillOffPhs(){

  OffPhs_pt.clear();
  OffPhs_eta.clear();
  OffPhs_phi.clear();
  OffPhs_ptcone.clear();
  OffPhs_etcone.clear();
  OffPhs_flag.clear();
  OffPhs_tightflag.clear();
  OffPhs_isoflag.clear();

  const xAOD::PhotonContainer* photons = 0;
  CHECK( evtStore()->retrieve( photons, "Photons" ));

  static SG::AuxElement::ConstAccessor<char> PhMedium("Medium");
  static SG::AuxElement::ConstAccessor<char> PhTight("Tight");

  bool isMed = false;
  bool isTight = false;
  bool isoflag = false;
  for (auto TP: *photons){
    OffPhs_pt.push_back(TP->pt());
    OffPhs_eta.push_back(TP->eta());
    OffPhs_phi.push_back(TP->phi());

    OffPhs_ptcone.push_back(TP->auxdata<float>("ptvarcone20"));
    OffPhs_etcone.push_back(TP->auxdata<float>("topoetcone20"));

    isMed = PhMedium.isAvailable(*TP) && TP->passSelection("Medium");
    OffPhs_flag.push_back(isMed);

    isTight = PhTight.isAvailable(*TP) && TP->passSelection("Tight");
    OffPhs_tightflag.push_back(isTight);

    isoflag = iso_tool->accept(*TP);
    OffPhs_isoflag.push_back(isoflag);
  }

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::FillHltPhs(){

  HltPhs_pt.clear();
  HltPhs_eta.clear();
  HltPhs_phi.clear();
  HltPhs_ptcone.clear();
  HltPhs_etcone.clear();
  HltPhs_flag.clear();
  HltPhs_tightflag.clear();
  HltPhs_looseisoflag.clear();
  HltPhs_tightisoflag.clear();

  const xAOD::PhotonContainer* photons = 0;
  CHECK( evtStore()->retrieve( photons, "HLT_xAOD__PhotonContainer_egamma_Photons" ));

  static SG::AuxElement::ConstAccessor<char> PhMedium("Medium");
  static SG::AuxElement::ConstAccessor<char> PhTight("Tight");

  bool isMed = false;
  bool isTight = false;
  for (auto TP: *photons){
    HltPhs_pt.push_back(TP->pt());
    HltPhs_eta.push_back(TP->eta());
    HltPhs_phi.push_back(TP->phi());

    HltPhs_ptcone.push_back(TP->auxdata<float>("ptvarcone20"));
    HltPhs_etcone.push_back(TP->auxdata<float>("topoetcone20"));

    isMed = PhMedium.isAvailable(*TP) && TP->passSelection("Medium");
    HltPhs_flag.push_back(isMed);

    isTight = PhTight.isAvailable(*TP) && TP->passSelection("Tight");
    HltPhs_tightflag.push_back(isTight);
    // WIP: Issue with finding the proper variable in the ESD                                                                                      // The topoetcone variable does not exist in the HLT_xAOD__PhotonContainer_egamma_Photons or Iso_Photons container                             // Isolation check: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/IsolationTrigger#Photon_isolation_in_the_HLT                           // Two WPs:
    // icalovloose: topoetcone20/E_{T} < 0.1                                                                                                       // icalotight : (topoetcone40 - 2.45 GeV)/E_{T} < 0.03                                                                                         //topoetcone40 = TP->auxdata<float>("topoetcone40");                                                                                           //HltPhs_looseisoflag = iso_tool->accept(*TP);                                                                                                 //HltPhs_tightisoflag = iso_tool->accept(*TP);                                                                                                                   
  }
  
  return StatusCode::SUCCESS;
}


StatusCode NtupleDumperAlg::FillTruthTaus(){
  
  TruthTaus_ptvis.clear();
  TruthTaus_etavis.clear();
  TruthTaus_phivis.clear();
  TruthTaus_mvis.clear();
  TruthTaus_nCharged.clear();
  TruthTaus_nNeutral.clear();

  const xAOD::TruthParticleContainer* tausTruth = 0;
  CHECK( evtStore()->retrieve( tausTruth, "TruthTaus") );
  
  for(auto ttaus : *tausTruth){
    if(ttaus->auxdecor<char>("IsHadronicTau"))
      {
	TruthTaus_ptvis.push_back(ttaus->auxdecor<double>("pt_vis"));
	TruthTaus_etavis.push_back(ttaus->auxdecor<double>("eta_vis"));
	TruthTaus_phivis.push_back(ttaus->auxdecor<double>("phi_vis"));
	TruthTaus_mvis.push_back(ttaus->auxdecor<double>("m_vis"));
	TruthTaus_nCharged.push_back(ttaus->auxdecor<size_t>("numCharged"));
	TruthTaus_nNeutral.push_back(ttaus->auxdecor<size_t>("numNeutral"));
      }
  }

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::FillTrigDecisions(){

  //auto allChains = m_trigDecTool->getChainGroup(".*");
  //std::cout << "Triggers that passed : ";
  //for(auto& trig : allChains->getListOfTriggers())
  //  if(m_trigDecTool->getChainGroup(trig)->isPassed()) std::cout << trig << ", ";
  //std::cout << std::endl;

  for (unsigned int i = 0; i < triggerlist.size(); i++){
    int passTrigger = m_trigDecTool->isPassed(triggerlist[i]);
    //ATH_MSG_INFO("Trigger " << triggerlist[i] << ": " << passTrigger); 
    triggerDecisions[i] = passTrigger;
  }

 return StatusCode::SUCCESS; 
}

StatusCode NtupleDumperAlg::Run2_L1(){

  Run2_L1Jet_Et.clear();
  Run2_L1Jet_eta.clear();
  Run2_L1Jet_phi.clear();

  Run2_L1Ele_Et.clear();
  Run2_L1Ele_eta.clear();
  Run2_L1Ele_phi.clear();
  Run2_L1Ele_iso.clear();
  Run2_L1Ele_had.clear();

  Run2_L1Tau_Et.clear();
  Run2_L1Tau_eta.clear();
  Run2_L1Tau_phi.clear();
  Run2_L1Tau_iso.clear();
  
  Run2_L1Muon_ptThres.clear();
  Run2_L1Muon_eta.clear();
  Run2_L1Muon_phi.clear(); 

  const xAOD::JetRoIContainer * l1_jets = 0;
  if (evtStore()->retrieve(l1_jets,"LVL1JetRoIs").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 jet RoIs");
    return StatusCode::FAILURE;
  }
  for(auto l1j : *l1_jets){
    //ATH_MSG_INFO("l1 Jet et4x4 " << l1j->et4x4() << " 8x8 " << l1j->et8x8());
    Run2_L1Jet_Et.push_back(l1j->et8x8() );
    Run2_L1Jet_eta.push_back(l1j->eta() );
    Run2_L1Jet_phi.push_back(l1j->phi() );
  }

  Run2_L1MET = 0.;
  Run2_L1MET_phi = -999.;

  const xAOD::EnergySumRoI* l1MetObject(0);
  CHECK( evtStore()->retrieve(l1MetObject, "LVL1EnergySumRoI") );

  float l1_mex = l1MetObject->exMiss();
  float l1_mey = l1MetObject->eyMiss();

  Run2_L1MET = sqrt(l1_mex*l1_mex+l1_mey*l1_mey);
  Run2_L1MET_phi=std::atan2(l1_mey, l1_mex);

  const xAOD::EmTauRoIContainer * l1_taus = 0;
  if (evtStore()->retrieve(l1_taus, "LVL1EmTauRoIs").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 tau RoIs");
    return StatusCode::FAILURE;
  }
  
  for(auto l1t : *l1_taus){
    //ATH_MSG_INFO("jTaus type: " << l1t->roiType()); 
    //ATH_MSG_INFO("eta,phi,et " << l1t->eta() << "," << l1t->phi() << "," <<l1t->tauClus()<<" iso " << l1t->emIsol() ); 
    // Ignore EM RoIs
    if(l1t->roiType() == 2)
      {   
	Run2_L1Tau_Et.push_back(l1t->tauClus() );
	Run2_L1Tau_eta.push_back(l1t->eta() );
	Run2_L1Tau_phi.push_back(l1t->phi() );
	Run2_L1Tau_iso.push_back(l1t->emIsol() );
      }
    // Now, do EM RoIs
    if(l1t->roiType() == 1)
      {
	Run2_L1Ele_Et.push_back(l1t->emClus() );
	Run2_L1Ele_eta.push_back(l1t->eta() );
	Run2_L1Ele_phi.push_back(l1t->phi() );
	Run2_L1Ele_iso.push_back(l1t->emIsol() );
	Run2_L1Ele_had.push_back(l1t->hadCore() );
      }
  }
  
  const xAOD::MuonRoIContainer* l1_muons = 0;
  if (evtStore()->retrieve(l1_muons, "LVL1MuonRoIs").isFailure() ) {
    ATH_MSG_INFO("ERROR loading L1 tau RoIs");
    return StatusCode::FAILURE;
  }

  for(auto l1m : *l1_muons){
    Run2_L1Muon_ptThres.push_back(l1m->thrValue() ); 
    Run2_L1Muon_eta.push_back(l1m->eta() );
    Run2_L1Muon_phi.push_back(l1m->phi() ); 
  }
  return StatusCode::SUCCESS;
}



StatusCode NtupleDumperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  //HERE IS AN EXAMPLE
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  mu = ei->averageInteractionsPerCrossing(); 
  eventNumber = ei->eventNumber(); 
  bool isMC=false; 
  isMC = (ei->eventType(xAOD::EventInfo::IS_SIMULATION) ); 
    if(!isMC){
        m_applyFilter=false;
    }
  if(isMC){
    int mcChannel = ei->mcChannelNumber();
    bool isJZ = false; 
    
    if(mcChannel >= 361020 && mcChannel<=361032)isJZ=true; 
    if(!isJZ && m_applyFilter) m_applyFilter=false; 
    if(m_applyFilter) {
      isPileup=false; 
      CHECK(FilterPileupJets());
    }//applyFilter
  }//isMC

  if(isMC) eventWeight = ei->mcEventWeights()[0];

  if(!isMC){
    eventLiveTime = ei->auxdata<double>("EnhancedBiasLivetime"); // Keep track of my total
    eventWeight = ei->auxdata<double>("EnhancedBiasWeight"); // Weight with me
    goodLB = static_cast<bool> (ei->auxdata<char>("IsGoodLBFlag") );
    
  }
  ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
  distFrontBunchTrain = m_bcTool->distanceFromFront(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);

  CHECK(FillEventShapes() );
  if(m_useRun2L1)CHECK( Run2_L1() );
  CHECK( MET_algs() ); 
  if(m_dumpEleAlgs)CHECK( Ele_algs() );
  CHECK(CalibrateOffJets());
  if(m_dumpTrigDec)CHECK(ConstituentScaleHLTJets());
  CHECK( Jet_algs() ); 
  if(m_dumpHltMet) CHECK(HLT_MET_algs());
  if(m_dumpTrigDec) CHECK(FillTrigDecisions());
  if(m_dumpTauAlgs)   CHECK( Tau_algs()      );
  if(m_dumpTruthMet)  CHECK( FillTruthMET()  );
  if(m_dumpTruthTaus) CHECK( FillTruthTaus() );
  if(m_dumpTruthEles) CHECK( FillTruthEles() );
  if(m_dumpTruthPhs) CHECK( FillTruthPhs() );
  if(m_dumpOffEles) CHECK( FillOffEles() );
  if(m_dumpOffPhs) CHECK( FillOffPhs() );
  if(m_dumpHltPhs) CHECK( FillHltPhs() );
  if(m_dumpTowers || m_dumpRun2Towers)    CHECK( DumpTowers()    ); 

  m_tree->Fill();
 
  setFilterPassed(true); //if got here, assume that means algorithm passed

  return StatusCode::SUCCESS;
}

StatusCode NtupleDumperAlg::beginInputFile() { 


  return StatusCode::SUCCESS;
}



//  LocalWords:  Eles
