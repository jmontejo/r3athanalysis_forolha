#ifndef NTUPLEDUMPER_COMPUTEGTOWERPARAMETERS_H
#define NTUPLEDUMPER_COMPUTEGTOWERPARAMETERS_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigL1Calo/JGTowerContainer.h"
#include "GaudiKernel/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigger/EnergySumRoIAuxInfo.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"

//Example ROOT Includes
//#include "TTree.h"
//#include "TH1D.h"



class ComputegTowerParameters: public ::AthAnalysisAlgorithm { 
 public: 
  ComputegTowerParameters( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ComputegTowerParameters(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  virtual StatusCode  Noise(); 
  virtual StatusCode  createProfile(TString name, TString title, int Nx, float X0, float X1); 
  virtual StatusCode  Rho_Eta(); 
  virtual StatusCode  RetrieveRho(TString name); 
  int iEta(float eta); 
  float Rho(std::vector<float> et); 
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 
  TH1F *h_eta;
  TProfile *pr_eta; 
  TProfile *pr_eta_EM;
  TProfile *pr_eta_HAD;

  std::map<TString, TProfile*> prName; 

  float m_max_et; 
  float m_min_et; 
  float m_absEt; 

  bool m_midTrain;

  float m_rhoA; float m_rhoB; float m_rhoC;
}; 

#endif //> !NTUPLEDUMPER_COMPUTEGTOWERPARAMETERS_H
