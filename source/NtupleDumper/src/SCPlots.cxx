// NtupleDumper includes
#include "SCPlots.h"

//#include "xAODEventInfo/EventInfo.h"




SCPlots::SCPlots( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
    
    declareProperty("SuperCellContainer", m_SuperCellContainer="SCell");
    
    
}


SCPlots::~SCPlots() {}


StatusCode SCPlots::createProfile(TString Name, TString title, int Nx, double X0, double X1){
    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK( histSvc.retrieve() );
    
    TProfile *pr = new TProfile(Name, title, Nx, X0,X1);
    prName[Name]=pr;
    
    std::string name_string = Form("/OUTPUT/SCProfiles/%s",Name.Data());
    CHECK( histSvc->regHist(name_string,pr) );
    
    return StatusCode::SUCCESS;
}

StatusCode SCPlots::initialize() {
    ATH_MSG_INFO ("Initializing " << name() << "...");
    ATH_MSG_INFO ("Supercell container " << m_SuperCellContainer);
    ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
    CHECK( histSvc.retrieve() );
    
    for(int iL = 0; iL<24; iL++){
        TString name = Form("Average_Et_%d",iL);
        TString title = Form("Layer %d; Dist. front train;<E_{T}> (MeV)",iL);
        CHECK(createProfile(name, title, 12,0,48));
        
        name = Form("Average_Et_%d_20BC_mu",iL);
        title = Form("Layer %d, BC < 20; <#mu>;<E_{T}> (MeV)",iL);
        CHECK(createProfile(name, title, 8,40,80)); 
        
        name = Form("Average_Et_%d_BC20_mu",iL);
        title = Form("Layer %d,BC > 20; <#mu>;<E_{T}> (MeV)",iL);
        CHECK(createProfile(name, title, 8,40,80));
        
        name = Form("Average_Et_%d_Timing",iL);
        title = Form("Layer %d w/ Timing; Dist. front train;<E_{T}> (MeV)",iL);
        CHECK(createProfile(name, title, 12,0,48));
        
    }
    
    return StatusCode::SUCCESS;
}

StatusCode SCPlots::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    //
    //Things that happen once at the end of the event loop go here
    //
    
    
    return StatusCode::SUCCESS;
}

StatusCode SCPlots::PlotAverageET(){
    const CaloCellContainer* scells = 0;
    CHECK( evtStore()->retrieve( scells, m_SuperCellContainer.data() ) );
    
    for(auto scell : *scells) {
        
        float et = scell->et();
        float t = scell->time();
        int sampling = scell->caloDDE()->getSampling();
        bool passTime = scell->provenance() & 0x200;
        bool passPF = scell->provenance() & 0x40;
        if(m_SuperCellContainer=="AllCalo"){
            //Bitmas not set of call cell container, so just throw a timing cut
            // in there. Majority of the pedestal details come from low ET anyway, so
            //make it a symmetric 8ns.
            passTime=std::abs(t)<8;
        }
        TString name = Form("Average_Et_%d",sampling);
        prName[name]->Fill(distFrontBunchTrain,et);
        
        name = Form("Average_Et_%d_20BC_mu",sampling);
        if(distFrontBunchTrain<20) prName[name]->Fill(mu,et);
        
        name = Form("Average_Et_%d_BC20_mu",sampling);
        if(distFrontBunchTrain>=20) prName[name]->Fill(mu,et);
        
        name = Form("Average_Et_%d_Timing",sampling);
        if(passTime) prName[name]->Fill(distFrontBunchTrain,et);
    }
    
    return StatusCode::SUCCESS;
}

StatusCode SCPlots::execute() {  
    ATH_MSG_DEBUG ("Executing " << name() << "...");
    setFilterPassed(false); //optional: start with algorithm not passed
    
    const xAOD::EventInfo* ei = 0;
    CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
    mu = ei->averageInteractionsPerCrossing();
    bool isMC=false;
    isMC = (ei->eventType(xAOD::EventInfo::IS_SIMULATION) );
    ToolHandle<Trig::IBunchCrossingTool> m_bcTool("Trig::MCBunchCrossingTool/BunchCrossingTool");
    distFrontBunchTrain = m_bcTool->distanceFromFront(ei->bcid(), Trig::IBunchCrossingTool::BunchCrossings);
    
    CHECK(PlotAverageET());
    
    setFilterPassed(true); //if got here, assume that means algorithm passed
    return StatusCode::SUCCESS;
}
