#ifndef NTUPLEDUMPER_TOWERPLOTS_H
#define NTUPLEDUMPER_TOWERPLOTS_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandle.h"
#include "CaloEvent/CaloCellContainer.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"
#include "xAODTrigL1Calo/JGTowerContainer.h"

#include "TH1.h" 
#include "TTree.h" 
#include "TH2.h" 
#include "TProfile.h" 


class TowerPlots: public ::AthAnalysisAlgorithm { 
 public: 
  TowerPlots( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~TowerPlots(); 

  ///uncomment and implement methods as required

  std::string m_SuperCellContainer; 
                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  virtual StatusCode  DigiTruth(); 
  virtual StatusCode  SuperCellPlots();
  virtual StatusCode  PlotGTowers();
  virtual StatusCode  PlotJTowers(); 
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 
  
   TH1F*   m_scTimeRawET10 = 0;
   TH1F*   m_scTimeRaw10ET = 0;
   TH1F*   m_scTimeET10 = 0; 
   TH1F*   m_scTime10ET = 0; 
   TH1F*   m_scTimeET = 0; 
   TH1F*   m_scTimePFET10 = 0;
   TH1F*   m_scTimePF10ET = 0;
   TH1F*   m_scPFET=0; 
   TH1F*   m_scPFvsTimingET10=0; 
   TH1F*   m_scPFvsTiming10ET=0;

   TH1F*  m_gTowerET = 0;
   TH1F*  m_gTowerET_EM = 0;
   TH1F*  m_gTowerET_had = 0;
   TH1F*  m_gTowerET_fwd = 0;
   TH1F*  m_gTowerEta = 0;
   TH1F*  m_gTowerPhi = 0;
   TH2F*  m_gTowerET_BCID = 0; 
   TH2F*  m_gTowerEtaPhi_ETmap = 0; 
   TH2F*  m_gTowerEtaPhi_ET_EMmap = 0; 
   TH2F*  m_gTowerEtaPhi_ET_hadmap = 0; 
   TH2F*  m_gTowerEtaPhi_ET_fwdmap = 0; 
   TH2F*  m_gTowerEtaPhi_BCIDmap = 0; 
   TH1F*  m_jTowerET = 0;
   TH1F*  m_jTowerET_EM = 0;
   TH1F*  m_jTowerET_had = 0;
   TH1F*  m_jTowerET_fwd = 0;
   TH1F*  m_jTowerEta = 0;
   TH1F*  m_jTowerPhi = 0;
   TH2F*  m_jTowerET_BCID = 0; 
   TH2F*  m_jTowerEtaPhi_ETmap = 0; 
   TH2F*  m_jTowerEtaPhi_ET_EMmap = 0; 
   TH2F*  m_jTowerEtaPhi_ET_hadmap = 0; 
   TH2F*  m_jTowerEtaPhi_ET_fwdmap = 0; 
   TH2F*  m_jTowerEtaPhi_BCIDmap = 0; 

   TProfile *m_SCET_bcid =0;
   TProfile *m_SCET_mu = 0; 

   TProfile *m_jTowerET_bcid=0;
   TProfile *m_jTowerET_mu=0; 
   
   TProfile *m_gTowerET_bcid=0; 
   TProfile *m_gTowerET_mu=0; 

   float distFrontBunchTrain; 
   float mu; 

}; 

#endif //> !NTUPLEDUMPER_TOWERPLOTS_H
