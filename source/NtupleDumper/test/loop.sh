V="V38Run2"
Setting="Default"
JO="jobSettings_Default.py"

#To include options, do ./RunPathena.sh $JO $p... then modify RunPathena accordingly
while read p; do
    echo "$p"
    dsid="$(cut -d'.' -f2 <<<"$p")"
    ./RunPathena.sh $p user.bcarlson.$dsid.$Setting.$V
done <samples
