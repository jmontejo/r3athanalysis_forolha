from TrigT1CaloFexSim.L1SimulationControlFlags import L1Phase1SimFlags as simflags                                                      
       
simflags.CTP.RunCTPEmulation=False                                                                
simflags.Calo.SCellType="Emulated" #Pulse, BCID, emulated                                                                               
simflags.Calo.QualBitMask=0x200
simflags.Calo.ApplySCQual=True
