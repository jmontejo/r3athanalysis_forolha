#This is for setting performance monitoring if needed 
from PerfMonComps.PerfMonFlags import jobproperties
jobproperties.PerfMonFlags.doMonitoring = False

theApp.EvtMax=-1

#Some generic setup stuff

debug = False
doFexAlgos=True
doTruth=False
isESD=False

import glob
files=glob.glob("/eos/atlas/user/b/bcarlson/TriggerHAT/mc16_13TeV.450000.aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_bbbb.recon.AOD.e7107_e5984_s3126_r11410/*")
print files


#Input file
from PyUtils import AthFile
import AthenaPoolCnvSvc.ReadAthenaPool                 #sets up reading of POOL files (e.g. xAODs)
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
svcMgr.EventSelector.InputCollections=files

###
#svcMgr.EventSelector.InputCollections=['/eos/atlas/user/b/bcarlson/Run3Tmp/data18_13TeV.00360026.physics_EnhancedBias.recon.ESD.r10978_r11179_r11185/ESD.16781883._001043.pool.root.1']
#I have one reference file to come back to 

athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections 

from AthenaCommon.AlgSequence import AlgSequence
topSequence=AlgSequence()

af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
run_number = af.run_number[0]

if(isESD):
    #Some database settings, needed for ESD  
    from RecExConfig import AutoConfiguration
    AutoConfiguration.ConfigureSimulationOrRealData()
    AutoConfiguration.ConfigureGeo()
    
    from AthenaCommon.DetFlags import DetFlags
    DetFlags.detdescr.all_setOff() 
    DetFlags.detdescr.Calo_setOn()
    include("RecExCond/AllDet_detDescr.py")

#What does this do 
if(isMC and isESD ):
    include( "LArConditionsCommon/LArIdMap_MC_jobOptions.py" )

#What does this do 
from RegistrationServices.RegistrationServicesConf import IOVRegistrationSvc
svcMgr += IOVRegistrationSvc()
if debug:
    svcMgr.IOVRegistrationSvc.OutputLevel = DEBUG
else:
    svcMgr.IOVRegistrationSvc.OutputLevel = INFO
svcMgr.IOVRegistrationSvc.RecreateFolders = True
svcMgr.IOVRegistrationSvc.SVFolder = False
svcMgr.IOVRegistrationSvc.userTags = False

#These lines are for configuring the bunch crossing tool, useful for some cases
from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
if isMC: ToolSvc += BunchCrossingTool( "MC" )
else: ToolSvc += BunchCrossingTool( "LHC" )


if(isMC and doTruth):
    #Truth jets
    from DerivationFrameworkCore.DerivationFrameworkMaster import *
    from DerivationFrameworkMCTruth.MCTruthCommon import * 

    from JetRec.JetAlgorithm import addJetRecoToAlgSequence
    addJetRecoToAlgSequence(topSequence,eventShapeTools=None)

    from JetRec.JetRecFlags import jetFlags
    jetFlags.useTruth = True

    from JetRec.JetRecStandard import jtm
    from JetRec.JetRecConf import JetAlgorithm

    jetFlags.truthFlavorTags = ["BHadronsInitial", "BHadronsFinal", "BQuarksFinal",
                                "CHadronsInitial", "CHadronsFinal", "CQuarksFinal",
                                "TausFinal",
                                "Partons",
                                ]
    akt4 = jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth", 
                            modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler], 
                            ptmin= 5000)
    akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth",
                            modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler],
                            ptmin= 5000)
    akt4alg = JetAlgorithm("jetalgAntiKt4TruthJets", Tools = [akt4] )
    akt10alg = JetAlgorithm("jetalgAntiKt10TruthJets", Tools = [akt10] )
    topSequence +=  akt4alg 
    topSequence +=  akt10alg


    # Try using TAT to build truth taus?
    DFCommonTauTruthWrapperTools = []

    from MCTruthClassifier.MCTruthClassifierConf import MCTruthClassifier
    DFCommonTauTruthClassifier = MCTruthClassifier(name = "DFCommonTauTruthClassifier",
                                        ParticleCaloExtensionTool="")

    ToolSvc += DFCommonTauTruthClassifier

    from DerivationFrameworkMCTruth.DerivationFrameworkMCTruthConf import DerivationFramework__TruthCollectionMakerTau
    DFCommonTruthTauTool = DerivationFramework__TruthCollectionMakerTau(name             = "DFCommonTruthTauTool",
                                                                        NewCollectionName       = "TruthTaus",
                                                                        MCTruthClassifier       = DFCommonTauTruthClassifier)
    ToolSvc += DFCommonTruthTauTool
    DFCommonTauTruthWrapperTools.append(DFCommonTruthTauTool)

    from TauAnalysisTools.TauAnalysisToolsConf import TauAnalysisTools__TauTruthMatchingTool
    from DerivationFrameworkTau.DerivationFrameworkTauConf import DerivationFramework__TauTruthMatchingWrapper
    DFCommonTauTruthMatchingTool = TauAnalysisTools__TauTruthMatchingTool(name="DFCommonTauTruthMatchingTool")
    ToolSvc += DFCommonTauTruthMatchingTool
    DFCommonTauTruthMatchingWrapper = DerivationFramework__TauTruthMatchingWrapper( name = "DFCommonTauTruthMatchingWrapper",
                                                                                    TauTruthMatchingTool = DFCommonTauTruthMatchingTool,
                                                                                    TauContainerName     = "TauJets")
    ToolSvc += DFCommonTauTruthMatchingWrapper
    print DFCommonTauTruthMatchingWrapper
    DFCommonTauTruthWrapperTools.append(DFCommonTauTruthMatchingWrapper)

    from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__CommonAugmentation
    topSequence += CfgMgr.DerivationFramework__CommonAugmentation("TauTruthCommonKernel",
                                                                AugmentationTools = DFCommonTauTruthWrapperTools)

#Calibrate jets                                                                                                                                                                                    
 
from METUtilities.METMakerConfig import getMETMakerAlg
metAlg = getMETMakerAlg('AntiKt4EMTopo',"Loose")
metAlg.METName = 'MET_Reco_AntiKt4EMTopo'

ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=True
ToolSvc.myJESTool.ConfigFile="JES_MC16Recommendation_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_EtaJES_GSC"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo"
#ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)                                                                                                                           

ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)
ToolSvc += CfgMgr.CP__MuonSelectionTool("MediumMuonTool", MuQuality=1) # 1 corresponds to Medium                                                                                                    
ToolSvc += CfgMgr.JetVertexTaggerTool('JVT')


#Create an output root file 
jps.AthenaCommonFlags.HistOutputs = ["OUTPUT:myfile.root"]

#Default setup
if(isESD): 
    from RecExConfig.RecFlags import rec
    from TrigT1CaloFexSim.L1SimulationControlFlags import L1Phase1SimFlags as simflags
    rec.readAOD=True
    rec.readESD=True
    rec.readRDO=False
    rec.doESD=True
    rec.doWriteAOD=False

    simflags.Calo.QualBitMask=0x40 
    if(doFexAlgos==False):
        simflags.Calo.RunFexAlgorithms=False
        simflags.CTP.RunCTPEmulation=False 

    if(isMC==False):
        simflags.Calo.ApplySCQual=False;
        simflags.Calo.SCellType="Emulated"

if(isESD):
    include("TrigT1CaloFexSim/createL1SimulationSequence.py")

if(isMC==False):
    from EnhancedBiasWeighter.EnhancedBiasWeighterConf import EnhancedBiasWeighter
    TRIG1AugmentationTool = EnhancedBiasWeighter(name = "TRIG1AugmentationTool")
    TRIG1AugmentationTool.RunNumber = run_number
    TRIG1AugmentationTool.UseBunchCrossingTool = True
    ToolSvc += TRIG1AugmentationTool

    from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__DerivationKernel
    topSequence += CfgMgr.DerivationFramework__DerivationKernel("TRIG1Kernel",
                                                              AugmentationTools = [TRIG1AugmentationTool]
                                                              )
        
Mlist = ["jNOISECUT_MET","gXENOISECUT_MET","gXEJWOJ_MET","gXERHO_MET","gXEPUFIT_MET"] 
Jlist = ["jRoundJets", "jRoundLargeRJets", "gL1Jets"]
triggerlist = ["HLT_j15", "HLT_j20", "HLT_j25", "HLT_j35",
               "HLT_j45", "HLT_j45_L1RD0_FILLED"
               "HLT_j60", "HLT_j60_L1RD0_FILLED",
               "HLT_j85", "HLT_j110", "HLT_j175", 
               "HLT_j260", "HLT_j360", "HLT_j400", "HLT_j420",
               "HLT_j440",]

#Dump ntuples  
if(doFexAlgos==False):
    print "Skipping ntupledumper algorithm"
else:
    if(isMC and doTruth):
        topSequence += CfgMgr.NtupleDumperAlg(dumpTauAlgs=True, dumpTowers=False,dumpRun2Towers=False, met_list=Mlist, jet_list=Jlist, trigger_list = triggerlist)
    if (isMC and doTruth==False):
        topSequence += CfgMgr.NtupleDumperAlg(dumpTauAlgs=False, dumpEleAlgs=False,dumpTruthJets=False,dumpTruthTaus=False,dumpTruthMet=False, dumpTruthEles=False, dumpTowers=False,dumpRun2Towers=False, met_list=Mlist, jet_list=Jlist, trigger_list = triggerlist)
    if(isMC==False): 
        topSequence += CfgMgr.NtupleDumperAlg(dumpTauAlgs=False,dumpTruthJets=False,dumpTruthTaus=False,dumpTruthMet=False, dumpTruthEles=False, met_list=Mlist,jet_list=Jlist)
