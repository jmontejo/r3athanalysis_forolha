#This is for setting performance monitoring if needed 
from PerfMonComps.PerfMonFlags import jobproperties
jobproperties.PerfMonFlags.doMonitoring = False

theApp.EvtMax=-1

#Some generic setup stuff

debug = False
doMonitoring=False
doTowerPlots=True
doJetAlgos=True
Sample="background"
doFexAlgos=True
isESD=True
doTruth=True
phaseII=False

import glob
if(Sample=="VBFHinv"):
    files=glob.glob("/eos/atlas/user/b/bcarlson/Run3Tmp/mc16_13TeV.308276.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4nu_MET125.recon.ESD.e6126_e5984_s3126_r11881/*")
if(Sample=="data"):
    files=glob.glob("/eos/atlas/user/b/bcarlson/Run3Tmp/data18_13TeV.00360026.physics_EnhancedBias.recon.ESD.r10978_r11179_r11185/*")
if(Sample=="background"):
    files=glob.glob("/eos/atlas/user/b/bcarlson//Run3Tmp/mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.recon.ESD.e3569_s3126_r11881/*")
if(Sample=="ttbar"):
    files=glob.glob("/eos/atlas/user/b/bcarlson//Run3Tmp/mc16_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.ESD.e5603_s3126_r11881/*")
if(Sample=="AOD"):
    files=glob.glob("/eos/atlas/user/b/bcarlson/TriggerHAT/mc16_13TeV.450000.aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_bbbb.recon.AOD.e7107_e5984_s3126_r11410/*")
if(Sample=="PhaseII"):
    files=glob.glob("/eos/atlas/user/b/bcarlson//PhaseII/mc16_valid.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.recon.AOD.e7543_e5984_s3477_r11666/AOD.19614922._000323.pool.root.1")
else:
    print "No files listed"

print files


#Input file
from PyUtils import AthFile
import AthenaPoolCnvSvc.ReadAthenaPool                 #sets up reading of POOL files (e.g. xAODs)
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
svcMgr.EventSelector.InputCollections=files
athenaCommonFlags.FilesInput = svcMgr.EventSelector.InputCollections 

from RecExConfig.InputFilePeeker import inputFileSummary
print ("Input type: %s" % inputFileSummary['stream_names'])

if("StreamAOD" in inputFileSummary['stream_names']):
    print "Processing an AOD file and disable some settings for reading an ESD, and do not run Run 3 FEX algorithms"
    isESD=False
    doFexAlgos=False

af = AthFile.fopen(svcMgr.EventSelector.InputCollections[0])
isMC = 'IS_SIMULATION' in af.fileinfos['evt_type']
run_number = af.run_number[0]

if(isMC==False):
    print "Setting up for data, do not run truth algorithms"
    doTruth=False

print af.fileinfos['conditions_tag']
tags=af.fileinfos['det_descr_tags']['AMITag']
tag=tags.split("_")[-1:]
if("r11666" in tag):
    print "Running in Phase II settings"
    print tag
    include("NtupleDumper/preIncludeMu200.py")
    phaseII=True

from AthenaCommon.AlgSequence import AlgSequence
topSequence=AlgSequence()

if(isESD):
    #Some database settings, needed for ESD  
    from RecExConfig import AutoConfiguration
    AutoConfiguration.ConfigureSimulationOrRealData()
    AutoConfiguration.ConfigureGeo()
    
    from AthenaCommon.DetFlags import DetFlags
    DetFlags.detdescr.all_setOff() 
    DetFlags.detdescr.Calo_setOn()
    include("RecExCond/AllDet_detDescr.py")

#What does this do 
if(isMC and isESD ):
    include( "LArConditionsCommon/LArIdMap_MC_jobOptions.py" )

#What does this do 
from RegistrationServices.RegistrationServicesConf import IOVRegistrationSvc
svcMgr += IOVRegistrationSvc()
if debug:
    svcMgr.IOVRegistrationSvc.OutputLevel = DEBUG
else:
    svcMgr.IOVRegistrationSvc.OutputLevel = INFO
svcMgr.IOVRegistrationSvc.RecreateFolders = True
svcMgr.IOVRegistrationSvc.SVFolder = False
svcMgr.IOVRegistrationSvc.userTags = False

#These lines are for configuring the bunch crossing tool, useful for some cases
from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
if isMC: ToolSvc += BunchCrossingTool( "MC" )
else: ToolSvc += BunchCrossingTool( "LHC" )

#Calibrate jets 
from METUtilities.METMakerConfig import getMETMakerAlg
metAlg = getMETMakerAlg('AntiKt4EMTopo',"Loose")
metAlg.METName = 'MET_Reco_AntiKt4EMTopo'

ToolSvc += CfgMgr.JetCalibrationTool("myJESTool")
ToolSvc.myJESTool.IsData=True
ToolSvc.myJESTool.ConfigFile="JES_MC16Recommendation_Aug2017.config"
ToolSvc.myJESTool.CalibSequence="JetArea_Residual_EtaJES_GSC"
ToolSvc.myJESTool.JetCollection="AntiKt4EMTopo" 
#ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)

ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)
ToolSvc += CfgMgr.CP__MuonSelectionTool("MediumMuonTool", MuQuality=1) # 1 corresponds to Medium
ToolSvc += CfgMgr.JetVertexTaggerTool('JVT')

if(isMC and doTruth):
    #Truth jets
    from NtupleDumper.TruthJets import truthJets
    truthJets(isESD)
  
    from NtupleDumper.TruthTaus import truthTaus
    truthTaus()

#Create an output root file 

if doJetAlgos:
    if(phaseII): 
        include("TrigL0GepPerf/createL0GepSimulationSequence.py")
    else: 
        from NtupleDumper.JetBuilder import addStandardJets, modifyClusters, modifyJets
        modifyClusters()
        modifyJets()

jps.AthenaCommonFlags.HistOutputs = ["OUTPUT:myfile.root"]

#Default setup
from TrigT1CaloFexSim.L1SimulationControlFlags import L1Phase1SimFlags as simflags
#simflags.Calo.minSCETp=125 
#simflags.Calo.maxSCETm=-125
#simflags.Calo.minTowerET=0
simflags.CTP.RunCTPEmulation=False
from RecExConfig.RecFlags import rec
rec.readAOD=True
rec.readESD=True
rec.readRDO=False
rec.doESD=True
rec.doWriteAOD=False

#simflags.Calo.QualBitMask=0x40 
if(doFexAlgos==False):
    simflags.Calo.RunFexAlgorithms=False
    simflags.CTP.RunCTPEmulation=False 

if(isMC==False):
    simflags.Calo.ApplySCQual=False;
    simflags.Calo.SCellType="Emulated"

if(isESD and doFexAlgos):
    print "Running Run 3 L1Calo"
    include("TrigT1CaloFexSim/createL1SimulationSequence.py")

if(isMC==False):
    from NtupleDumper.EnhancedBiasWeights import enhancedBiasWeight
    enhancedBiasWeight(run_number)
        
if(doMonitoring):
    from TrigL1CaloUpgrade.TrigL1CaloUpgradeConf import SimpleSuperCellChecks
    topSequence += SimpleSuperCellChecks() 
    #from TrigL1CaloUpgrade.TrigL1CaloUpgradeConf import SimpleLArDigitsChecks
    #athAlgSeq += SimpleLArDigitsChecks()


#Make some template data quality plots 
if(isMC and doTowerPlots):
    SCIn = "SCell" 
    if(simflags.Calo.SCellType()=="Emulated"): SCIn = "SimpleSCell" 
    if(simflags.Calo.SCellType()=="BCID"): SCIn = "SCellBCID"

    topSequence += CfgMgr.TowerPlots(SuperCellContainer = SCIn )
    #topSequence += CfgMgr.MakeSCNtuple(SuperCellContainer = SCIn) 
    topSequence += CfgMgr.SCPlots(SuperCellContainer = SCIn)
    #topSequence += CfgMgr.ComputejTowerParameters() 
    #topSequence += CfgMgr.ComputegTowerParameters()

Mlist = []
Run3MET =["jNOISECUT_MET","jXERHO_MET","gXENOISECUT_MET","gXEJWOJ_MET","gXEJWOJRHO_MET","gXEJWOJRHOHT_MET","gXERHO_MET","gXEPUFIT_MET","EventVariables"]
if(doFexAlgos):
    Mlist += Run3MET
Run3Jets = ["jRoundJets","jRoundJetsPUsub", "jRoundLargeRJets","jRoundLargeRJetsPUsub", "gL1Jets","gBlockJets"]
Jlist =["AntiKt4EMTopoJetsCalibrated","AntiKt4lctopoSKJets","AntiKt4lctopoVorJets",
        "AntiKt4lctopoNoAlgJets","AntiKt4lctopoVorSKJets","InTimeAntiKt4TruthJets","OutOfTimeAntiKt4TruthJets"]
        
#This rats nest of conditions is because I can only seem to configure R = 0.4 jets on the AOD...
if(isMC):
    Jlist += ["AntiKt4TruthJets"]
if(isMC and isESD):
    Jlist += ["AntiKt10TruthJets"]
HltJets = ["HLT_xAOD__JetContainer_a4tcemsubjesISFS","HLT_xAOD__JetContainer_a4tcem","HLT_xAOD__JetContainer_a4tcemsub"]
ClusterList = ["CaloCalTopoClusters","OrigSKTopoClusters","OrigVorTopoClusters","OrigVorSKTopoClusters"]
Jlist += ClusterList
if(phaseII):
    Mlist+= ["cluster420","clusterSK420","clusterVor420","clusterVorSK420","clusterVorSK420Timing","pufit420","cluster420Timing","pufit420Timing"]
if(phaseII==False):
    Jlist += HltJets
if(doFexAlgos):
    Jlist+=Run3Jets
    
HltMetlist = ["HLT_xAOD__TrigMissingETContainer_TrigEFMissingET","HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht","HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC"]

triggerlist = ["HLT_j15", "HLT_j20", "HLT_j25", "HLT_j35",
               "HLT_j45", "HLT_j45_L1RD0_FILLED",
               "HLT_j60", "HLT_j60_L1RD0_FILLED",
               "HLT_j85", "HLT_j110", "HLT_j175", 
               "HLT_j260", "HLT_j360", "HLT_j400", "HLT_j420",
               "HLT_j440",
               "L1_J100","L1_SC111-CJ15",
               "L1_XE50",  "HLT_xe110_pufit_xe65_L1XE50",
               "L1_EM22VHI","L1_EM24VHI","L1_4J15.0ETA25", 
               "L1_MU4"]

#Dump ntuples  
if(doFexAlgos==False):
    print "Skipping ntupledumper algorithm"

dumpTauAlgs=doFexAlgos
dumpClusters=True
dumpTowers=False
dumpRun2Towers=False
dumpEleAlgs=doFexAlgos
dumpTrigDec=True
useRun2L1=True

dumpTruthJets=doTruth
dumpTruthTaus=doTruth
dumpTruthMet=doTruth
dumpTruthPhs=doTruth
dumpTruthEles=doTruth
dumpHLTPhs=True
dumpHltMET=True


if(phaseII):
    dumpTrigDec=False
    useRun2L1=False
    dumpHLTPhs=False
    dumpHltMET=False

topSequence += CfgMgr.NtupleDumperAlg(isMC=isMC, dumpTauAlgs=dumpTauAlgs, applyJVT=True,dumpClusters=dumpClusters, dumpTowers=dumpTowers,dumpRun2Towers=dumpRun2Towers, dumpEleAlgs=dumpEleAlgs,  met_list=Mlist, jet_list=Jlist, trigger_list = triggerlist, HLTmet_list=HltMetlist,dumpTrigDec=dumpTrigDec,useRun2L1=useRun2L1,dumpHLTPhs=dumpHLTPhs,dumpHltMET=dumpHltMET,
     dumpTruthJets=dumpTruthJets,dumpTruthTaus=dumpTruthTaus,dumpTruthMet=dumpTruthMet, dumpTruthPhs=dumpTruthPhs, dumpTruthEles=dumpTruthEles,)
