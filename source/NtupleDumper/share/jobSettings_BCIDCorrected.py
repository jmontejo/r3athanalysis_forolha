from TrigT1CaloFexSim.L1SimulationControlFlags import L1Phase1SimFlags as simflags

simflags.CTP.RunCTPEmulation=False

simflags.Calo.QualBitMask=0x40 #0x40 is peak finder, 0x200 is time cut                                                                                                                                            
simflags.Calo.SCellType="BCID" #Pulse, BCID, emulated                                                                                                                                                             
simflags.Calo.ApplySCQual=True; #T/F 
