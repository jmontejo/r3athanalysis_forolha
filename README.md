# R3AthAnalysis

## Make new gitcache (for Athena, but only needed once):  
These instructions assume you have aliased a work directory for your gitcache. If you have not, replace it with the full path.  
      export WORK='PATH TO YOUR DIRECTORY'  
      setupATLAS  
      lsetup git  
      cd $WORK  
      mkdir run3cache  
      cd run3cache  
      git atlas init-workdir https://:@gitlab.cern.ch:8443/$USER/athena.git  

## Checking the analysis package (algorithm code in Athena release) 
    git clone ssh://git@gitlab.cern.ch:7999/l1calo-run3-offline/r3athanalysis.git
    cd r3athanalysis
    mkdir build  run
    cd build 
    acmSetup --sourcearea=../source Athena,21.3,latest,slc6
    acm compile 
Skip to running the code (below) 

## sparse clone fork, checking the analysis package  
    git clone ssh://git@gitlab.cern.ch:7999/l1calo-run3-offline/r3athanalysis.git   
    cd r3athanalysis  
    mkdir build  run  
    cd build  
    acmSetup --sourcearea=../source Athena,21.3,latest,slc6   
    acm sparse_clone_project athena $WORK/run3cache/athena  
    acm add_pkg athena/Trigger/TrigT1/TrigT1CaloFexSim  
    acm add_pkg athena/Trigger/TrigL1Upgrade/TrigL1CaloUpgrade  
    acm compile   

## In the future to setup  
    cd build/  
    acmSetup  
    acm compile (if you made changes)   

## Code structure  
   Go to the source directory  
   Type ls, and you will see NtupleDumper/ and athena/  
   The code checked out from athena will have the packages you need to be able to edit 

## Running the code  

      cd ../run   
      athena  NtupleDumper/jobOptions_full.py 

   To run using the peak finder, run: 
   
      athena NtupleDumper/jobSettings_PeakFiner.py NtupleDumper/jobOptions_full.py
   
## To update to l1calo-run3-offline fork (probably obsolete)
   You may get into a situation where you are on the upstream rather than origin. This doesn't always matter, but here it does, because the branch has changes not in the upstream repository (/athena). To solve this, you could do:

    cd ../source/athena/
    git checkout origin/21.3 (or create a branch)
    git pull origin 21.3



